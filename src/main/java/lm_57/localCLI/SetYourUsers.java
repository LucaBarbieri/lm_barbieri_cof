package lm_57.localCLI;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import lm_57.server.controller.User;
import lm_57.server.model.Constants;

public class SetYourUsers {
	
	private List<User>users;
	private String name;
	private String color;
	private int numUsers;
	private Scanner scan;
	private List<String> colorAssign;

	public SetYourUsers(){
		users=new ArrayList<>();
		scan=new Scanner(System.in);
		colorAssign=new ArrayList<>();
	}
	
	public void setUsers(){
		insertTheNumberOfUsers();
		insertUsers();
	}
	
	public int getNumUsers(){
		return numUsers;
	}
	
	public List<User> getUsers(){
		return users;
	}
	
	private void insertTheNumberOfUsers(){
		System.out.println("Insert the number of users:\t");
		do{
			numUsers=scan.nextInt();
		}while(numUsers<Constants.MIN_PLAYERS || numUsers>Constants.MAX_PLAYERS);
	}
	
	private void insertUsers(){
		for(int i=0;i<numUsers;i++){
			do{
				System.out.println("Insert the user name:\t");
				name=scan.nextLine();
				System.out.println("Insert the user color:\t");
				color=scan.nextLine();
			}while(theColorIsRight());
			users.add(new User(this.name,this.color));
		}
	}
	
	private boolean theColorIsRight(){
		if(!(theColorIsAssigned()) && theColorIsSupported()){
				return true;
		}
		return false;
	}
	
	private boolean theColorIsSupported(){
		for(String c:Constants.getColorSupportedString()){
			if(color.equals(c)){
				return true;
			}
		}
		return false;
	}
	
	private boolean theColorIsAssigned(){
		for(String c:colorAssign){
			if(color.equals(c)){
				return true;
			}
		}
		return false;
	}
}
