package lm_57.localCLI;

import java.rmi.RemoteException;

import lm_57.server.controller.action.ControlMainAction;
import lm_57.server.controller.socket.ControlCommandAction;
import lm_57.server.model.GameBoard;

public class RealMainAction {
	
	private ControlMainAction mainAction;
	
	public void ControlMainAction(){
		
	}
	
	public void run(ControlCommandAction controlCommandAction, GameBoard game) throws RemoteException{
		switch (controlCommandAction.getNumCurrentAction()){
		case 1:
			//BuildEmporiumByPermitCard-City_IndexPlayerPermit-
			mainAction=new ControlMainAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction()), null, controlCommandAction.getCity(), null, null, 0, controlCommandAction.getPermitCard());
			mainAction.isCompleted();
			break;
		case 2:
			//BuyPermitCard-Region_IndexRegionPermit-
			mainAction=new ControlMainAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction()), controlCommandAction.getRegion(), null, null, null, controlCommandAction.getIndexRegion(), null);
			mainAction.isCompleted();
			break;
		case 3:
			//BuildWithKing-ListCities-
			mainAction=new ControlMainAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction()), null, null, controlCommandAction.getCityPath(), null, 0, null);
			mainAction.isCompleted();
			break;
		case 4:
			//SetNewCouncillorOnBalcon-King_Region_Color-
			mainAction=new ControlMainAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction())+"King", null, null, null, null, 0, null);
			mainAction.isCompleted();
			break;
		}
	}
}