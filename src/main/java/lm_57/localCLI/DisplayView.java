package lm_57.localCLI;

import java.util.Scanner;


import lm_57.server.controller.socket.ControlCommandAction;
import lm_57.server.controller.socket.ControlCommandMarket;
import lm_57.server.model.GameBoard;
import lm_57.server.model.Player;

public class DisplayView {
	
	private GameBoard game;
	private Scanner scan;
	private String command;
	private ControlCommandAction controlCommandAction;
	private ControlCommandMarket controlCommandMarket;
	
	
	public DisplayView(GameBoard game){
		this.scan=new Scanner(System.in);
		this.game=game;
	}
	
	public void showMeActions(){
		System.out.println("1)BuildEmporiumByPermitCard-City_IndexPlayerPermit-\n"+
	                         "2)BuyPermitCard-Region_IndexRegionPermit-\n"+
        		             "3)BuildWithKing-ListCities-\n"+
	                         "4)SetNewCouncillorOnBalcon-King_Region_Color-\n"+
        		             "5)BuyAssistant-\n"+
	                         "6)PayAssistantForChangePermitCard-Region-\n"+
        		             "7)ElectedCouncillor-King_Region_Color\n"+
	                         "8)PayAssistantForNewMainAction-\n"+
        		             "9)NoFastAction-");
	}
	
	public void showMeMarketAction(){
		System.out.println("1)Assistant-NumAssistant_CoinToBuy-\n"+
							"2)PoliticCard-Politic_CoinToBuy-\n"+
   		     				"3)PermitCard-Permit_CoinToBuy-");
	}
	
	public void showMeCurrentPlayer(){
		System.out.println(game.getCurrentPlayerToString());
	}
	
	public void showMePlayersScore(){
		System.out.println("Players Score");
		for(Player p:game.getPlayers()){
			System.out.println(p.toString());
			System.out.println(p.displayPermitCard());
		}
		System.out.print("\n");
	}
	
	public void showMyPoliticCard(){
		System.out.println("Your Politic Card Deck");
		for(String s:game.getCurrentPlayerTurn().getPoliticCardToString()){
			System.out.println(s);
		}
	}
	
	public ControlCommandAction chooseYourAction(){
		System.out.println("Insert the command whit its option:\n");
		do{
			this.command=scan.nextLine();
			controlCommandAction=new ControlCommandAction(game.getMap(),game.getPlayers());
		}while(controlCommandAction.controlPrototypeCommand(this.command,game.getCurrentPlayerToString()));
		return controlCommandAction;
	}
	
	public ControlCommandMarket chooseYourMarketAction(){
		System.out.println("Insert the command whit its option:\n");
		do{
			this.command=scan.nextLine();
			controlCommandMarket=new ControlCommandMarket(game.getPlayers());
		}while(controlCommandMarket.controlPrototypeCommand(this.command,game.getCurrentPlayerToString()));
		return controlCommandMarket;
	}
}