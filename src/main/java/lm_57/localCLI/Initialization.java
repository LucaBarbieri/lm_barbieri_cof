package lm_57.localCLI;

import java.io.IOException;

import java.util.Scanner;

import lm_57.server.model.GameBoard;


public class Initialization {
	
	private GameBoard game;
	private SetYourUsers setUser;
	private String gameName;
	private Scanner scan;
	
	public Initialization(){
		scan=new Scanner(System.in);
		setGameBordName();
		game=new GameBoard(gameName);
		setUser=new SetYourUsers();
	}
	
	public void startInit() throws IOException{
		this.game.setNumMap(setNumberMap());
		this.game.setNumBonus(setNumberBonus());
		this.game.setMap();
		this.game.addPlayer(setUser.getNumUsers(),setUser.getUsers());
	}
	
	public GameBoard getGameBoard(){
		return this.game;
	}
	
	private void setGameBordName(){
		System.out.println("Insert the game name:\t");
		this.gameName=scan.nextLine();
	}
	
	private int setNumberMap(){
		int numMap;
		do{
			System.out.println("Choose your map. Insert its number:\t");
			numMap=scan.nextInt();
		}while(numMap>8 || numMap<1);
		return numMap;
	}
	
	private int setNumberBonus(){
		System.out.println("Choose the number of bonus for each permit card and city. Insert its number:\t");
		int numBonus=scan.nextInt();
		return numBonus;
	}
}