package lm_57.localCLI;

import java.rmi.RemoteException;

import lm_57.server.controller.action.ControlFastAction;
import lm_57.server.controller.socket.ControlCommandAction;
import lm_57.server.model.GameBoard;

public class RealFastAction {
	
private ControlFastAction fastAction;
	
	public void run(ControlCommandAction controlCommandAction, GameBoard game) throws RemoteException{
		switch (controlCommandAction.getNumCurrentAction()){
		case 5:
			//BuyAssistant-
			fastAction=new ControlFastAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction()), null, null, null, controlCommandAction.getColorChose(), 0, null);
			fastAction.isCompleted();
			break;
		case 6:
			//PayAssistantForCHangePermitCard-Region-
			fastAction=new ControlFastAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction()), controlCommandAction.getRegion(), null, null, null, 0, null);
			fastAction.isCompleted();
			break;
		case 7:
			//ElectedCouncillor-King_Region_Color
			fastAction=new ControlFastAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction())+"King", null, null, null, null, 0, null);
			fastAction.isCompleted();
			break;
		case 8:
			//PayAssistantForNewMainAction
			
			fastAction=new ControlFastAction(game.getCurrentPlayerTurn(),game.getMap(),Integer.toString(controlCommandAction.getNumAction()), null, null, null, null, 0, null);
			fastAction.isCompleted();
			break;
		}
	}
}
