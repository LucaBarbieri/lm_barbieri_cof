package lm_57.localCLI;



import java.rmi.RemoteException;


import lm_57.server.controller.socket.ControlCommandAction;
//import lm_57.server.controller.socket.ControlCommandMarket;
import lm_57.server.model.GameBoard;


public class Gaming {
	
	private Initialization init;
	private GameBoard game;
	private DisplayView displayView;
	private ControlCommandAction controlCommandAction;
	//private ControlCommandMarket controlCommandMarket;
	private RealMainAction realMainAction;
	private RealFastAction realFastAction;
	private int mainAction=1;
	private int fastAction=1;
	
	public Gaming(){
		init=new Initialization();
		this.game=this.init.getGameBoard();
		displayView=new DisplayView(this.game);
		controlCommandAction=new ControlCommandAction(game.getMap(),game.getPlayers());
	}
	
	public void startTheGame() throws RemoteException{
		this.game.start();
		displayView.showMeActions();
	
		
		doAction();
		mainAction=1;
		fastAction=1;
		this.game.setNextTurn();
	}
	
	private void doAction() throws RemoteException{
		do{
			if((controlCommandAction=displayView.chooseYourAction()).getNumAction()<=4 && controlCommandAction.getNumAction()>=1){
				realMainAction.run(controlCommandAction, game);
				mainAction--;
			}
			if(controlCommandAction.getNumAction()>=5 && controlCommandAction.getNumAction()<=8){
				realFastAction.run(controlCommandAction, game);
				fastAction--;
			}
			else{
				if(controlCommandAction.getNumAction()==8){
					do{
						controlCommandAction=displayView.chooseYourAction();
					}while((controlCommandAction=displayView.chooseYourAction()).getNumAction()<=4 && controlCommandAction.getNumAction()>=1);
					realMainAction.run(controlCommandAction, game);
					fastAction--;
				}
			}
			if(controlCommandAction.getNumAction()==9){
				fastAction--;
			}
		}while(mainAction==0 && fastAction==0);
	}
}