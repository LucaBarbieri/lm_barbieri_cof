package lm_57.server.controller;

import java.io.Serializable;

import lm_57.client.controller.RMI.ClientRMI;

public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -211201835270419156L;
	private final String name;
	private final String color;
	private ClientRMI clientRMI;
	
	public User(String name,String color){
		this.name=name;
		this.color=color;
	}

	public String getName() {
		return name;
	}

	public void setClientRMI( ClientRMI client){
		clientRMI=client;
	}
	
	public ClientRMI getClientRMI(){
		return this.clientRMI;
	}
	
	
	public String getColor() {
		return color;
	}

	
	public boolean checkColor(String color){
		return this.color.equals(color);
	}
	


}
