package lm_57.server.controller;



import lm_57.server.model.GameBoard;
import lm_57.server.model.action.*;

public class Controller {

	private final GameBoard game;
	
	public Controller(GameBoard game){
		this.game=game;
	}
	
	public GameBoard getGame(){
		return game;
	}
	
	public void update(Action action){
		System.out.println("I am the controller updating the model !");
		action.isCompleted();
	}
	
}
