package lm_57.server.controller.state;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import lm_57.server.model.Constants;
import lm_57.server.model.Player;

public class StateRound implements IState{

	private final Queue<Player> players;
	private Player currentPlayer;
	private StateTurn currentTurn;
	private boolean stateFinishedGame;
	
	/**
	 * the round state manage the turns in order to the queue of players 
	 * and if a player during his turn have finished the game the  boolean stateFinishedGame 
	 * become true and the game is set like finished.
	 * 
	 * @param players queue of players
	 * 
	 */
	
	public StateRound(final Queue<Player> players){
		this.players=new LinkedBlockingQueue<>(players);
		stateFinishedGame=false;
		setCurrentPlayer();
		currentTurn=new StateTurn(currentPlayer,this);
	}
	
	public void setCurrentPlayer(){
		currentPlayer=players.peek();
		players.remove(currentPlayer);
	}
	
	
	public void setNextTurn(){
		if(currentTurn.isCompleted() && gameIsFinished()){
			currentTurn.getCurrentAgent().addPlayerVictory(3);
			setCurrentPlayer();
			currentTurn=new StateTurn(currentPlayer,this);	
			return;
		}
		if(currentTurn.isCompleted() && !gameIsFinished()){
			setCurrentPlayer();
			currentTurn=new StateTurn(currentPlayer,this);	
		}
	}
	public StateTurn getCurrentTurn(){
		return currentTurn;
	}
	public Queue<Player> getPlayers(){
		return players;
	}
	
	public void setFinishedGame(){
		stateFinishedGame=true;
	}
	
	@Override
	public StateRound getCurrentState(){
		return this;
	}
	
	@Override
	public boolean gameIsFinished(){
		return stateFinishedGame;
	}
	
	@Override
	public boolean isCompleted(){
		if(players.isEmpty() && currentTurn.isCompleted()){
				return true;
			}
		
			return false;
	}
	

	@Override
	public Player getCurrentAgent() {
		if(!this.isCompleted() ){
			return currentTurn.getCurrentAgent();
		}
		return null;
	}
	
	@Override
	public String getColorAgent() {
		return Constants.convertColorToString(this.getCurrentAgent().getPlayerColor());
	}
	
	@Override
	public String toString(){
		return "[ state: Round ]" + "[current Turn: "+ this.getCurrentTurn().getCurrentAgent().getName()+"]";
	}
}
