package lm_57.server.controller.state;

import lm_57.server.model.Player;

public interface IState {
	
	public boolean isCompleted();
	
	public boolean gameIsFinished();
	
	public IState getCurrentState();
	
	public Player getCurrentAgent();
	
	public String getColorAgent();
	
}
