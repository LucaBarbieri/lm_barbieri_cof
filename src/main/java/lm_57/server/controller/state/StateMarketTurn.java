package lm_57.server.controller.state;

import lm_57.server.model.Constants;
import lm_57.server.model.Player;

public class StateMarketTurn implements IState{
	
	private Player currentPlayer;
	private int action;
	
	public StateMarketTurn(Player agent){
		currentPlayer=agent;
		action=1;
	}	
	public void removeAction(){
		if(action>0){
			action--;
		}
	}

	@Override
	public IState getCurrentState(){
		return this;
	}

	@Override
	public boolean isCompleted() {
		if(action==0){
			return true;
		}
		return false;
	}

	@Override
	public boolean gameIsFinished() {
		return false;
	}
	@Override
	public Player getCurrentAgent() {
		return currentPlayer;
	}
	
	@Override
	public String getColorAgent() {
		return Constants.convertColorToString(currentPlayer.getPlayerColor());
	}
}
