package lm_57.server.controller.state;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import lm_57.server.model.Player;

public class StateMarketBuy implements IState{
	
	List<Player> players=new ArrayList<>();
	StateMarketTurn currentTurn;
	Player currentPlayer;
	Random random=new Random();
	
	/**
	 * the state market buy have the list of all player and set the current  random Turn of player to buy 
	 * @param player list of player
	 */
	
	public StateMarketBuy(Queue<Player> player) {
		players.addAll(player);
		setCurrentPlayer();
		setCurrentTurn(currentPlayer);
		
		
	}
	public void setCurrentPlayer(){
		
		int index=random.nextInt(this.players.size());
		currentPlayer=players.get(index);
		players.remove(currentPlayer);
		
	}
	
	public void setCurrentTurn(Player player){
		currentTurn=new StateMarketTurn(player);
	}
	
	public StateMarketTurn getCurrentTurn(){
		return currentTurn;
	}
	
	public void setNextTurn(){
		if(currentTurn.isCompleted() && !isCompleted()){
			setCurrentPlayer();
			setCurrentTurn(currentPlayer);
		}
	}
	
	@Override
	public boolean isCompleted(){
		if(players.isEmpty()){
			return true;
		}
		else
			return false;
	}
	@Override
	public boolean gameIsFinished() {
		return false;
	}
	@Override
	public IState getCurrentState() {
		return this;
	}
	@Override
	public Player getCurrentAgent() {
		if(!this.isCompleted()){
			return currentPlayer;
		}
		return null;
	}
	@Override
	public String getColorAgent() {
		return this.getCurrentTurn().getColorAgent();
	}

	
}
