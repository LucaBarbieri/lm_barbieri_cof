package lm_57.server.controller.state;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import lm_57.server.model.Player;

public class StateGameCompleted implements IState{
	
	List <Player> players=new ArrayList<>();
	
	public StateGameCompleted(Queue <Player> players){
		this.players.addAll(players);
	}
	
	/**
     * calculate the victory point at the end of the game
     * @param firstPlayerNobility players with the highest position in the nobility track
     * @param secondPlayerNobility players with the second highest position in the nobility track
     * 
     */
	
	 public void pointCalculation(){
	       	List<Player> firstPlayerNobility=new ArrayList<>();
	    	List<Player> secondPlayerNobility=new ArrayList<>();
	    	int first= firstPositionNobility();
	    	int second=secondPositionNobility(removeFirstNobilityPlayer(first));
	    		for(Player player:players){
	    			if(player.getPlayerNobility()==first){
	    			firstPlayerNobility.add(player);	
	    			}
	    			if(player.getPlayerNobility()==second){
	    				secondPlayerNobility.add(player);
	    			}
	    		}
	    	addVictoryForNobilityPosition(firstPlayerNobility,secondPlayerNobility);
	    	addVictoryForPermitCard();
	    }
	    
	 public Player getWinner(){
 		pointCalculation();
 		int maxVictoryPoint=0;
 		Player winnerPlayer=null;
 		for(Player player:players){
 			if(player.getPlayerVictory()>maxVictoryPoint){
 				maxVictoryPoint=player.getPlayerVictory();
 				winnerPlayer=player;
 			}
 		}
 		return winnerPlayer;
 }
 /**
  * add victory to players in the first or second position in the nobility track
  * following the rules of the game
  * @param firstPosition 
  * @param secondPosition
  */

 private void addVictoryForNobilityPosition(List<Player> firstPosition, List<Player> secondPosition){
 	if(firstPosition.size()==1){
 		firstPosition.get(0).addPlayerVictory(5);
 		for(int i=0;i<secondPosition.size();i++){
 			secondPosition.get(i).addPlayerVictory(2);
 		}
 		return;
 	}
 	else{
 		for(int i=0;i<firstPosition.size();i++){
 			firstPosition.get(i).addPlayerVictory(5);
 		}
 		return;
 	}
 }
 
 /**
  * remove the player in the first position of nobility track 
  * 
  * @param firstPosition the player in the first position in the nobility track
  * @return the list of player without the player in first position
  */
 
 private List<Player> removeFirstNobilityPlayer(int firstPosition){
	 List<Player> playerCurrent=new ArrayList<>();
	 playerCurrent.addAll(players);
	 for(Player player:players){
	 		if(player.getPlayerNobility()==firstPosition){
	 			playerCurrent.remove(player);
	 		}
	 	}
	 return playerCurrent;
 }
 
 /**
  * return the position of the second player in nobility track
  * 
  * @param playerNobility the list of player in the nobility without the first position
  * @return the max of player position, that is the second position
  * 
  */
 
 private int secondPositionNobility(List<Player> playerNobility){
 	int secondPosition=0;
 	for(Player player:playerNobility){
 		if(player.getPlayerNobility()>secondPosition){
 			secondPosition=player.getPlayerNobility();
 		}
 	}
 	
 	return secondPosition;
 }
 
/**
 * 
 * @return the position of first player/players in the nobility track
 */
 
 private int firstPositionNobility(){
 	int maxNobility=0;
 	for(Player player:players){
 		if(maxNobility<player.getPlayerNobility()){
 			maxNobility=player.getPlayerNobility();
 		}
 	}
 	return maxNobility; 	
 }
 
 /**
  * add victory points to the player that owned more permit cards then others
  */
 
 private void addVictoryForPermitCard(){
 	Player player = null;
 	int maxPermitCard=0;
 	for(Player currentPlayer:players ){
 		if(currentPlayer.getPlayerPermitCard().size()>maxPermitCard){
 			maxPermitCard=currentPlayer.getPlayerPermitCard().size();
 			player=currentPlayer;
 		}
 	}
 	if(maxPermitCard==0){
 		return;
 	}
 	if(player !=null){
 		player.addPlayerVictory(3);
 	}
 }
 	

 	@Override
 	public IState getCurrentState(){
 		return this;
 	}
 	
	@Override
	public boolean isCompleted() {
		return true;
	}

	@Override
	public boolean gameIsFinished() {
		return true;
	}

	@Override
	public Player getCurrentAgent() {
		return null;
	}
	@Override 
	public String toString(){
		return "[ state: game is completed ]"+" [The winner is: "+ this.getWinner().getName()+"]";
	}

	@Override
	public String getColorAgent() {

		return null;
	}

}
