package lm_57.server.controller.state;

import java.util.LinkedList;
import java.util.Queue;

import lm_57.server.model.Player;

public class StateMarket implements IState{
	
	Queue<Player> players =new LinkedList<>();
	IState currentState;
	StateMarketSell sell;
	StateMarketBuy buy;
	
	
	/**
	 * the market is also divided into two substate: 
	 * StateMarketBuy
	 * StateMarketSell
	 * the Market set the beginning current state to the sellState
	 * 
	 */
	
	public StateMarket(Queue<Player> player){
		this.players.addAll(player);
		sell=new StateMarketSell(player);
		currentState=sell;
	}
	
	/**
	 * set the next substate that can be:
	 * SELL->BUY
	 * BUY->NULL
	 */
	
	public void setNextState(){
		if(currentState.equals(sell) && sell.isCompleted()){
			buy=new StateMarketBuy(players);
			currentState=buy;
			return;
		}
		if(currentState.equals(sell) && !sell.isCompleted()){
			sell.setNextTurn();
			return;
		}
		if(currentState.equals(buy) && buy.isCompleted()){
			currentState=null;
			return;
		}
		if(currentState.equals(buy) && !buy.isCompleted()){
			buy.setNextTurn();
		}
	}
	
	public IState getMarketState(){
		return currentState;
	}
	
	public StateMarketSell getSell(){
		return sell;
	}
	
	public StateMarketBuy getBuy(){
		return buy;
	}

	@Override
	public boolean isCompleted() {
		if(currentState==null){
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean gameIsFinished() {
		return false;
	}

	@Override
	public StateMarket getCurrentState() {
		return this;
	}

	@Override
	public Player getCurrentAgent() {
		if(currentState.equals(sell) && !sell.isCompleted()){
			return sell.getCurrentTurn().getCurrentAgent();
		}
		if(currentState.equals(buy) && !buy.isCompleted()){
			return buy.getCurrentTurn().getCurrentAgent();
		}
		return null;
	}
	
	@Override 
	public String toString(){
		if(currentState==sell){
			return "[ state: Market sell] "+ "[ current turn: "+ sell.getCurrentTurn().getCurrentAgent().getName()+"]";
		}
		if(currentState==buy){
			return "[ state: Market buy] "+ "[ current turn: "+ buy.getCurrentTurn().getCurrentAgent().getName()+"]";
		}
		else
			return "The state of market is finished";
	}

	@Override
	public String getColorAgent() {
		if(currentState==sell){
			return sell.getColorAgent();
		}
		else{
			return buy.getColorAgent();
		}
	}
	
}
