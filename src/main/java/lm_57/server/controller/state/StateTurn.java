package lm_57.server.controller.state;

import lm_57.server.model.Constants;
import lm_57.server.model.Player;

public class StateTurn implements IState{

private Player agentPlayer;
	
	private int mainAction;
	private int fastAction;
	private StateRound round;
	
	/**
	 * each turn of a player have a single main action and fast action
	 * 
	 * @param agentPlayer currentPlayer
	 * @param round currentRound
	 */
	
	public StateTurn(Player agentPlayer, StateRound round){
		this.agentPlayer=agentPlayer;
		this.mainAction=1;
		this.fastAction=1;
		agentPlayer.addPoliticCard(1);
		this.round=round;
	}
	
	public void removeMainAction(){
		mainAction--;
	}
	public void removeFastAction(){
		fastAction--;
	}
	
	public void addMainAction(){
		mainAction++;
	}
	
	public int getNumberMainAction(){
		return mainAction;
	}
	
	public int getNumberFastAction(){
		return fastAction;
	}

	@Override
	public IState getCurrentState(){
		return this;
	}
	
	@Override
	public boolean isCompleted(){
		if(mainAction==0 && fastAction==0 && gameIsFinished()){
			return true;
		}
		if(mainAction==0 && fastAction==0 && !gameIsFinished()){
			return true;
		}
		else{
			return false;
		}
	}
	
	@Override
	public boolean gameIsFinished(){
		if(agentPlayer.getPlayerEmporium().isEmpty()){
			round.setFinishedGame();
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public Player getCurrentAgent() {
		return agentPlayer;
	}
	
	@Override
	public String getColorAgent() {
		return Constants.convertColorToString(agentPlayer.getPlayerColor());
	}
	
}
