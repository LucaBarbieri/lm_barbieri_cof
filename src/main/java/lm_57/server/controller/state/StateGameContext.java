package lm_57.server.controller.state;

import java.util.LinkedList;
import java.util.Queue;

import lm_57.server.model.Player;



public class StateGameContext {
	
	private IState currentState;
	private Queue<Player> players=new LinkedList<>();
	
	private IState gameFinished;
	private StateRound round;
	private StateMarket market;
	
	/**
	 * this state track all the others possible states, that are:
	 * roundState
	 * MarketState
	 * GameCompletedState
	 * 
	 * @param players the queue of game players in order
	 * 
	 * the constructor set the initialization state to the Round
	 * 
	 */
	
	public StateGameContext(Queue<Player> players){
		this.players.addAll(players);
		round=new StateRound(players);
		currentState=round;
	}
	
	/**
	 * set the next state in the game that can be:
	 * ROUND && !GAMEISFINISHED->MARKET
	 * ROUND && GAMEISFINISHED->GAMECOMPLETED
	 * MARKET->ROUND
	 * GAMECOMPLETED->NULL
	 * 
	 */
	
	public void setNextState(){
		if(currentState.equals(round) && round.isCompleted() && !round.gameIsFinished()){
				market=new StateMarket(players);
				currentState=market;
				return;
			}
		if(currentState.equals(round) && !round.isCompleted()){
			round.setNextTurn();
			return;
		}
		if(currentState.equals(market) && market.isCompleted()){
				round=new StateRound(players);
				currentState=round;
				return;
			}
		if(currentState.equals(market) && !market.isCompleted()){
			market.setNextState();
			return;
		}
		if(currentState.equals(gameFinished)){
			return;
		}
		if(currentState.equals(round) && round.isCompleted() && round.gameIsFinished()){
			gameFinished=new StateGameCompleted(players);
			currentState=gameFinished;
			return;
		}
	}	

	
	
	public StateGameCompleted getGameCompleted(){
		return (StateGameCompleted) gameFinished.getCurrentState();
	}
	
	public StateMarket getCurrentMarket(){
		return  (StateMarket) market.getCurrentState();
	}
	
	public StateRound getCurrentRound(){
		return (StateRound) round.getCurrentState();
	}

	public IState getCurrentState(){
		if(currentState==round){
			return (StateRound) round.getCurrentState();
		}
		if(currentState==market){
			return (StateMarket) currentState;
		}
		return (StateGameCompleted) currentState;
	}
	
	@Override
	public String toString(){
		if(currentState==round){
			return round.toString();
		}
		if(currentState==market){
			return market.toString();
		}
		else
			return gameFinished.toString();
	}
	
	
	public void removePlayer(Player p){
		this.players.remove(p);
	}
	 
	
}
