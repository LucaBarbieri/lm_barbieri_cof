package lm_57.server.controller.state;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import lm_57.server.model.Constants;
import lm_57.server.model.Player;

public class StateMarketSell implements IState{
	
	Queue<Player> players=new LinkedBlockingQueue<>();
	StateMarketTurn currentTurn;
	Player currentPlayer;
	
	/**
	 * the market sell set the turn in the order of the queue of players
	 * @param player
	 * 
	 */
	
	public StateMarketSell(Queue<Player> player) {
		players.addAll(player);
		setCurrentPlayer();
		setCurrentTurn(currentPlayer);
	}
	
	public void setCurrentPlayer(){
		currentPlayer=players.poll();
		players.remove(currentPlayer);
	}
	
	public void setCurrentTurn(Player player){
		currentTurn=new StateMarketTurn(player);
	}
	
	public StateMarketTurn getCurrentTurn(){
		return currentTurn;
	}
	
	public void setNextTurn(){
		if(currentTurn.isCompleted()){
			setCurrentPlayer();
			setCurrentTurn(currentPlayer);
		}
	}
	
	@Override
	public boolean isCompleted(){
		if(players.isEmpty() && currentTurn.isCompleted()){
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean gameIsFinished() {
		return false;
	}

	@Override
	public IState getCurrentState() {
		return this;
	}

	@Override
	public Player getCurrentAgent() {
	
		return currentPlayer;
		
	}	
	
	@Override
	public String getColorAgent() {
		return Constants.convertColorToString(this.getCurrentAgent().getPlayerColor());
	}

}
