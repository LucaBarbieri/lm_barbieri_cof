package lm_57.server.controller.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuyAssistant;

public class ControlBuyAssistant {
	
	private Player agentPlayer;
	private BuyAssistant buyAssistant;
	
	public ControlBuyAssistant(Player agentPlayer){
		this.agentPlayer=agentPlayer;
	}
	
	
	public boolean isCompleted() throws RemoteException{
		if(this.agentPlayer.getPlayerCoin()>=3){
			this.buyAssistant=new BuyAssistant(this.agentPlayer);
			buyAssistant.isCompleted();
			return true;
		}
		return false;
	}


	public Player getPlayer() {
	
		return this.agentPlayer;
	}//Only for test 
}
