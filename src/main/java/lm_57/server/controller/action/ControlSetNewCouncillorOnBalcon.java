package lm_57.server.controller.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.action.SetNewCouncillorOnBalcon;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.Councillor;

public class ControlSetNewCouncillorOnBalcon{
	
	private Player agentPlayer;
	private CouncilBalcon council;
	private Councillor newCouncillor=new Councillor();
	private SetNewCouncillorOnBalcon setNewCouncillorOnBalcon;
	
	
	public ControlSetNewCouncillorOnBalcon(Player agentPlayer,CouncilBalcon council,Councillor newCouncillor){
		this.agentPlayer=agentPlayer;
		this.council=council;
		this.newCouncillor=newCouncillor;
	}
	
	public boolean isCompleted() throws RemoteException{
		//this.newCouncillor.setCouncillorColor(this.color);
		this.setNewCouncillorOnBalcon=new SetNewCouncillorOnBalcon(this.agentPlayer,this.council,this.newCouncillor);
		setNewCouncillorOnBalcon.isCompleted();	
		return setNewCouncillorOnBalcon.isCompleted();
		}
}
