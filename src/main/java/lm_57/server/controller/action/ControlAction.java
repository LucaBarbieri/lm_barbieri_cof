package lm_57.server.controller.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;

public abstract class ControlAction {
	
	protected Player agentPlayer;
	
	public void setAgent(Player player){
		agentPlayer=player;
	}
	
	public Player getAgent(){
		return agentPlayer;
	}
			
	public abstract boolean isCompleted() throws RemoteException;
	
	public abstract String showMeActions();
}
