package lm_57.server.controller.action;

import java.awt.Color;


import java.rmi.RemoteException;
import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.Councillor;
import lm_57.server.model.utility.PermitCard;

public class ControlMainAction extends ControlAction{
	
	private Player agentPlayer;
	private Map map;
	private int numAction;
	private PermitCard permitCard;	//to initialize by command
	private City city;	/*to initialize by command*/
	private Region region;	/*to initialize by command*/
	private List<String> cityPath=new ArrayList<>();	/*to initialize by command*/
	private Councillor newCouncillor=new Councillor();
	private Color color;
	private CouncilBalcon council;
	private int indexRegionPermitCard;
	
	/*main action*/
	private ControlBuildEmporiumByPermitCard buildEmporiumByPermitCard;
	private ControlBuyPermitCard buyPermitCard;
	private ControlBuildOnKingCity buildOnKingCity;
	private ControlSetNewCouncillorOnBalcon setNewCouncillorOnBalcon;	
	
	public ControlMainAction(Player agentPlayer,Map map,String command,Region region,City city,List<String> cityPath,Color color,int indexRegionPermitCard,PermitCard permitCard){
		this.agentPlayer=agentPlayer;
		this.map=map;
		String[] commands=command.split("-");
		numAction=Integer.parseInt(commands[0]);
		try{
			this.council=new CouncilBalcon();
		}
		catch(RemoteException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"Can't generate council");
		}
		if(!(region==null)){
			this.region=region;
		}
		if(!(city==null)){
			this.city=city;
		}
		if(command.contains("4") && (region!=null)){
			this.council=region.getCouncilBalcon();
		}
		if(command.contains("4") && command.contains("King")){
			for(Councillor c:this.map.getKing().getCouncillorOnBalcon()){
				this.council.setCouncillorOnBalcon(c);
			}
		}
		if(command.contains("4") && !(color==null)){
			this.color=color;
			this.newCouncillor.getCouncillor();
			this.newCouncillor.setCouncillorColor(this.color);
		}
		if(permitCard!=null){
			this.permitCard=permitCard;
		}
		this.map.getCityList();
		if(!cityPath.isEmpty()){
			this.cityPath=cityPath;
		}
		
		if(!(indexRegionPermitCard==0)){
			this.indexRegionPermitCard=indexRegionPermitCard;
		}
	}
	
	@Override
	public boolean isCompleted() throws RemoteException{
		return chooseYourMainAction();
	}
	
	@Override
	public String showMeActions(){
		return "[BuildEmporiumByPermitCard] [BuyPermitCard] [BuildOnKingCity] [SetNewCouncillorOnBalcon]";
	}
	
	private boolean chooseYourMainAction() throws RemoteException{
		
		switch(numAction){
		case 1:
			buildEmporiumByPermitCard=new ControlBuildEmporiumByPermitCard(this.agentPlayer,this.map,this.permitCard,this.city);//to finish
			buildEmporiumByPermitCard.isCompleted();
			return true;
		case 2:
			buyPermitCard=new ControlBuyPermitCard(this.agentPlayer,this.region,this.indexRegionPermitCard);
			buyPermitCard.isCompleted();
			return true;
		case 3:
			buildOnKingCity=new ControlBuildOnKingCity(this.agentPlayer,this.city,this.map,this.cityPath);
			buildOnKingCity.isCompleted();
			return true;
		case 4:
			setNewCouncillorOnBalcon=new ControlSetNewCouncillorOnBalcon(this.agentPlayer,this.council,this.newCouncillor);
			setNewCouncillorOnBalcon.isCompleted();
			return true;
		default:
			return false;
		}
	}	
}