package lm_57.server.controller.action;

import java.awt.Color;

import java.rmi.RemoteException;
import java.util.List;

import lm_57.server.model.Player;
import lm_57.server.model.action.PayAssistantForMainAction;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class ControlPayAssistantForMainAction{
	
	private Player agentPlayer;
	private PayAssistantForMainAction payAssistantForMainAction;
	private ControlMainAction mainAction;
	
	public ControlPayAssistantForMainAction(Player agentPlayer,Map map,String command,Region region,City city,List<String> cityPath,Color color,int indexRegionPermitCard,PermitCard permitCard){
		this.agentPlayer=agentPlayer;
		payAssistantForMainAction=new PayAssistantForMainAction(this.agentPlayer);
		this.mainAction=new ControlMainAction(this.agentPlayer,map,command,region,city,cityPath,color,indexRegionPermitCard,permitCard);
	}
	
	public boolean isCompleted() throws RemoteException{
		if(checkYourAssistant(3)){
			payAssistantForMainAction.isCompleted();
			mainAction.isCompleted();
			return true;
		}
		return false;
	}
	
	private boolean checkYourAssistant(int numberAssistant){
		if(this.agentPlayer.getPlayerAssistant()>=numberAssistant){
			return true;
		}
		else{
			return false;
		}
	}
}
