package lm_57.server.controller.action;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import lm_57.server.model.Constants;
import lm_57.server.model.Player;
import lm_57.server.model.action.BuildEmporiumByPermitCard;
import lm_57.server.model.bonus.BonusColor;
import lm_57.server.model.bonus.BonusRegion;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.Emporium;
import lm_57.server.model.utility.PermitCard;

public class ControlBuildEmporiumByPermitCard{
	
	private Player agentPlayer;
	private PermitCard permitCard;
	private List<PermitCard> permitCardToRemove=new ArrayList<>();
	private City city;
	private List<City> prevCity=new ArrayList<>();
	private List<City> cityOnMap=new ArrayList<>();	/*to initialize by map*/
	private List<City> cityColor=new ArrayList<>();
	private Region region;
	private String regionName;
	private Map map;
	private BonusRegion bonusRegion;
	private BonusColor bonusColor;
	private BuildEmporiumByPermitCard buildEmporiumByPermitCard;
	
	public ControlBuildEmporiumByPermitCard(Player agentPlayer,Map map,PermitCard permitCard,City city){
		this.agentPlayer=agentPlayer;
		this.map=map;
		this.permitCard=permitCard;
		this.city=city;
		

	}
	public Player getPlayer (){
		return this.agentPlayer;
		
	}
	public City getCity(){
		return this.city;
	}
	
	
	public boolean isCompleted() throws RemoteException{
		for(PermitCard c:this.agentPlayer.getPlayerPermitCard()){
			if(c.equals(this.permitCard) && this.permitCard.getCityAllowed().contains(this.city.getName())){
				this.buildEmporiumByPermitCard=new BuildEmporiumByPermitCard(this.agentPlayer,this.permitCard,this.city);
				buildEmporiumByPermitCard.isCompleted();
				this.permitCardToRemove.add(this.permitCard);
				this.agentPlayer.removePlayerPermitCard(this.permitCardToRemove);
				getBonusFromNextCity(this.city.getLinkedCity(),this.prevCity);
				checkEmporiumOnRegion();
				checkEmporiumOnCity();
				return true;
			}
		}
		return false;
	}
	
	private void getBonusFromNextCity(List<City> linkCity, List<City> prevCity){
		if(prevCity.isEmpty()){
			for(City c: linkCity){
				for(Emporium e: c.getEmporium()){
					if(e.getColor().equals(agentPlayer.getPlayerColor())){
						agentPlayer.getRelativeBonus(c.getBonus());
						this.prevCity.add(c);
						getBonusFromNextCity(c.getLinkedCity(),this.prevCity);
					}
				}
			}
			return;
		}
		for(City c: linkCity){
			if(!(this.prevCity.contains(c))){
				for(Emporium e: c.getEmporium()){
					if(e.getColor().equals(agentPlayer.getPlayerColor())){
						agentPlayer.getRelativeBonus(c.getBonus());
					}
				}
				this.prevCity.add(c);
			}
			getBonusFromNextCity(c.getLinkedCity(),this.prevCity);
		}
	}
	
	private void checkEmporiumOnRegion() throws RemoteException{
		this.regionName=this.city.getRegionName();

		for(Region r:map.getRegionList()){
			if(r.getName().equals(regionName)){
				region=r;
			}
		}
		int tmp=0;
		for(City c:region.getCities()){
			for(Emporium e:c.getEmporium()){
				if(e.getColor().equals(agentPlayer.getPlayerColor())){
					tmp++;
				}
			}
		}
		if(tmp==Constants.NUM_CITY_FOR_REGION){
			bonusRegion.getBonus(agentPlayer, this.region);
		}
	}
	
	private void checkEmporiumOnCity(){
		
		for(City c:cityOnMap){
			if(c.getColor().equals(this.city.getColor())){
				cityColor.add(c);
			}
		}
		int tmp=0;
		for(City i:cityColor){
			for(Emporium e:i.getEmporium()){
				if(e.getColor().equals(this.agentPlayer.getPlayerColor())){
					tmp++;
				}
			}
		}
		if(cityColor.size()==tmp){
			this.bonusColor.getBonus(this.agentPlayer,this.city.getColor());//not introduced
		}
	}
	
}
