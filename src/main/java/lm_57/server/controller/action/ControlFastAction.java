package lm_57.server.controller.action;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.map.King;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.Councillor;
import lm_57.server.model.utility.PermitCard;

public class ControlFastAction extends ControlAction{
	
	private Player agentPlayer;
	private Region region;
	private int numAction;
	private City city;
	private Councillor newCouncillor=new Councillor();
	private String command;
	private CouncilBalcon council;
	private Color color;
	private Map map;
	private King king;
	private List<String> cityPath;
	private int indexRegionPermitCard;
	private PermitCard permitCard;
	
	/*fast action*/
	private ControlBuyAssistant buyAssistant;
	private ControlPayAssistantForChangePermitCard payAssistantForChangePermitCard;
	private ControlElectedCouncilor electedCouncilor;
	private ControlPayAssistantForMainAction payAssistantForMainAction;
	
	public ControlFastAction(Player agentPlayer,Map map,String command,Region region,City city,List<String> cityPath,Color color,int indexRegionPermitCard,PermitCard permitCard){
		this.agentPlayer=agentPlayer;
		this.map=map;
		try{
			this.council=new CouncilBalcon();
		}
		catch(RemoteException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"Can't generate council");
		}
		String[] commands=command.split("-");
		numAction=Integer.parseInt(commands[0]);
		if(!cityPath.isEmpty()){
			this.cityPath=cityPath;
		}
		if(!(permitCard==null)){
			this.permitCard=permitCard;
		}
		this.command=command;
		if(!(region==null)){
			this.region=region;
		}
		if(!(city==null)){
			this.city=city;
		}
		this.king=map.getKing();
		if(!(color==null)){
			this.color=color;
		}
		if(!(indexRegionPermitCard==0)){
			this.indexRegionPermitCard=indexRegionPermitCard;
		}
		if(command.contains("7") && (region!=null)){
			this.council=region.getCouncilBalcon();
		}
		if(command.contains("7") && command.contains("King")){
			for(Councillor c:king.getCouncillorOnBalcon()){
				this.council.setCouncillorOnBalcon(c);
			}
		}
		if(command.contains("7")){
			this.newCouncillor.getCouncillor();
			this.newCouncillor.setCouncillorColor(this.color);
		}
	}
	
	@Override
	public boolean isCompleted() throws RemoteException{
		return chooseYourFastAction();
	}
	
	@Override
	public String showMeActions(){
		return "[BuyAssistan] [PayAssistantForChangePermitCard] [ElectedCouncilor] [PayAssistantForMainAction]";
	}
	
	private boolean chooseYourFastAction() throws RemoteException{
		switch(numAction){
		case 5:
			buyAssistant=new ControlBuyAssistant(this.agentPlayer);
			buyAssistant.isCompleted();
			return true;
		case 6:
			payAssistantForChangePermitCard=new ControlPayAssistantForChangePermitCard(this.region,this.agentPlayer);
			payAssistantForChangePermitCard.isCompleted();
			return true;
		case 7:
			electedCouncilor=new ControlElectedCouncilor(this.council,this.newCouncillor,this.agentPlayer);
			electedCouncilor.isCompleted();
			return true;
		case 8:
			payAssistantForMainAction=new ControlPayAssistantForMainAction(this.agentPlayer,this.map,this.command,this.region,this.city,this.cityPath,this.color,this.indexRegionPermitCard,this.permitCard);
			payAssistantForMainAction.isCompleted();
			return true;
		default:
			return false;
		}
	}
	
	
}
