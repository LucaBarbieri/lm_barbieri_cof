package lm_57.server.controller.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.action.ElectedCouncilor;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.Councillor;

public class ControlElectedCouncilor {
	
	private Player agentPlayer;
	private CouncilBalcon council;
	private Councillor newCouncillor;
	private ElectedCouncilor electedCouncilor;
	
	public ControlElectedCouncilor(CouncilBalcon council,Councillor newCouncillor,Player agentPlayer){
		this.agentPlayer=agentPlayer;
		this.council=council;
		this.newCouncillor=newCouncillor;
	}
	
	public boolean isCompleted() throws RemoteException{
		if(checkYourAssistant(1)){
			this.electedCouncilor=new ElectedCouncilor(this.agentPlayer,this.council,this.newCouncillor);
			electedCouncilor.isCompleted();
			return true;
		}
		return false;
	}
	
	private boolean checkYourAssistant(int numberAssistant){
		if(this.agentPlayer.getPlayerAssistant()>=numberAssistant){
			return true;
		}
		else{
			return false;
		}
	}
}
