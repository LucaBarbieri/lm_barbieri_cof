package lm_57.server.controller.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.action.PayAssistantForChangePermitCard;
import lm_57.server.model.map.Region;

public class ControlPayAssistantForChangePermitCard {
	
	private Player agentPlayer;
	private Region region;
	private PayAssistantForChangePermitCard payAssistantForChangePermitCard;
	
	public ControlPayAssistantForChangePermitCard(Region region,Player agentPlayer){
		this.agentPlayer=agentPlayer;
		this.region=region;
	}
	
	public boolean isCompleted() throws RemoteException{
		if(checkYourAssistant(1)){
			this.payAssistantForChangePermitCard=new PayAssistantForChangePermitCard(this.agentPlayer,this.region);
			payAssistantForChangePermitCard.isCompleted();
			return true;
		}
		return false;
	}
	
	private boolean checkYourAssistant(int numberAssistant){
		if(this.agentPlayer.getPlayerAssistant()>=numberAssistant){
			return true;
		}
		else{
			return false;
		}
	}
}
