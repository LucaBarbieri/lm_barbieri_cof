package lm_57.server.controller.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuyPermitCard;
import lm_57.server.model.map.Region;

public class ControlBuyPermitCard {
	
	private Player agentPlayer;
	private Region region;
	private BuyPermitCard buyPermitCard;
	private int indexPermitCard;
	
	public ControlBuyPermitCard(Player agentPlayer,Region region,int indexPermitCard){
		this.agentPlayer=agentPlayer;
		this.region=region;
		this.indexPermitCard=indexPermitCard;
	}
	
	public boolean isCompleted() throws RemoteException{
		if((this.agentPlayer.getPlayerCoin())>=(this.region.getCouncilBalcon().coinPoliticCard(agentPlayer.getPoliticCard()))){
			this.buyPermitCard=new BuyPermitCard(this.agentPlayer,this.region,this.indexPermitCard);
			buyPermitCard.isCompleted();
			return true;
		}
		return false;
	}
}
