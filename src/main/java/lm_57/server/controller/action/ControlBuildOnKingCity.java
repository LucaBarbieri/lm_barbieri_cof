package lm_57.server.controller.action;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuildOnKingCity;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;

public class ControlBuildOnKingCity {
	
	private Player agentPlayer;
	private City destination;
	private City kingCity;
	private Map map;
	private List<String> cityPath=new ArrayList<>();	/*to initialize by command*/
	private BuildOnKingCity buildOnKingCity;
	
	public ControlBuildOnKingCity(Player agentPlayer,City destination,Map map,List<String> cityPath){
		this.agentPlayer=agentPlayer;
		this.destination=destination;
		this.map=map;
		this.cityPath=cityPath;
		this.kingCity=map.getKing().getCurrentCity();
	}
	
	
	public Player getPlayer(){
		return this.agentPlayer;
	}
	public City getCity(){
		return this.destination;
		
	}
	public List<String> getCityPath(){
		return this.cityPath;
	}
	//Only for test
	
	
	public boolean isCompleted() throws RemoteException{
		int tmpKingPath=(checkYourKingPath(this.cityPath));
		if(tmpKingPath>=0){
			this.buildOnKingCity=new BuildOnKingCity(this.agentPlayer,this.destination,this.map.getKing());
			buildOnKingCity.isCompleted();
			this.agentPlayer.removePlayerCoin(tmpKingPath);
			return true;
		}
		return false;
	}
	
	private int checkYourKingPath(List<String> cityPath){
		int tmp=0;
		if(cityPath==null)
			return -1;
		if(cityPath.isEmpty()){
			tmp=0;
		}
		
		if(cityPath!=null && !cityPath.isEmpty()){
			for(String str:cityPath){
				for(City c:this.kingCity.getLinkedCity()){
					if(c.getName().equals(str)){
						tmp++;
					}
				}
				
			}
			}
		
	if(tmp==cityPath.size() && this.agentPlayer.getPlayerCoin()>=(tmp*2)){
			return (tmp*2);
		}
	else
		
			return -1;
	
	}
	}


