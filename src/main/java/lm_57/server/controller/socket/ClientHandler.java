package lm_57.server.controller.socket;
 
import java.awt.Color;
import java.util.List;
 
import lm_57.server.controller.User;
import lm_57.server.model.Constants;

import static lm_57.server.model.Constants.MAX_PLAYERS;
import static lm_57.server.model.Constants.MAXNUMBONUS;
import static lm_57.server.model.Constants.NUMBEROFMAP;
 
 
public class ClientHandler extends Thread{
 
 
    private PlayGame game;
	private int numberOfPlayers=0;
	private int numberOfMap=0;
	private int numberOfBonus=0;
	private String playerName;
	private SocketServer server;
	private SocketConnection socket;
 
	public ClientHandler(SocketConnection socket,SocketServer server){
 
		this.socket=socket;
		this.server=server;
	}
	
	@Override
	public void run(){
		login();
		if(server.getFlagNumPlayer()){
           socket.sendMessage("true");
			firstPlayerInitialization();
		}
		else{
 			socket.sendMessage("false");
		}
		play();
	}
 
	public void login(){
 			boolean input;
			while(true){  		
				String username=socket.receiveMessage();
				String colorPlayer=socket.receiveMessage();
				input=checkColorPlayer(colorPlayer);
				if(input){
					socket.sendMessage("true");
					socket.sendMessage("Identification successful");
					playerName=username;
					server.addServerUser(new User(playerName,colorPlayer));
					break;
				}
				else{
					socket.sendMessage("false");
					socket.sendMessage("Identification unsuccesful");
				}
		}
	}
 
	private boolean checkColorPlayer(String color){
		boolean check=true;
        List<User> usersTmp;
        Color colorConverted=Constants.convertStringToColor(color);
          if(colorConverted==null)
        	  return false;
		if(!server.getGeneralServer().getUsers().isEmpty()){
			usersTmp=server.getGeneralServer().getUsers();
			for(User tmp:usersTmp){
				if(tmp.checkColor(color)){
					check=false;
				}
			}
		}
		return check;
 
	}
 
	private void firstPlayerInitialization(){
		socket.sendMessage("You are the first player");
		firstPlayerSettings();              
 
	}
 
	private void firstPlayerSettings(){
		int numPlayers=0;
		int numMap=0;
		int numBonus=0;
			while(true){
				socket.sendMessage("Set the number of players,the number of map,the number of bonus:");
				numPlayers=Integer.parseInt(socket.receiveMessage());
				numMap=Integer.parseInt(socket.receiveMessage());
				numBonus=Integer.parseInt(socket.receiveMessage());
				if(controlInteger(numPlayers,MAX_PLAYERS)&&controlInteger(numMap,NUMBEROFMAP)&&controlInteger(numBonus,MAXNUMBONUS)){
					server.setNumBonus(numBonus);
					server.setNumMap(numMap);
					server.setNumPlayers(numPlayers);
					socket.sendMessage("true");
					socket.sendMessage("Success");
					break;
				}
				socket.sendMessage("false");
				socket.sendMessage("Input not valid");
			}
 
		this.numberOfBonus=numBonus;
		this.numberOfMap=numMap;
		this.numberOfPlayers=numPlayers;
 
	}
 
 
 
 
	public boolean controlInteger(int toControl,int controller){
 
		return toControl<=controller&&toControl>0;
 
 
	}
	public int getNumPlayer(){
 
		return this.numberOfPlayers;
	}
	
	public int getNumMap(){
		return this.numberOfMap;
	}
 
	public int getNumBonus(){
		return this.numberOfBonus;
	}
	public String getNamePlayer(){
		return this.playerName;
	}
	public void play(){
		socket.sendMessage("Welcome to the game");
		game=new PlayGame(socket,server,playerName);
		game.play();
	}
}