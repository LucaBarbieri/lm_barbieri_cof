package lm_57.server.controller.socket;
 
 
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
 
import lm_57.server.model.Constants;
import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.King;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;
 
 
public class ControlCommandAction extends ControlCommand{
		
		private Map map;
		private List<Player> players=new ArrayList<>();
		private Player currentPlayer;
		private Color colorChose;
		private PermitCard playerPermitCard;
		private List<String> cityPath=new ArrayList<>();
        private King king;
		private int indexRegion;
		private Region region;
		private String kingString;
		
 		public ControlCommandAction(Map map,Queue<Player> players){
			this.map=map;
			this.players.addAll(players);
		}
 		
		public String getStringKing(){
			return this.kingString;
		}
 		
		public Player getCurrentPlayer() {
			return currentPlayer;
		}
 
        public Region getRegion(){
        	return region;
        }
		
		public Color getColorChose() {
			return colorChose;
		}
 
 
 
        public PermitCard getPermitCard(){
        	return playerPermitCard;
        }
 
 
 
		public List<String> getCityPath() {
			return cityPath;
		}
 
 
 
		public King getKing() {
			return king;
		}
 
 
		public int getIndexRegion() {
			return indexRegion;
		}
		
		@Override 
 		 public boolean controlPrototypeCommand(String command,String namePlayer){
			       this.currentPlayer=foundThePlayer(namePlayer);
	               String[] tmp=command.split("-");
	               boolean  control;
	               control=controlAction(tmp);
	               return control;
		 } 
		 
		 private boolean controlAction(String[] command){
			 boolean control;
			 int valueAction=Integer.parseInt(command[0]);
			 String[] tmp=command[1].split("_");
			 if(valueAction<=4){
			 switch(valueAction){
			 case 1:
				  control=controlOptionActionOne(tmp);
				  break;
			 case 2:
				 control=controlOptionActionTwo(tmp);
				 break;
			 case 3:
				 control=controlOptionActionThree(tmp);
				 break;
			 case 4:
				 control=controlOptionActionFour(tmp);
				 break;
			 default:
				 control=false;
				 break;
			 }
		 }
			 else{
				 control=controlPrototypeFastAction(valueAction,tmp);
			 }
			 if(control){
				 super.setNumAction(valueAction);
			 }
			 return control;
		 }
		 
		 private boolean controlPrototypeFastAction(int fastAction,String[] command){
			 boolean tmp;
			 switch(fastAction)
			 {
			 case 5:
				 tmp=true;
				 break;
			 case 6:
				 tmp=controlRegion(command[0]);
				 break;
			 case 7:
				 tmp=controlOptionActionFour(command);
				 break;
			 case 8:
				 tmp=true;
				 break;
			default:
				 tmp=false;
					 break;
			 }
			 if(tmp){
				 super.setNumAction(fastAction);
			 }
			 return tmp;
		 }
			  
		 private boolean controlOptionActionOne(String[] controlling){
			    boolean cn;
			    cn=controlCity(controlling[0]);
			    if(cn)
			    	cn=controlIndexPlayerPermitCard(controlling[1]);
			    return cn;
		 }
		 
		 private boolean controlOptionActionTwo(String[] parameter){
			   boolean tmp;
			   tmp=controlRegion(parameter[0]);
			   if(tmp){
				    tmp=controlIndexRegion(parameter[1]);
			   }
			   return tmp;
		 }
		 
		 private boolean controlIndexPlayerPermitCard(String par){
			 int tmp=Integer.parseInt(par);
			 if(tmp<currentPlayer.getPlayerPermitCard().size()){
				this.playerPermitCard=currentPlayer.getPlayerPermitCard().get(tmp);
				return true;
			 }
			 else{
				 return false;
			 }
			 
		 }
		 
		 private boolean controlOptionActionThree(String[] param){
			 return controlCity(param[0]);
		 }
		 
		 private boolean controlOptionActionFour(String[] param){
			 boolean controller;
			  controller=controlKing(param[0]);
			  if(controller)
				  controller=controlColor(param[1]);
			  return controller;
		 }
		 private boolean controlCity(String city){
			 List<City> cities=this.map.getCityList();
			 boolean control=false;
			 String[] commandCity=city.split(" ");
			 int length=commandCity.length;
			 int sum=0;
			 for(int i=0;i<length;i++){
			  for(City tmp:cities){
				  if(tmp.getName().equals(commandCity[i])){
					  this.cityPath.add(tmp.getName());
					  sum++;
				  }
			  }
			 }
			 if(sum==length){
				 control=true;
			 }
            return control;
		 }
		 
		 
		 private boolean controlRegion(String region){
			 List<Region> regions=this.map.getRegionList();
			 boolean indexRegionPermit=false;
			 for(Region tmpRegion:regions){
				 if(tmpRegion.getName().equals(region)){
					 this.region=tmpRegion;
					 indexRegionPermit=true;
					 break;
				 }
			 }
			 if("null".equals(region))
				 indexRegionPermit=true;
			 return indexRegionPermit;
		 }
 
		 private boolean controlIndexRegion(String com){
			 int index=Integer.parseInt(com);
			  if(index<this.map.getRegionList().size()){
				  this.indexRegion=index;
				  return true;
			  }
			  return false;
		 }
		 
		 private boolean controlKing(String king){
			 if("King".equals(king)){
                 this.king=this.map.getKing();
                 this.kingString="King";
			      return true;
			 }
			 this.kingString="null";
			 return controlRegion(king);
		 }
		 
		 private boolean controlColor(String color){
			  Color changeColor=Constants.convertStringToColor(color);
			  if(changeColor==null)
				  return false;
			  boolean change=false;
			    for(int i=0;i<Constants.getColors().length;i++){
			    	  if(changeColor.equals(Constants.getColors()[i]))
			    		  this.colorChose=Constants.getColorSupported()[i];
 			    		  change=true;
			    }
			    return change;
		 }
 
     private Player foundThePlayer(String playerName){
    	 for(Player pl:this.players){
    		 if(playerName.equals(pl.getName()))
    			 return pl;
    	 }
    	 return null;
     }
     
     public int getNumCurrentAction(){
    	 return super.getNumAction();
     }
     
     public City getCity(){
    	 if(!cityPath.isEmpty()){
    	      String tmp=cityPath.get(0);
    	      for(City c:map.getCityList()){
    	    	  if(c.getName().equals(tmp)){
    	    		  return c;
    	    	  }
    	      }
    	 }
    	      return null;
     }
     
     @Override
     public void addPlayers(List<Player> players){
    	 this.players.addAll(players);
     }
     
}