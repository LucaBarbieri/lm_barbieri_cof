package lm_57.server.controller.socket;
 
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import lm_57.server.model.Player;
import lm_57.server.model.utility.PermitCard;
import lm_57.server.model.utility.PoliticCard;
 
public class ControlCommandMarket extends ControlCommand{
	
    private Set<Player> players=new HashSet<>();
	private Player player;
	private int numAssistant;
	private int numCoin;
	private List<PoliticCard> politicCards=new ArrayList<>();
	private List<PermitCard> permitCards=new ArrayList<>();
	
	
	public ControlCommandMarket(Queue<Player> players){
		super();
		this.players.addAll(players);
	}
	
	public int getNumAssistant(){
		return this.numAssistant;
	}
	public int getNumCoin(){
		return this.numCoin;
	}
	
	public List<PermitCard> getPermitCards(){
		return this.permitCards;
	}
	public List<PoliticCard> getPoliticCard(){
		return this.politicCards;
	}
	
 	public boolean controlPrototypeCommand(String command,String namePlayer){
		this.player=foundThePlayer(namePlayer);
		String[] tmp=command.split("-");
		String[] buy=tmp[1].split("_");
		boolean market;
		int choice=Integer.parseInt(tmp[0]);
		super.setNumAction(choice);
		switch(choice){
		case 1:
			market=controlAssistant(buy[0]);
			break;
		case 2:
			market=controlCards(buy[0],1);
			break;
		case 3:
			market=controlCards(buy[0],0);
			break;
		default:
			market=false;
			break;
		}
		return market;
	}
	
	private boolean controlAssistant(String com){
		int numAssistant=Integer.parseInt(com);
		if( this.player.getPlayerAssistant()>=numAssistant){
			this.numAssistant=numAssistant;
			return true;
		}
		return false;
	}
	
	private boolean controlCards(String com,int flag){
		 String[] tmp=com.split(" ");
		 int k=0;
		 int chose;
		 boolean control=false;
		 for(int i=0;i<tmp.length;i++){
			 chose=Integer.parseInt(tmp[i]);
			 if(chose<=this.player.getPlayerPermitCard().size()&&flag==1){
				 this.permitCards.add(this.player.getPlayerPermitCard().get(chose));
				 k++;
			 }
			 if(chose<=this.player.getPoliticCard().size()&&flag==0){
				 this.politicCards.add(this.player.getPoliticCard().get(chose));
				 k++;
			 }
		 }
		 if(k==tmp.length){
			 control=true;
		 }
		 return control;
	}
	
	private Player foundThePlayer(String playerName){
		for(Player play:players){
 			if(playerName.equals(play.getName())){
				return play;
			}
		}
		return null;
	}
	
	@Override
	public void addPlayers(List<Player> players){
		this.players.addAll(players);
	}
}