package lm_57.server.controller.socket;
 
 
public class PlayingGame {
	
	private ClientConnection playConnection;
	private int numRoundPassed=0;
    public PlayingGame(ClientConnection playConnection){
    	 this.playConnection=playConnection;
    }
    
    public void play(){
    	       if(Boolean.parseBoolean(playConnection.receiveSocketMessage())){
    		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    		   playTurn();
    	       }
    	       else{
    	    	   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    	       }
    }
    	       
  private void playTurn(){
    	String chose;
    	while(true){
             displayCurrentCommand();
             if(numRoundPassed==2){
            	 playConnection.closeSocket();
            	 return;
             }
    		 playConnection.sendSocketMessage(chose=playConnection.receiveKeyboardMessage());
    		 if("PlayTurn".equals(chose)){
   			  playCurrentTurn();
   		  }
   		  else if("MarketPhase".equals(chose)){
   			  marketPhase();
   		  }
   		  else if("ShowMap".equals(chose)){
   			  displayMap();
   		  }
   		  else if("Exit".equals(chose)){
   			  break;
   		  }
   		  else if("PlayersRecord".equals(chose)){
   			  displayPlayerRecords();
   		  }
    	}
    	
    }
  
  private void displayPlayerRecords(){
	       Boolean control;
	       int numPlayer=Integer.parseInt(playConnection.receiveSocketMessage());
	       for(int i=0;i<numPlayer;i++){
	    	   displayPlayers();
	    	   control=Boolean.parseBoolean(playConnection.receiveSocketMessage());
	    	   if(control){
	    		   displayPlayerPermitAndPoliticCard();
	    	   }
	       }
	  }
   private void displayPlayers(){
	   for(int i=0;i<7;i++){
		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
	   }
   }
   
   private void  displayPlayerPermitAndPoliticCard(){
	     int numPermitCard=Integer.parseInt(playConnection.receiveSocketMessage());
	     int numPoliticCard=Integer.parseInt(playConnection.receiveSocketMessage());
	     for(int i=0;i<numPermitCard;i++){
	    	 playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
	     }
	     for(int i=0;i<numPoliticCard;i++){
	    	 playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
	     }
   }

  private void displayMap(){
	  for(int k=0;k<4;k++){
		  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
	  }
  }  
  
    private void marketPhase(){
		while(true){
        displayCommandMarket();
		String command=playConnection.receiveKeyboardMessage();
		playConnection.sendSocketMessage(command);
		if("SellProduct".equals(command)){
			sellingProduct();
		}
		else if("BuyProduct".equals(command)){
			buyingProduct();
		}
		else if("Exit".equals(command)){
			break;
		}
    }
    }
		
		private void displayCommandMarket(){
			playConnection.sendVideoMessage(playConnection.receiveSocketMessage());		
		playConnection.sendVideoMessage(playConnection.receiveSocketMessage());	
		}
    
    public void displayCurrentCommand(){
    	for(int i=0;i<4;i++){
    		playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    	}
    }
    
    
    private void playCurrentTurn(){
    	boolean cont;
    	if(Boolean.parseBoolean(playConnection.receiveSocketMessage())){
	    	 playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
	    	 while(true){
	    	 playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
             cont=choseFirstAction();
             if(cont){
            	 playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
            	 choseSecondAction();
            	 break;
             }
             else{
             playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
             }
	    	 }
    }
    	else{
    		playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    	}
    }
    
    private boolean choseFirstAction(){
    	boolean control=false;
    	while(true){
    		playConnection.sendSocketMessage(playConnection.receiveKeyboardMessage());
    		if(Boolean.parseBoolean(playConnection.receiveSocketMessage())){
        	  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
        	  break;
          }
          playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    		}
    	control=Boolean.parseBoolean(playConnection.receiveSocketMessage());
    	return control;
    }
    
    	
    
    private void choseSecondAction(){
    	Boolean contr;
    	while(true){
    		if(!controlSecondAction()){
    			break;
    		}
    		contr=Boolean.parseBoolean(playConnection.receiveSocketMessage());
    			   if(contr){
    			   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    			   break;
                     }
    			   else{
    				   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    			   }
    	}
    	}
    
    private boolean controlSecondAction(){
    	String command;
    	boolean control;
       while(true){
		playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		command=playConnection.receiveKeyboardMessage();
		playConnection.sendSocketMessage(command);
		if("No".equals(command)){
			return false;
		}
		control=Boolean.parseBoolean(playConnection.receiveSocketMessage());
		if(control){
			playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
			return true;
		}
		playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		
         }
    }
   
    private void sellingProduct(){
    	Boolean market=Boolean.parseBoolean(playConnection.receiveSocketMessage());
    	Boolean state;
    	String chose;
    	if(!market){
    		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    		   return;
    		}
         state=Boolean.parseBoolean(playConnection.receiveSocketMessage());
         if(state){
  		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
  		   while(true){
  			   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
  			   chose=playConnection.receiveKeyboardMessage();
    		   playConnection.sendSocketMessage(chose);
    		   if("No".equals(chose)){
    			   return;
    		   }
    		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
    		   controlCommandMarket();
    		   break;
    		   }
  			   }
         else{
        	 playConnection.sendVideoMessage(playConnection.receiveSocketMessage()+"\n");
         }
  		   }
    
	private void controlCommandMarket(){
		boolean control;
		String chose;
		while(true){
			chose=playConnection.receiveKeyboardMessage();
			playConnection.sendSocketMessage(chose);
			control=Boolean.parseBoolean(playConnection.receiveSocketMessage());
			if(control){
	    		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
				break;
			}
 		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		}
	}
	
	private void buyingProduct(){
		String choseBuyPhase;
		int choseBuy;
		int contr;
		  if(Boolean.parseBoolean(playConnection.receiveSocketMessage())){
			  displayProductOnSale();
			  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
			  while(true){
				  playConnection.sendVideoMessage(playConnection.receiveSocketMessage()); 
			  choseBuyPhase=playConnection.receiveKeyboardMessage();
			  playConnection.sendSocketMessage(choseBuyPhase);
	 		   if("No".equals(choseBuyPhase)){
	 			   return;
	 		   }
	 		   playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
	 		   contr=Integer.parseInt(playConnection.receiveSocketMessage());
	 		   choseBuy=Integer.parseInt(playConnection.receiveKeyboardMessage());
	 		   playConnection.sendSocketMessage(String.valueOf(choseBuy));
			  if(choseBuy<contr){
				  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
				  break;
			  }
			  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		}
			  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		  }
		  else{
			  playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		  }
	}
	
	private void displayProductOnSale(){
		int numSellersAssistant=Integer.parseInt(playConnection.receiveSocketMessage());
		for(int i=0;i<numSellersAssistant;i++){
			playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		}
		int numSellersPermit=Integer.parseInt(playConnection.receiveSocketMessage());
		for(int i=0;i<numSellersPermit;i++){
			playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		}
		int numSellersPolitic=Integer.parseInt(playConnection.receiveSocketMessage());
		for(int i=0;i<numSellersPolitic;i++){
			playConnection.sendVideoMessage(playConnection.receiveSocketMessage());
		}
		
	}
}
