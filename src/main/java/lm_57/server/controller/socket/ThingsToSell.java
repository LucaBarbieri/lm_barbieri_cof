package lm_57.server.controller.socket;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.model.Player;
import lm_57.server.model.market.AssistantSeller;
import lm_57.server.model.market.PermitSeller;
import lm_57.server.model.market.PoliticSeller;
import lm_57.server.model.utility.PermitCard;
import lm_57.server.model.utility.PoliticCard;

public class ThingsToSell {

	private List<AssistantSeller> sellersAssistant;
	private List<PermitSeller> sellersPermit;
	private List<PoliticSeller> sellersPolitic;
	
	public ThingsToSell(){
		sellersAssistant=new ArrayList<>();
		sellersPermit=new ArrayList<>();
		sellersPolitic=new ArrayList<>();
	}
	
	public void addAssistantSeller(Player agentPlayer,int numAssistant,int numCoin){
		try{
		        AssistantSeller tmp=new AssistantSeller(agentPlayer,numAssistant,numCoin);
		        sellersAssistant.add(tmp);
		}
		catch(RemoteException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot instanziate the assistant seller",e);
		}
		        
	}
	
	public void addPoliticSeller(Player agentPlayer,List<PoliticCard> politicCards,int numCoin){
		try{
	        PoliticSeller tmp=new PoliticSeller(agentPlayer,politicCards,numCoin);
	        sellersPolitic.add(tmp);
	}
	catch(RemoteException e){
		Logger.getAnonymousLogger().log(Level.SEVERE,"cannot instanziate the assistant seller",e);
	}
	}
	
	public void addPermitSeller(Player agentPlayer,List<PermitCard> chosePermit,int numCoin){
		try{
	        PermitSeller tmp=new PermitSeller(agentPlayer,chosePermit,numCoin);
	        sellersPermit.add(tmp);
	}
	catch(RemoteException e){
		Logger.getAnonymousLogger().log(Level.SEVERE,"cannot instanziate the assistant seller",e);
	}
	}
	
	public List<AssistantSeller> getAssistantSeller(){
		return this.sellersAssistant;
	}
	
	public List<PoliticSeller> getPoliticSeller(){
		return this.sellersPolitic;
	}
	
	public List<PermitSeller> getPermitSeller(){
		return this.sellersPermit;
	}
}

