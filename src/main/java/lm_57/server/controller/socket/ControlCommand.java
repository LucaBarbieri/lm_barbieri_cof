package lm_57.server.controller.socket;

import java.util.List;

import lm_57.server.model.Player;

public abstract class ControlCommand {
	 
	private int numAction;
	
	public ControlCommand(){
		numAction=0;
	}
	
	public abstract boolean controlPrototypeCommand(String command,String namePlayer);
	
	public abstract void addPlayers(List<Player> players);
	
	public int getNumAction()
	{
		return this.numAction;
	}
	
	public void setNumAction(int numAction){
		this.numAction=numAction;
	}
}
