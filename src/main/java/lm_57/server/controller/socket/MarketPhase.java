package lm_57.server.controller.socket;

public class MarketPhase {
	
	private SocketConnection playConnection;
	private ControlCommandMarket controlMarket;
	private SocketServer server;
	private String playerName;
	
	public MarketPhase(SocketConnection playConnection,SocketServer server,String playerName){
		this.playConnection=playConnection;
		this.server=server;
		this.playerName=playerName;
	}
	
	public void startMarketPhase(){
		while(true){
			displayCommandMarket();
		String command=playConnection.receiveMessage();
		if("SellProduct".equals(command)){
			sellingProduct();
		}
		else if("BuyProduct".equals(command)){
			buyingProduct();
		}
		else if("Exit".equals(command)){
			break;
		}
		
	}
	}
	
	private void displayCommandMarket(){
		playConnection.sendMessage("SellProduct");
		playConnection.sendMessage("BuyProduct");
	}
	
	private void sellingProduct(){
		Boolean bool;
		String choseSellPhase;
		Boolean market;
		Boolean state;
		market=(server.getCurrentGameBoard().getStateGame().getCurrentRound().isCompleted());
		playConnection.sendMessage(String.valueOf(market));
		if(!market){
			playConnection.sendMessage("Market stage not started");
			return;
		}
		server.getCurrentGameBoard().getStateGame().setNextState();
		state=server.getCurrentGameBoard().getStateGame().getCurrentMarket().getCurrentAgent().getName().equals(playerName);
		playConnection.sendMessage(String.valueOf(state));
		if(state){
			playConnection.sendMessage("Is your market turn:");
			while(true){
				playConnection.sendMessage("Do you want sell anything");
				choseSellPhase=playConnection.receiveMessage();
				if("No".equals(choseSellPhase)){
					setNextMarketTurn();
					return;
				}
			playConnection.sendMessage("Choose a thing to sell:");
			this.controlMarket=server.getCommandMarketController();
			bool=controlCommandMarket();
			if(bool){
				addThingsIntoSelling();
			    setNextMarketTurn();
			break;
			}
		}
	}
		else {
			playConnection.sendMessage("Is not your market turn");
		}
	}
	
	private void setNextMarketTurn(){
		server.getCurrentGameBoard().getStateGame().getCurrentMarket().getSell().getCurrentTurn().removeAction();
	    server.getCurrentGameBoard().getStateGame().setNextState();
	}
	
	private void addThingsIntoSelling(){
		 ThingsToSell sell=server.getThingsToSell();
		 int numMarket=controlMarket.getNumAction();
		 switch(numMarket){
		 case 1:
			 sell.addAssistantSeller(server.getCurrentGameBoard().getCurrentPlayerTurn(),controlMarket.getNumAssistant(),this.controlMarket.getNumCoin());
			 break;
		 case 2:
			 sell.addPermitSeller(server.getCurrentGameBoard().getCurrentPlayerTurn(),controlMarket.getPermitCards(),this.controlMarket.getNumCoin());
			 break;
		 case 3:
			 sell.addPoliticSeller(server.getCurrentGameBoard().getCurrentPlayerTurn(),controlMarket.getPoliticCard(),controlMarket.getNumCoin());
			 break;
		default:
			break;
		 }
		 
	}
	
	private boolean controlCommandMarket(){
		boolean control;
		while(true){
			String chose=playConnection.receiveMessage();
			control=this.controlMarket.controlPrototypeCommand(chose,playerName);
			playConnection.sendMessage(String.valueOf(control));
			if(control){
				playConnection.sendMessage("Command market valid");
				break;
			}
			playConnection.sendMessage("Command market  not valid");
		}
		return control;
	}
	
	
	private void buyingProduct(){
		int choseBuy;
		Boolean bol;
		String choseBuyPhase;
		int maxThingsToSell=server.getThingsToSell().getAssistantSeller().size()+
				            server.getThingsToSell().getPermitSeller().size()+
				            server.getThingsToSell().getPoliticSeller().size();
		if(server.getCurrentGameBoard().getStateGame().getCurrentMarket().getSell().isCompleted()){
		bol=server.getCurrentGameBoard().getStateGame().getCurrentMarket().getBuy().getCurrentAgent().equals(playerName);
		playConnection.sendMessage(String.valueOf(bol));
		if(bol){
			 displayProductSale();
			 playConnection.sendMessage("Is your market buy turn");
			 while(true){
		     playConnection.sendMessage("Do you want buy anything");
		     choseBuyPhase=playConnection.receiveMessage();
		     if("No".equals(choseBuyPhase))
		    	 return;
			 playConnection.sendMessage("Chose a thing to buy:");
			 playConnection.sendMessage(String.valueOf(maxThingsToSell));
			 choseBuy=Integer.parseInt(playConnection.receiveMessage());
			 if(choseBuy<maxThingsToSell){
				 playConnection.sendMessage("Command buy market valid");
				 break;
			 }
			 playConnection.sendMessage("Command buy market not valid");
			 }
			 buyProduct(choseBuy);
			 playConnection.sendMessage("Buy succesfull");
			 server.getCurrentGameBoard().getStateGame().getCurrentMarket().getBuy().setNextTurn();
		}
		}
		else{
			playConnection.sendMessage("Buy stage not started");
		}		   
	}
	
	private void buyProduct(int choseBuy){
		int numAssistantBuy=server.getThingsToSell().getAssistantSeller().size();
		int numPermitBuy=server.getThingsToSell().getPermitSeller().size();
		int numPoliticBuy=server.getThingsToSell().getPoliticSeller().size();
		if(choseBuy<numAssistantBuy){
			server.getThingsToSell().getAssistantSeller().get(choseBuy).sellingProduct(server.getCurrentGameBoard().getCurrentPlayerTurn());
		}
		else if(choseBuy<numPermitBuy+numAssistantBuy){
			server.getThingsToSell().getPermitSeller().get(choseBuy).sellingProduct(server.getCurrentGameBoard().getCurrentPlayerTurn());
		}
		else if(choseBuy<numPermitBuy+numAssistantBuy+numPoliticBuy){
			server.getThingsToSell().getPoliticSeller().get(choseBuy).sellingProduct(server.getCurrentGameBoard().getCurrentPlayerTurn());
		}
	}
	
	private void displayProductSale(){
		     ThingsToSell things=server.getThingsToSell();
		     playConnection.sendMessage(String.valueOf(things.getAssistantSeller().size()));
		     for(int i=0;i<things.getAssistantSeller().size();i++){
		    	 playConnection.sendMessage(things.getAssistantSeller().get(i).toString());
		     }
		     playConnection.sendMessage(String.valueOf(things.getPermitSeller().size()));
		     for(int i=0;i<things.getPermitSeller().size();i++){
		    	 playConnection.sendMessage(things.getPermitSeller().get(i).toString());
		     }
		     playConnection.sendMessage(String.valueOf(things.getPoliticSeller().size()));
		     for(int i=0;i<things.getPoliticSeller().size();i++){
		    	 playConnection.sendMessage(things.getPoliticSeller().get(i).toString());
		     }
		
	}
}

