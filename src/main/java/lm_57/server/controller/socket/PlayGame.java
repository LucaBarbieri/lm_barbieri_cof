package lm_57.server.controller.socket;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lm_57.server.controller.action.ControlAction;
import lm_57.server.controller.action.ControlFastAction;
import lm_57.server.controller.action.ControlMainAction;
import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class PlayGame {

	private SocketConnection connection;
	private String command=new String();
	private String commandAction=new String();
	private String commandMarket=new String();
	private SocketServer server;
	private String playerName;
	private ControlCommandAction controlAction;
	private MarketPhase phaseMarket;
	private Timer timerActions=new Timer();
	private ActionPerformed actionController;
	private ControlAction actionMainController;
	private int numRoundPassed=0;
	private boolean anotherMainAction=false;


	public PlayGame(SocketConnection connection,SocketServer server,String playerName){
		this.connection=connection;
		this.server=server;
		this.playerName=playerName;
		this.command=constructCommand();
		this.commandMarket=contructCommandMarket();
		this.commandAction=constructCommandAction();

	}

	private String constructCommandAction() {
		return"1)BuildEmporiumByPermitCard-City_IndexPlayerPermitCard-\n"+
				"2)BuyPermitCard-Region_IndexRegionPermitCard-\n"+
				"3)BuildWithKing-ListCities-\n"+
				"4)SetNewCouncillorOnBalcon-King or Region_Color-\n"+
				"5)BuyAssistant-   -\n"+
				"6)PayAssistantForCHangePermitCard-Region-\n"+
				"7)ElectedCouncillor-King or Region_Color\n"+
				"8)PayAssistantForNewMainAction-    -";
	}

	private String contructCommandMarket() {
		return"1)Assistant-NumAssistant_CoinToBuy-\n"+
				"2)PoliticCard-Politic_CoinToBuy-\n"+
				"3)PermitCard-Permit_CoinToBuy-";

	}

	public void play(){
		String comm;
		connection.sendMessage(this.command);
		while(true){
			comm=connection.receiveMessage();
			if(numRoundPassed==2){
				connection.closeSocket();
				return;
			}
			if("Play".equals(comm))
				playTurn();
			if("CommandAction".equals(comm)){
				connection.sendMessage(this.commandAction);
			}
			else if("CommandMarket".equals(comm)){
				connection.sendMessage(this.commandMarket);
			}
			else if("Exit".equals(comm)){
				break;
			}
		}
	}

	private void chosePlayOrMarket(){
		while(true){
			connection.sendMessage("PlayTurn");
			connection.sendMessage("MarketPhase");
			connection.sendMessage("ShowMap");
			connection.sendMessage("PlayersRecord");
			if(numRoundPassed==2){
				connection.closeSocket();
				return;
			}
			String chose=connection.receiveMessage();
			if("PlayTurn".equals(chose)){
				playCurrentRound();
			}
			else if("Exit".equals(chose)){
				break;
			}
			else if("PlayersRecord".equals(chose)){
                   displayPlayersRecord();
			}
			else if("MarketPhase".equals(chose)){
				phaseMarket=new MarketPhase(this.connection,this.server,this.playerName);
				phaseMarket.startMarketPhase();
			}
			else if("ShowMap".equals(chose)){
				displayMap();
			}
		}
	}
	
	private void displayPlayersRecord(){
		Boolean control;
		List<Player> listPlayers=new ArrayList<>();
		listPlayers.addAll(server.getCurrentGameBoard().getPlayers());
		connection.sendMessage(String.valueOf(listPlayers.size()));
		for(int i=0;i<listPlayers.size();i++){
			   connection.sendMessage(listPlayers.get(i).toString());
			   control=listPlayers.get(i).getName().equals(server.getCurrentGameBoard().getCurrentPlayerTurn().getName());
			   connection.sendMessage(String.valueOf(control));
			   if(control){
				   connection.sendMessage(String.valueOf(listPlayers.get(i).getPlayerPermitCard().size()));
				   connection.sendMessage(String.valueOf(listPlayers.get(i).getPoliticCard().size()));
				   connection.sendMessage(listPlayers.get(i).displayPermitCard());
				   connection.sendMessage(listPlayers.get(i).displayPoliticCard());
			   }
		}
	}
	
	public void displayMap(){
		connection.sendMessage(server.getCurrentGameBoard().getMap().displayMap());
	}

	public void playTurn(){
		connection.sendMessage(String.valueOf(server.getStateTimer()));
		if(server.getStateTimer()){
			connection.sendMessage("Game started");
			chosePlayOrMarket();
		}
		else{
			connection.sendMessage("Waiting for inizialzation game");
		}
	}

	private void playCurrentRound(){
		boolean control;
		boolean controlCurrentAction;
		control=playerName.equals(server.getCurrentGameBoard().getStateGame().getCurrentRound().getCurrentAgent().getName());
		connection.sendMessage(String.valueOf(control));
		if(control)
		{
			connection.sendMessage("Is your turn:");
			while(true){
				connection.sendMessage("Choose a action:");
				this.controlAction=server.getCommandActionController();
				controlCurrentAction=controlCommandAction();
				connection.sendMessage(String.valueOf(controlCurrentAction));
				if(controlCurrentAction){
					connection.sendMessage("Action successful");
					choseSecondAction();
					if(!server.getCurrentGameBoard().getStateGame().getCurrentRound().isCompleted()){
					server.getCurrentGameBoard().getStateGame().getCurrentRound().setNextTurn();
					}
					break;
				}
				else{
					connection.sendMessage("Action insuccessful");
				}
			}
			}

		else {
			connection.sendMessage("Is not your turn");
		}
	}


	private String constructCommand(){		
		return"Play-->play the game\n"+
				"CommandAction-->display the prototype of the action\n"+
				"CommandMarket-->display the prototype of the market";
	}

	private boolean controlCommandAction(){
		String chose;
		boolean control;
		while(true){
			chose=connection.receiveMessage();
			control=this.controlAction.controlPrototypeCommand(chose,this.playerName);
			connection.sendMessage(String.valueOf(control));
			if(control){
				connection.sendMessage("Command valid");
				break;
			}
			connection.sendMessage("Command not valid");
		}
		return playAction(chose);
	}

	private boolean playAction(String command){
		int numAction;
		boolean con=false;
		numAction=controlAction.getNumCurrentAction();
		if(numAction==8){
			anotherMainAction=true;
		}
		instanziateControllerAction(numAction,command);
		con=actionController.actionPerformed();
		return con;
	}

	private void instanziateControllerAction(int numAction,String command){
		Map tmpMap=server.getGeneralServer().getCurrentGame().getMap();
		Player tmpPlayer=server.getGeneralServer().getCurrentGame().getCurrentPlayerTurn();
		Region tmpRegion=controlAction.getRegion();
		Color colorTmp=controlAction.getColorChose();
		List<String> tmpCityPath=controlAction.getCityPath();
		int indexRegion=controlAction.getIndexRegion();
		PermitCard permitCards=controlAction.getPermitCard();
		City tmpCity=controlAction.getCity();
		if(numAction<=4){
			this.actionMainController=new ControlMainAction(tmpPlayer,tmpMap,command,tmpRegion,tmpCity,tmpCityPath,colorTmp,indexRegion,permitCards);
		}
		else{
			this.actionMainController=new ControlFastAction(tmpPlayer,tmpMap,command,tmpRegion,tmpCity,tmpCityPath,colorTmp,indexRegion,permitCards);
		}
		this.actionController=new ActionPerformed(this.actionMainController);
	}

	private void choseSecondAction(){
		boolean contr;
		while(true){
			if(!controlSecondAction()){
				break;
			}
			if(this.anotherMainAction){
				
			}
			contr=actionController.actionPerformed();
			connection.sendMessage(String.valueOf(contr));
			if(contr){
				connection.sendMessage("Second action performed");
				removePlayerMove();
				break;
			}
				connection.sendMessage("Second action not performed");
		}
	}

	private void removePlayerMove(){
		if(server.getCurrentGameBoard().getStateGame().getCurrentRound().getCurrentTurn().getNumberFastAction()==0){
			server.getCurrentGameBoard().getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
		}
		else{
			server.getCurrentGameBoard().getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
		}
	}
	private boolean controlSecondAction(){
		String action;
		boolean contr;
		while(true){
			if(this.controlAction.getNumCurrentAction()>4){
				server.getCurrentGameBoard().getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
				connection.sendMessage("Chose a main action:");
			}
			else{
				server.getCurrentGameBoard().getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
				connection.sendMessage("Chose a fast action:");
			}
			action=connection.receiveMessage();
			if("No".equals(action)){
				removePlayerMove();
				server.getCurrentGameBoard().getStateGame().getCurrentRound().setNextTurn();
				return false;
			}
			contr=this.controlAction.controlPrototypeCommand(action,playerName);
            connection.sendMessage(String.valueOf((contr)));
            if(this.controlAction.getNumAction()==8){
            	anotherMainAction=true;
            }
			if(contr){
				connection.sendMessage("Command Valid");
				instanziateControllerAction(controlAction.getNumCurrentAction(),action);
				return true;
			}
             connection.sendMessage("Command Not Valid");
			}
		}
	

	private void timerStart(){
		timerActions.schedule(new TimerTask(){
			@Override
			public void run(){
				server.getCurrentGameBoard().getStateGame().getCurrentRound().setNextTurn();
			}
		},20000);
	}

}