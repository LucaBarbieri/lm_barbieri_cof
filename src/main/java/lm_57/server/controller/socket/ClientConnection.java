package lm_57.server.controller.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientConnection {
	
	private static final String ADDRESSSERVER="localhost";
	private static final int PORT=63005;

	private Socket socket;
	private BufferedReader inKey;
	private BufferedReader inSocket;
	private PrintWriter outSocket;
	private PrintWriter outVideo;
	
	
	public ClientConnection(){
		try{
		this.socket=new Socket(ADDRESSSERVER,PORT);
		this.inKey=new BufferedReader(new InputStreamReader(System.in));
		this.inSocket=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.outVideo=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)),true);
		this.outSocket=new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
		}
		catch(IOException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot communicate",e);
		}
	}
		
		public void sendSocketMessage(String message){
			 this.outSocket.println(message);
			 this.outSocket.flush();
		}
		
		public void sendVideoMessage(String message){
			this.outVideo.println(message);
			this.outVideo.flush();
		}
		
		public String receiveSocketMessage(){
			String tmp=new String();
			try{
				tmp=this.inSocket.readLine();
			}
			catch(IOException e){
				Logger.getAnonymousLogger().log(Level.SEVERE,"cannot receive messasge",e);
				closeSocket();
			}
			return tmp;
		}
		
		public String receiveKeyboardMessage(){
			String key=new String();
			try{
				key=this.inKey.readLine();				
			}
			catch(IOException f){
				Logger.getAnonymousLogger().log(Level.SEVERE,"cannot receive key message",f);
			}
			return key;
		}
		public void closeSocket(){
			try{
				this.socket.close();
			}
			catch(IOException f){
				Logger.getAnonymousLogger().log(Level.SEVERE,"cannot close the reader",f);
			}
		}
	}
