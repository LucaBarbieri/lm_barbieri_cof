package lm_57.server.controller.socket;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.controller.action.ControlAction;

public class ActionPerformed {
	
    private ControlAction control;
	
	public ActionPerformed(ControlAction control){
		this.control=control;
	}
	
	public boolean actionPerformed(){
		boolean controller=false;
		    if(control!=null){
		    	try{
		    	controller=control.isCompleted();
		    	}
		    	catch(RemoteException e){
		    		Logger.getAnonymousLogger().log(Level.SEVERE,"cannot complete main action",e);
		    	}
		    }
		    if(control!=null){
		    	try{
		    		controller=control.isCompleted();
		    	}
		    	catch(RemoteException f){
		    		Logger.getAnonymousLogger().log(Level.SEVERE,"cannot complete fast action",f);
		    	}
		    }
		    return controller;
	}
	

}
