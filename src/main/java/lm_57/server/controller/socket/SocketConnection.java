package lm_57.server.controller.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SocketConnection {
	
	private Socket socket;
	private BufferedReader inSocket;
	private PrintWriter outSocket;
	
	public SocketConnection(Socket socket){
		this.socket=socket;
		try{
		this.inSocket=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.outSocket=new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
		}
		catch(IOException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot communicate",e);
			closeSocket();
		}
	}
	
	public void closeSocket(){
		try{
			this.socket.close();
		}
		catch(IOException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot close socket",e);
		}
	}
	
	public void sendMessage(String message){
		this.outSocket.println(message);
		this.outSocket.flush();
	}
	
	public String receiveMessage(){
		String tmp=new String();
		try{
		tmp=this.inSocket.readLine();
		}
		catch(IOException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot read message",e);
			closeSocket();
		}
		return tmp;
	}

}
