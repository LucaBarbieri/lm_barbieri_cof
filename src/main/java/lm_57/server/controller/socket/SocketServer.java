package lm_57.server.controller.socket;
 
 
import java.io.IOException;
 
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
 
import lm_57.server.controller.User;
import lm_57.server.controller.connection.ServerStartGame;
import lm_57.server.model.Constants;
import lm_57.server.model.GameBoard;
 
 
public class SocketServer{
 
	private static final int PORT=63005;
	private ServerSocket serSocket;
	private Socket socket;
	private Set<Integer> positionBonus=new HashSet<>();
	private int numPlayer=0;
	private ClientHandler client;
	private ServerStartGame serController;
	private Timer time=new Timer();
	private boolean timer=false;
	private List<ClientHandler> clients=new ArrayList<>();
	private ControlCommandAction controlAction;
	private ControlCommandMarket controlMarket;
	private List<GameBoard> games=new ArrayList<>();
	private int numGame=0;
	private int numCurrentPlayer=0;
	private int numMap=0;
	private int numBonus=0;
	private int numPlayers=0;
	private boolean flagNumPlayer;
	private ThingsToSell productInSelling=new ThingsToSell();
 
	
    public SocketServer(ServerStartGame server){ 	
    	this.serController=server;
    	try{
    		serSocket=new ServerSocket(PORT);
    	 while(true){
    	    	socket=this.serSocket.accept();
    	    	flagNumPlayer=false;
    	    	numPlayer++;
    	    	client=new ClientHandler(new SocketConnection(socket),this);
    	    	clients.add(client);
    	    	client.start();
    	    	numCurrentPlayer=serController.getUsers().size();
    	    	controlGameConnection();
    	    	
    	}
    	}
    	catch(IOException e){
    		Logger.getAnonymousLogger().log(Level.SEVERE,"cannot communicate",e);
    		closeSocket();
    	}
    }
 
    private void getTheSettings(){
    	     this.numBonus=serController.getNumBonus();
    	     this.numMap=serController.getNumMap();
    	     this.numPlayers=serController.getNumPlayers();
    }
    
    private void setGameSettings() throws IOException{
    		 for(ClientHandler c:clients){
    			 if(c.getNumPlayer()!=0){
    				 try{
    		 setFirstPlayerInizialization();			 
    		 serController.startMatchGame(Constants.generateMatchName());
    		 this.games.add(serController.getCurrentGame());
    		 startUpControlCommand();
    		 }
    		 catch(RemoteException e){
    			 Logger.getAnonymousLogger().log(Level.SEVERE,"cannot start the game",e);
    			 closeSocket();
    		 }
    	 }
    		 }
    }
 
    private void setFirstPlayerInizialization(){
		 serController.setNumBonus(this.numBonus);
		 serController.setNumMap(this.numMap);
		 positionBonus.addAll(setPositionBonus(this.numBonus));
		 serController.setPositionBonus(positionBonus);
    }

	public void setNumMap(int numMap) {
		this.numMap = numMap;
	}


	public void setNumBonus(int numBonus) {
		this.numBonus = numBonus;
	}


	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public void addServerUser(User us){
      serController.addUser(us);
}
    
    
    public void timerManagment(){
    	   
     	this.time.schedule(new TimerTask(){
    		 @Override
    		 public void run(){
     			 System.out.println("Time is up");
    			 try {
    				 timer=true;
					setGameSettings();
				} catch (IOException e) {
					Logger.getGlobal().log(Level.SEVERE, "file not found!", e);
				}
    			 timer=true;
    		 }
    	 },20000);
    }
 
    public boolean getStateTimer(){
    	return timer;
    }
    
    public ServerStartGame getGeneralServer(){
    	return this.serController;
    }
    
    private void closeSocket(){
    	try{
    		serSocket.close();
    		}
    	catch(IOException e){
    		Logger.getAnonymousLogger().log(Level.SEVERE,"cannot close server socket",e);
    	} 	
   }
    
    private void controlGameConnection(){
    	if(numPlayer==1){
    	if(numCurrentPlayer==1){
    		getTheSettings();
    		numPlayer++;
    	}
    	else{
    		flagNumPlayer=true;
    	}
    	}
    	if(numPlayer==2){
    		timerManagment();
    	}
    	if(numCurrentPlayer==this.numPlayers){
    		//time.cancel();
    		numPlayer=0;
    		try{
    		setGameSettings();
    		}
    		catch(IOException e){
    			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot start game",e);
    		}
    		numCurrentPlayer=0;
    	}
    }
    
    private void startUpControlCommand(){
		 controlAction=new ControlCommandAction(serController.getCurrentGame().getMap(),this.games.get(0).getPlayers());
		 controlMarket=new ControlCommandMarket(serController.getCurrentGame().getPlayers());
    }
    
		
		private Set<Integer> setPositionBonus(int numberOfBonus){
			boolean eq;
			Set<Integer> pos=new HashSet<>();
			for(int i=0;i<numberOfBonus;i++){
				do{
				int k=(int) (20*Math.random());
				  eq=pos.add(k);							
				}while(!eq);
			} 
			return pos;
		}
		
		public ControlCommandAction getCommandActionController(){
			return this.controlAction;
		}
		
		public ControlCommandMarket getCommandMarketController(){
			return this.controlMarket;
		}
		
		public GameBoard getCurrentGameBoard(){
			return this.games.get(numGame);
		}
		
		public boolean getFlagNumPlayer(){
			return this.flagNumPlayer;
		}
		
		public ThingsToSell getThingsToSell(){
			return this.productInSelling;
		}
		
}