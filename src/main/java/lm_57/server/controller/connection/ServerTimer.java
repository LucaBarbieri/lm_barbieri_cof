package lm_57.server.controller.connection;

import java.rmi.RemoteException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.controller.RMI.ServerRMI;

import static lm_57.server.model.Constants.TIMEOUT_START_GAME;

public class ServerTimer {
	
	private Timer timer;
	private ServerRMI serverRMI;
	private boolean stateTimer;
	
	public ServerTimer(ServerRMI server) {
		this.serverRMI = server;
		stateTimer=false;
	}
	
	public void startTimer() {
		
		if (timer == null) {
			timer = new Timer();
			TimerTask start = new TimerTask(){
				@Override
				public void run() {
					if (serverRMI != null) {
						System.out.println("timer expired!");
						stateTimer=true;
						try {
							serverRMI.startGame();
						} catch (RemoteException e) {
							Logger.getGlobal().log(Level.SEVERE, "error starting the game", e);
						}
					}
				}
			};
		
			timer.schedule(start, TIMEOUT_START_GAME);
			
			
		}
	}

	/**
	 * stop and reset the timer
	 */
	public void stopTimer() {
		timer.cancel();
		timer.purge();
		timer = null;
	}
	
	public Timer getTimer(){
		return timer;
	}
	
	public boolean getState(){
		return stateTimer;
	}
}
