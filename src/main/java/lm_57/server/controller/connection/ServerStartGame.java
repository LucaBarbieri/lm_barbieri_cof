package lm_57.server.controller.connection;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lm_57.server.controller.User;
import lm_57.server.model.GameBoard;

public class ServerStartGame implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2897634898358521654L;
	private GameBoard game;
	private List<GameBoard> activeGame=new ArrayList<>();
	private List<User> usersLogin=new ArrayList<>();
	private int numBonus;
	private int numMap;
	private int numPlayers;
	private Set<Integer> bonusPosition=new HashSet<Integer>();
	
	
	public ServerStartGame() {
		
	}
	/**
	 * this method is used to start the game when the timer expires or the number of players is achieved
	 * 
	 * @param nameGame
	 * @throws RemoteException
	 */

	public void startMatchGame(String nameGame) throws RemoteException{
		if(game==null){
			game=new GameBoard(nameGame);
			game.setNumMap(numMap);
			game.setNumBonus(numBonus);
			game.setPositionBonus(bonusPosition);
			game.addPlayer(usersLogin.size(), usersLogin);
			activeGame.add(game);
			game.start();
						
		}
	}
	
	/**
	 * when the current game is finished the game is removed from the list
	 * @param game current GameBoard completed
	 */
	public void removeGame(GameBoard game){
		if(activeGame.contains(game)){
			activeGame.remove(game);
		}
	}
	
	public GameBoard getCurrentGame(){
		return game;
	}
	
	public List<GameBoard> getListActiveGame(){
		return this.activeGame;
	}
	
	public void addUser(User user){
		usersLogin.add(user);
	}
	
	public void addUserList(List<User> userList){
		usersLogin.addAll(userList);
	}
	
	public List<User> getUsers(){
		return this.usersLogin;
	}
	
	/**
	 * the following methods are used by the Socket or RMI server to initialize the game
	 * 
	 */
	
	public void setNumMap(int map){
		this.numMap=map;
	}
	public void setPositionBonus(Set<Integer> positions){
		this.bonusPosition.addAll(positions);
	}
		
	public void setNumBonus(int numBonus){
		this.numBonus=numBonus;
	}
	
	public int getNumMap(){
		return this.numMap;
	}
	
	public int getNumPlayers(){
		return this.numPlayers;
	}
	
	public int getNumBonus(){
		return this.numBonus;
	}
	/**
	 * reset the parameter 
	 */
	public void reset(){
		bonusPosition.clear();
		numMap=0;
		numBonus=0;
		usersLogin.clear();
		game=null;
	}
	
}
