package lm_57.server.controller.RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import lm_57.client.controller.RMI.IClientRMI;
import lm_57.server.controller.User;

public interface IServerRMI extends Remote{

	/**
	 * register the client on the server
	 * 
	 * @param client
	 * @throws RemoteException
	 */
	
	void register(IClientRMI client) throws RemoteException;
	
	/**
	 * called by the client to check the list of user for choosing the player color
	 * 
	 * @return List<User> in the current match
	 * @throws RemoteException
	 */
	public List<User> getRMIUser() throws RemoteException;

	
	
}
