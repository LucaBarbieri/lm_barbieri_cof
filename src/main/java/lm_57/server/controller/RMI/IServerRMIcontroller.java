package lm_57.server.controller.RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

import lm_57.client.controller.RMI.IClientRMI;
import lm_57.server.controller.User;
import lm_57.server.model.GameBoard;

public interface IServerRMIcontroller extends Remote{
	
	/**
	 * notify to all clients the start state of the game: match name,
	 * client color and number of players
	 * 
	 * @throws RemoteException
	 */
	public boolean notifyGameStart() throws RemoteException;
	
	/**
	 * notify to all clients the turn name, color
	 * 
	 * @param playerColor
	 * @throws RemoteException
	 */
	public void notifyTurn() throws RemoteException;
	
	/**
	 * notify the changes that takes place during the turn
	 * and notify it to the clients, after that change the game turn
	 * 
	 * @throws RemoteException
	 */
	public void passTurn() throws RemoteException;
	
	/**
	 * notify to all clients a server shutdown
	 */
	public void disconnection() throws RemoteException;
	
	/**
	 * used only by the server to add clients and users
	 */
	
	public void addPlayerClient(Map<IClientRMI,User> clients) throws RemoteException;
	
	/**
	 * used by the server to pass the game created
	 */
	
	public void startGame(GameBoard game)throws RemoteException;
	
	/**
	 * this method is called by the client to choose the  main action 
	 * and he pass the action, the server invoke the client for set the parameter for the action
	 * and then called the controller to update the model 
	 * at the end of the action notify to the other client what the player do
	 * @param mainAction 
	 */
	public void runMainAction(String mainAction)throws RemoteException;
	
	
	/**
	 * this method is called by the client to choose the fast action 
	 * and he pass the action, the server invoke the client for set the parameter for the action
	 * and then called the controller to update the model.
	 * at the end of the action notify to the other client what the player do
	 * @param fastAction
	 */
	public void runFastAction(String fastAction)throws RemoteException;
	
	/**
	 * this method allow to show the client the visible card
	 * @param client is who is run the action
	 * @throws RemoteException
	 */
	public void showVisiblePermitCardRegion(IClientRMI client)throws RemoteException;
	
	/**
	 * show the client the situation of all the council balcon in the game
	 * @param client is who is run the action
	 * @throws RemoteException
	 */
	public void showCouncilBalcon(IClientRMI client)throws RemoteException;
	
	/**
	 * update the turn removing the fast action to the current player
	 * @param clientRMI
	 * @throws RemoteException
	 */
	public void NoFastAction()throws RemoteException;
	
	/**
	 * display to the client his situation
	 */
	public void displayPlayerSituation(IClientRMI client)throws RemoteException;
	
	/**
	 * display the map to the client
	 * @param client
	 * @throws RemoteException
	 */
	public void displayMap(IClientRMI client)throws RemoteException;
	
	/**
	 * display all the connection of cities
	 * @param client
	 * @throws RemoteException
	 */
	public void displayConnectionCity(IClientRMI client)throws RemoteException;
	
	/**
	 * display the politic card  colors to client
	 * @param client
	 * @throws RemoteException
	 */
	void displayPlayerPoliticCard(IClientRMI client)throws RemoteException;
	
	/**
	 * run the turn of sell an object in the MARKET
	 * @param object
	 * @throws RemoteException
	 */
	void runSell(String object) throws RemoteException;
	
	/**
	 * check the turn of the client
	 * @param clientRMI
	 * @return true or false
	 * @throws RemoteException
	 */
	public boolean isMyTurn(IClientRMI clientRMI)throws RemoteException;
	
	/**
	 * called by the client to pass the MARKET TURN if he doesn't want to sell or buy anything
	 * @param clientRMI
	 * @throws RemoteException
	 */
	public void passTurnMarket(IClientRMI clientRMI)throws RemoteException;
	
	/**
	 * display the current city of the King
	 * @param clientRMI
	 */
	public void displayKingCity(IClientRMI clientRMI)throws RemoteException;
	
	/**
	 * run the turn of buy an object
	 * @param index of position of the object in the game list
	 * @throws RemoteException
	 */
	public void runBuy(int index) throws RemoteException;
	
	/**
	 * called by the client to see all the product on sale during market
	 * @param clientRMI
	 * @throws RemoteException
	 */
	void displayProductOnSale(IClientRMI clientRMI) throws RemoteException;

	public void passTurnMarketBuy(IClientRMI clientRMI)throws RemoteException;

	public int getDimensionSellList(IClientRMI clientRMI)throws RemoteException;
	
	/**
	 * begin when a client choose the action
	 */

	void startTimer(IClientRMI client)throws RemoteException;
	
	
}
