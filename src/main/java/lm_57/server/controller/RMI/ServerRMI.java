package lm_57.server.controller.RMI;


import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static lm_57.server.model.Constants.MIN_PLAYERS;
import static lm_57.server.model.Constants.NAME_MANAGER;
import static lm_57.server.model.Constants.SERVER_RMI_PORT;
import static lm_57.server.model.Constants.NAME_SERVER;

import lm_57.client.controller.RMI.IClientRMI;
import lm_57.client.view.CLI.Display;
import lm_57.server.controller.User;
import lm_57.server.controller.connection.ServerStartGame;
import lm_57.server.controller.connection.ServerTimer;
import lm_57.server.model.Constants;
import lm_57.server.model.GameBoard;

/**
 * this class is used to set multiple connection to the server RMI 
 * and start a new match in the game, the class which manage the game is the serverRMIcontroller
 *
 */
public class ServerRMI implements IServerRMI{

	private ServerTimer timer;
	private IServerRMIcontroller manager;
	private Map<IClientRMI, User> clientMap;
	ServerStartGame startServer;
	private int numPlayer;
	
	public ServerRMI (ServerStartGame commonServer) {
		startServer=commonServer;
		timer = new ServerTimer(this);
		clientMap = new ConcurrentHashMap<>();
	}
	
	public void resetServerRMI(){
		clientMap.clear();
		timer=new ServerTimer(this);
		manager=null;
		numPlayer=-1;
		Constants.resetConnections();
	}
	
	
	@Override
	public void register(IClientRMI client) throws RemoteException {
		User user=new User(client.getName(),client.getColor());
		clientMap.put(client, user);
		startServer.addUser(user);
		client.notifyMessage("You are connected to the server..\n");
		client.notifyMessage("Wait for other players\n");
		client.notifyMessage("You're color is: ["+ client.getColor()+"]");
		client.notifyMessage("You're Player name is: ["+ client.getName()+"]");
		Constants.addConnection();
		if(	Constants.getConnectionNumber()==1){
			client.notifyMessage("You are the first player please initialize the game!\n");	
			
			try {
				client.initializeGame();
				numPlayer= client.getNumPlayer();
				startServer.setNumBonus(client.getNumBonus());
				startServer.setNumMap(client.getNumMap());
				startServer.setPositionBonus(client.getPositionBonus());
				
			} catch (IOException e) {
				Logger.getGlobal().log(Level.SEVERE, "error in the input", e);
			}
		}
		
		if(Constants.getConnectionNumber()==numPlayer && !timer.getState()){
			startGame();
		}
		
		if(Constants.getConnectionNumber()>=MIN_PLAYERS){
			timer.startTimer();			
		}
		
	}
	
	public List<User> getRMIUser(){
		List<User> users=new ArrayList<>();
		users.addAll(clientMap.values());
		return users;
	}

	/**
	 * stop the timer and create the manager of the game
	 * also reset the condition of the server for a new connection 
	 * @throws RemoteException 
	 * 
	 */
	public void startGame() throws RemoteException {
		Map<IClientRMI, User> clientRMIOfTheMatch = new HashMap<>();
		GameBoard game=null;
		
		clientRMIOfTheMatch.putAll(clientMap);
		manager.addPlayerClient(clientRMIOfTheMatch);
		
		Display.printMessage("RMI Manager  of current game ready, stub server is registred on port: "
				+ SERVER_RMI_PORT); 
			
		startServer.startMatchGame(Constants.generateMatchName());
		for(int i=0; i<startServer.getListActiveGame().size();i++){
			if(startServer.getListActiveGame().get(i).equals(startServer.getCurrentGame())){
				game=startServer.getListActiveGame().get(i);
			}
		}
		if(game != null){
			manager.startGame(game);
			resetServerRMI();
		}
	}


	public void startRMI() {
		try {
			
			Registry registry = LocateRegistry.createRegistry(SERVER_RMI_PORT);
			manager=new ServerRMIcontroller(startServer);
			IServerRMI stub = (IServerRMI) UnicastRemoteObject.exportObject(this, 0);
			
			registry.rebind(NAME_MANAGER, manager);
			registry.rebind(NAME_SERVER, stub);
			
			Display.printMessage("RMI Server ready, stub server is registred on port: "
					+ SERVER_RMI_PORT); 
			
		} catch (Exception e) {
			
			Logger.getGlobal().log(Level.SEVERE, "error registry", e);
		}
	}
	

}
