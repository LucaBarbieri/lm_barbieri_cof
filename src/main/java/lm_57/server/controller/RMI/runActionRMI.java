package lm_57.server.controller.RMI;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import lm_57.server.controller.action.ControlBuildEmporiumByPermitCard;
import lm_57.server.controller.action.ControlBuildOnKingCity;
import lm_57.server.controller.action.ControlBuyAssistant;
import lm_57.server.controller.action.ControlBuyPermitCard;
import lm_57.server.controller.action.ControlElectedCouncilor;
import lm_57.server.controller.action.ControlPayAssistantForChangePermitCard;
import lm_57.server.controller.action.ControlSetNewCouncillorOnBalcon;
import lm_57.server.model.Constants;
import lm_57.server.model.GameBoard;
import lm_57.server.model.Player;
import lm_57.server.model.action.PayAssistantForMainAction;
import lm_57.server.model.map.City;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.map.Region;
import lm_57.server.model.market.AssistantSeller;
import lm_57.server.model.market.PermitSeller;
import lm_57.server.model.market.PoliticSeller;
import lm_57.server.model.market.ProductToSell;
import lm_57.server.model.utility.Councillor;
import lm_57.server.model.utility.PermitCard;
import lm_57.server.model.utility.PoliticCard;

public final class runActionRMI {
	
	
	/**
	 * fast action
	 * 
	 */
	public static boolean runFastActionElection(GameBoard game,String councilBalcon,String color) throws RemoteException{
		CouncilBalcon balcon=getStringToCouncil(game,councilBalcon);
		Councillor councillor=getStringToCouncillor(color);
		ControlElectedCouncilor controller=new ControlElectedCouncilor
				(balcon,councillor,game.getCurrentPlayerTurn());
		
		return controller.isCompleted();
	}
	
	public static boolean runFastActionChangeVisibleCard(GameBoard game,String region) throws RemoteException{
		Region regionChoose=null;
		for(Region regionList: game.getMap().getRegionList()){
			if(regionList.getName().equals(region)){
				regionChoose=regionList;
			}
		}
		ControlPayAssistantForChangePermitCard control=
				new ControlPayAssistantForChangePermitCard(regionChoose,game.getCurrentPlayerTurn());
		
		return control.isCompleted();
	}
	
	public static boolean runPayForMainAction(Player player) throws RemoteException{
		PayAssistantForMainAction control=new PayAssistantForMainAction(player);
		return control.isCompleted();
	}
	
	public static boolean runFastActionBuyAssistant(Player player) throws RemoteException{
		ControlBuyAssistant control= new ControlBuyAssistant(player);
		return control.isCompleted();
	}
	
	/**
	 * main action
	 * 
	 * 
	 */
	public static boolean runMainActionBuildEmporiumPermitCard(int indexCard, String cityCard, GameBoard game) throws RemoteException{
		PermitCard permit=getCardFromIndex(indexCard, game);
		City city=game.getMap().getCity(cityCard);
		ControlBuildEmporiumByPermitCard control=new ControlBuildEmporiumByPermitCard
				(game.getCurrentPlayerTurn(),game.getMap(),permit,city);
		return control.isCompleted();
	}
	
	public static boolean runMainActionBuildOnKing(GameBoard game, List<String> path,String nameCity) throws RemoteException{
		City destination= game.getMap().getCity(nameCity);
		ControlBuildOnKingCity control=new ControlBuildOnKingCity
				(game.getCurrentPlayerTurn(),destination,game.getMap(),path);
		return control.isCompleted();
	}
	
	public static boolean runMainActionElectedCouncillor(GameBoard game, String balcon, String color) throws RemoteException{
		CouncilBalcon councilBalcon=getStringToCouncil(game,balcon);
		Councillor councillor=getStringToCouncillor(color);
		ControlSetNewCouncillorOnBalcon control=new ControlSetNewCouncillorOnBalcon
				(game.getCurrentPlayerTurn(),councilBalcon,councillor);
		return control.isCompleted();
	}
	
	public static boolean runMainActionBuyPermitCard(GameBoard game, String regionName, int indexcard) throws RemoteException{
		Region region=game.getMap().getRegionFromName(regionName);
		ControlBuyPermitCard control=new ControlBuyPermitCard(game.getCurrentPlayerTurn(),region,indexcard);
		return control.isCompleted();
	}
	/**
	 * action market: SELL
	 * @throws RemoteException 
	 */
	
	public static boolean runSellAssistant(GameBoard game,int value, int quantity) throws RemoteException{
		ProductToSell product= new AssistantSeller(game.getCurrentPlayerTurn(),quantity,value);
		game.addProductToSell(product);
		return true;
	}
	
	public static boolean runSellPermit(GameBoard game,int value, List<Integer> index) throws RemoteException{
		ArrayList<PermitCard> permit=new ArrayList<>();
		for(int i=0;i<index.size();i++){
			permit.add(getCardFromIndex(index.get(i),game));
		}
		ProductToSell product= new PermitSeller(game.getCurrentPlayerTurn(),permit,value);
		game.addProductToSell(product);
		return true;
	}
	
	public static boolean runSellPolitic(GameBoard game,int value, List<Integer> index) throws RemoteException{
		ArrayList<PoliticCard> politic=new ArrayList<>();
		for(int i=0;i<index.size();i++){
			politic.add(getPoliticFromIndex(index.get(i),game));
		}
		ProductToSell product= new PoliticSeller(game.getCurrentPlayerTurn(),politic,value);
		game.addProductToSell(product);
		return true;
		
	}
	
	/**
	 * BUY MARKET
	 */
		
	public static boolean runBuyMarket(int index, GameBoard game){
		ProductToSell product=game.getProductToSell().get(index);
		if(game.getCurrentPlayerTurn().getPlayerCoin()>=product.getCoin()){
			product.sellingProduct(game.getCurrentPlayerTurn());
			game.removeProduct(index);
			return true;
		}
		return false;
	}
	/**
	 * convert the string to the object 
	 * @param game
	 * @param balcon
	 * @return the object council balcon
	 */
	private static CouncilBalcon getStringToCouncil(GameBoard game, String balcon){
		CouncilBalcon councilBalcon;
		
		if(Constants.isInRegion(balcon)){
			councilBalcon=game.getMap().getRegionFromName(balcon).getCouncilBalcon();
		}
		else{
			councilBalcon=game.getMap().getKing().getCouncilBalcon();
		}
		
		return councilBalcon;
	}
	
	/**
	 * convert the color to a councillor object
	 * @param color
	 * @return the councillor
	 */
	private static Councillor getStringToCouncillor(String color){
		Councillor councillor=new Councillor();
		councillor.setCouncillorColor(Constants.convertStringToColor(color));
		return councillor;
	}
	/**
	 * return a permit card from the index
	 */
	private static PermitCard getCardFromIndex(int index, GameBoard game){
		return game.getCurrentPlayerTurn().getPlayerPermitCard().get(index);
	}
	
	/**
	 * return a politic card from the index
	 */
	private static PoliticCard getPoliticFromIndex(int index, GameBoard game){
		return game.getCurrentPlayerTurn().getPoliticCard().get(index);
	}
	
}
