package lm_57.server.controller.RMI;

import static lm_57.server.model.Constants.NAME_MANAGER;
import static lm_57.server.model.Constants.SERVER_RMI_PORT;
import static lm_57.server.model.Constants.TIMEOUT_START_GAME;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.client.controller.RMI.IClientRMI;
import lm_57.client.view.CLI.Display;
import lm_57.server.controller.User;
import lm_57.server.controller.connection.ServerStartGame;
import lm_57.server.model.Constants;
import lm_57.server.model.GameBoard;
import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Region;
/**
 * creates the game adding the clients of the current match and manage 
 * all the game running
 *
 */

public class ServerRMIcontroller extends UnicastRemoteObject implements Runnable,IServerRMIcontroller, Serializable{
	
	private static final long serialVersionUID = -8528534902555378904L;
	
	private final String MESSAGE="YOUR COIN ARE: ";
	private ServerStartGame serverStart;
	private Map<IClientRMI, User> clientMap= new HashMap<>();
	private Set<IClientRMI> clientSet= new HashSet<>();
	private List<User> userRMI=new ArrayList<>();
	private Timer timer;
	private GameBoard game;
	private IClientRMI currentClient;
	
	
	public ServerRMIcontroller(ServerStartGame server) throws RemoteException {
		serverStart=server;
	}
	@Override
	public void startTimer(IClientRMI client)throws RemoteException{
		if (timer == null) {
			timer = new Timer();
			Player player=this.getPlayerFromClient(client);
			TimerTask start = new TimerTask(){
					@Override
					public void run(){
						try {
							client.notifyDisconnection();
							game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
							game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
							game.getStateGame().removePlayer(player);
							game.removePlayer(player);
							notifyChange("the player: ["+player.getName()+"]"
							+"["+Constants.convertColorToString(player.getPlayerColor())+"]", "IS NOW DISCONNECTED!!!!!!!");
							clientSet.remove(client);
							clientMap.remove(client);
							if(clientSet.size()<2){
								notifyChange("THERE ARE NO OTHER PLAYER", "GAME SHUT DOWN!!!!!!!");
								disconnection();
							}
							else
								passTurn();
						}catch(RemoteException e){
							Logger.getGlobal().log(Level.SEVERE, "ERROR", e);
						}
					}
			};
		
			timer.schedule(start, TIMEOUT_START_GAME);
		}
	}
	@Override
	public void addPlayerClient(Map<IClientRMI,User> clients){
		this.clientMap.putAll(clients);
		this.clientSet.addAll(clients.keySet());
		this.userRMI.addAll(clientMap.values());
	}

	
	@Override
	public void startGame(GameBoard game) throws RemoteException{
		
		this.game=game;
		if(notifyGameStart()){
			this.currentTurnRound();
		}
	}
	private String displayPlayers(){
		String string=".";
		for(IClientRMI client: clientMap.keySet()){
			string += ".. "+clientMap.get(client).getColor()+" ..";
		}
		return string;
	}
	@Override
	public boolean notifyGameStart() throws RemoteException {
		
		Iterator<IClientRMI> iter = clientSet.iterator();
		
		while (iter.hasNext()) {
			
			IClientRMI client = iter.next();
			String color;
			color= clientMap.get(client).getColor();			
			client.notifyGameStart(game.getGameName(),color,game.getPlayers().size(),this.displayPlayers() );
			client.notifyTurn(game.getCurrentPlayerToString(), game.getTurnColor());
		}
		return true;
	}

	@Override
	public void notifyTurn() throws RemoteException {
		Iterator<IClientRMI> iter = clientSet.iterator();
		
		while (iter.hasNext()) {
			IClientRMI client = iter.next();
			client.notifyMessage(game.displayCurrentTurn()); 

		}
	}

	@Override
	public void passTurn() throws RemoteException {
		game.setNextTurn();
		nextTurn();
	}

	private void nextTurn() throws RemoteException {
		Display.printMessage(game.getGameName()+" changing turn");
		
		if(game.getState().gameIsFinished()){
			this.notifyWinner();
		}
		if(game.getStateGame().getCurrentState()==game.getStateGame().getCurrentRound()){
			currentClient= takeCurrentClient();
			notifyTurn();
			currentTurnRound();
		}
		if(game.getStateGame().getCurrentMarket()==game.getStateGame().getCurrentState()){
			if(game.getStateGame().getCurrentMarket().getMarketState()==game.getStateGame().getCurrentMarket().getSell()){
				currentClient= takeCurrentClient();
				notifyTurn();
				currentTurnSellMarket();
			}
			if(game.getStateGame().getCurrentMarket().getMarketState()==null){
				this.notifyChange("MARKET FINISHED!!!", "NEW ROUND");
				game.clearProduct();
				passTurn();
			}
			else{
				currentClient= takeCurrentClient();
				notifyTurn();
				currentTurnBuyMarket();
			}
		}
		
	}
	/**
	 * make the current player play the turn of Buy SELL
	 * @throws RemoteException
	 */
	private void currentTurnSellMarket() throws RemoteException{
		IClientRMI client=takeCurrentClient();
		if(client!=null){
			client.notifyMessage("IT'S YOUR TURN : ");
			client.playTurnSell();
		}
	}
	/**
	 * make the current player play the turn of Buy MARKET
	 * @throws RemoteException
	 */
	private void currentTurnBuyMarket() throws RemoteException{
		IClientRMI client=takeCurrentClient();
		if(client!=null){
			client.notifyMessage("IT'S YOUR TURN : ");
			client.playTurnBuy();
		}
	}
	/**
	 * this method return the current client turn
	 * @return
	 * @throws RemoteException
	 */
	private IClientRMI takeCurrentClient()throws RemoteException{
		
		for(Entry<IClientRMI, User> entry :clientMap.entrySet()){
			IClientRMI client=entry.getKey();
			if(clientMap.get(client).getColor().equals(game.getTurnColor())){
				currentClient=client;
			}
		}
		return currentClient;
	}
	/**
	 * make the client playing the turn
	 * @throws RemoteException
	 */
	private void currentTurnRound() throws RemoteException{
		IClientRMI currentClient=takeCurrentClient();
		
		currentClient.notifyMessage("IT'S YOUR TURN : ");
		currentClient.playTurnRound();
	}
	@Override
	public void runBuy(int indexObject) throws RemoteException{
		IClientRMI client= this.takeCurrentClient();
		runActionBuy(indexObject, client);
	}
	
	private void runActionBuy(int indexObject, IClientRMI client) throws RemoteException {
		if(!runActionRMI.runBuyMarket(indexObject, game)){
			client.notifyMessage("YOU DON'T HAVE ENOUGH MONEY! CHOOSE ANOTHER ACTION");
			client.playTurnBuy();
		}
		client.notifyMessage("SUCCESS! ");
		this.displayPlayerSituation(client);
		this.displayPoliticCard(client);
		this.displayPermitCard(client);
		client.notifyMessage("END BUY");
		game.getStateGame().getCurrentMarket().getBuy().getCurrentTurn().removeAction();
		passTurn();
	}

	@Override
	public void runSell(String object) throws RemoteException{
		IClientRMI client= this.takeCurrentClient();
		switch(object){
		case "Assistant":
			 runSellAssistant(client);
			break;
		case "PermitCard":
			runSellPermitCard(client);
			break;
		case "PoliticCard":
			runSellPolitic(client);
			break;
		}
	}
	private void runSellPolitic(IClientRMI client) throws RemoteException {
		if(game.getCurrentPlayerTurn().getPoliticCard().isEmpty()){
			client.notifyMessage("YOU DON'T HAVE PERMIT CARD! CHOOSE ANOTHER ACTION");
			client.playTurnSell();
			return;
		}
		List<Integer> listIndex=new ArrayList<>();
		listIndex=client.chooseIndex(game.getCurrentPlayerTurn().getPoliticCard().size());
		int coin=client.chooseCoin();
		
		runActionRMI.runSellPolitic(game,coin, listIndex);
		game.getStateGame().getCurrentMarket().getSell().getCurrentTurn().removeAction();
		client.notifyMessage("POLITIC CARD ON SALE!");
		passTurn();
		
		
	}

	private void runSellPermitCard(IClientRMI client) throws RemoteException{
		if(game.getCurrentPlayerTurn().getPlayerPermitCard().isEmpty()){
			client.notifyMessage("YOU DON'T HAVE PERMIT CARD! CHOOSE ANOTHER ACTION");
			client.playTurnSell();
			return;
		}
		List<Integer> listIndex=new ArrayList<>();
		listIndex=client.chooseIndex(game.getCurrentPlayerTurn().getPlayerPermitCard().size());
		int coin=client.chooseCoin();
		runActionRMI.runSellPermit(game,coin, listIndex);
		game.getStateGame().getCurrentMarket().getSell().getCurrentTurn().removeAction();
		client.notifyMessage("PERMIT CARD ON SALE!");
		passTurn();
	}
	/**
	 * allow the player to add to the game some assistant to sell
	 * @param client
	 * @throws RemoteException
	 */
	private void runSellAssistant(IClientRMI client) throws RemoteException{
		if(this.getPlayerFromClient(client).getPlayerAssistant()==0){
			client.notifyMessage("YOU DON'T HAVE ASSISTANT! CHOOSE ANOTHER ACTION");
			client.playTurnSell();
			return;
		}
		int coin=client.chooseCoin();
		int quantity=client.chooseQuantity(this.getPlayerFromClient(client).getPlayerAssistant());
		runActionRMI.runSellAssistant(game,coin,quantity);
		game.getStateGame().getCurrentMarket().getSell().getCurrentTurn().removeAction();
		client.notifyMessage("OBJECT ON SALE!");
		passTurn();
	}
	private void stopTimer(){
		timer.cancel();
		timer.purge();
		timer=null;
	}
	
	@Override
	public void runFastAction(String fastAction) throws RemoteException{
		this.stopTimer();
		IClientRMI client=takeCurrentClient();
		
		switch(fastAction){
		case "BuyAssistant":
			runBuyAssistant(client);
			break;
		case "PayAssistantForChangePermitCard":
			runChangePermitCard(client);
			break;
		case "ElectedCouncilor":
			runFastActionElectedCouncilor(client);
			break;
		case "PayAssistantForMainAction":
			runPayForMainAction(client);
			break;
		}
	}
	/**
	 * run the fast action, if the client don't have the coins the make him choose another action
	 * @param client that play the turn
	 * @throws RemoteException
	 */
	private void runBuyAssistant(IClientRMI client) throws RemoteException {
		if(!runActionRMI.runFastActionBuyAssistant(game.getCurrentPlayerTurn())){
			client.notifyMessage("You don't have enough coin, choose another fast action!!");
			client.chooseFastAction();
			return;
		}
		else{
			client.notifyMessage("SUCCESS!!");
			notifyUpdate(client, MESSAGE,game.getCurrentPlayerTurn().getPlayerCoin());
			notifyUpdate(client, "YOUR CURRENT ASSISTANT ARE",game.getCurrentPlayerTurn().getPlayerAssistant());
			game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
		}
	}
	
	/**
	 * notify to the client that something is changed(coin,assistant,nobility or victory)
	 * @param client
	 * @throws RemoteException
	 */
	private void notifyUpdate(IClientRMI client, String message, int number) throws RemoteException {
		client.notifyUpdate(message,number);		
	}
	
	/**
	 * run the fast action, if the client don't have the assistant the make him choose another action
	 * @param client that play the turn
	 * @throws RemoteException
	 */
	private void runPayForMainAction(IClientRMI client) throws RemoteException {
		if(!runActionRMI.runPayForMainAction(game.getCurrentPlayerTurn())){
			client.notifyMessage("You don't have enough assistant, choose another fast action!!");
			client.chooseFastAction();
			return;
		}
		else{
			client.notifyMessage("SUCCESS!!!!!");
			notifyUpdate(client,"YOUR CURRENT ASSISTANT ARE: ", game.getCurrentPlayerTurn().getPlayerAssistant());
			game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
			game.getStateGame().getCurrentRound().getCurrentTurn().addMainAction();
			client.chooseMainAction();
		}
	}

	/**
	 * called the controller for electing a councillor after creating the right object 
	 * 
	 * @param agentClient
	 * @throws RemoteException
	 */
	private void runFastActionElectedCouncilor(IClientRMI agentClient) throws RemoteException{
		
		String color= agentClient.chooseColorCouncillor();
		String council=agentClient.chooseCouncilBalcon();
		
		if(!runActionRMI.runFastActionElection(game,council, color)){
			agentClient.notifyMessage("You don't have enough assistant!! choose another fast action");
			agentClient.chooseFastAction();
			return;
		}
		else{
			agentClient.notifyMessage("SUCCESS!!!");
			agentClient.notifyUpdate("YOUR CURRENT ASSISTANT ARE: ", game.getCurrentPlayerTurn().getPlayerAssistant());
			game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
		}
}
	
	/**
	 * made the client choose the paramater to run the fast action that he chose
	 * @param client the current client of the turn
	 * @throws RemoteException
	 */
	private void runChangePermitCard(IClientRMI client) throws RemoteException{
		String region=client.setRegion(game.getMap().regionListToString());
		
		if(!runActionRMI.runFastActionChangeVisibleCard(game,region)){
				client.notifyMessage("You don't have enough assistant, choose another action");
				client.chooseFastAction();
				return;
			}
			notifyNewVisibleCard();
			client.notifyMessage("Your fast action is completed ");
			game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();		
		
	}
	/**
	 * notify all the clients that permitCard changed
	 * @throws RemoteException
	 */
	private void notifyNewVisibleCard() throws RemoteException{
		Iterator<IClientRMI> iter = clientSet.iterator();
		
		while (iter.hasNext()) {
			IClientRMI client = iter.next();
			client.notifyMessage("end fast action changed visible card: "); 
			for(Region region: game.getMap().getRegionList()){
				client.notifyMessage("Region: "+ region.getName()+ "VisibleCard: "+region.visibleCardToString());
			}
		}
	}
	@Override
	public void runMainAction(String mainAction) throws RemoteException{
		this.stopTimer();
		IClientRMI client=takeCurrentClient();
		
		switch(mainAction){
		case "BuyPermitCard":
			runMainBuyPermitCard(client);
			break;
		case "BuildEmporiumByPermitCard":
			runBuildByPermitCard(client);
			break;
		case "BuildOnKingCity":
			runBuildEmporiumKing(client);
			break;
		case "SetNewCouncillorOnBalconRegion":
			runSetNewCouncillor(client);
			break;
		}
	}
	
	private void runSetNewCouncillor(IClientRMI client) throws RemoteException{
		String color= client.chooseColorCouncillor();
		String council=client.chooseCouncilBalcon();
		if(!runActionRMI.runMainActionElectedCouncillor(game, council, color));
		client.notifyUpdate(MESSAGE, game.getCurrentPlayerTurn().getPlayerCoin());
		client.notifyMessage("the new council balcon are:");
		showCouncilBalcon(client);
		game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
	}
	
	private void runBuildByPermitCard(IClientRMI client) throws RemoteException{
		if(game.getCurrentPlayerTurn().getPlayerPermitCard().isEmpty()){
			client.notifyMessage("YOU DON'T PERMIT CARD, CHOOSE ANOTHER ACTION");
			client.chooseMainAction();
			return;
		}
		
		Set<String> permitCard=new HashSet<>();
		permitCard.addAll(game.getCurrentPlayerTurn().getStringPermitCard());
		int index= client.chooseIndexPermitCard
				(permitCard,permitCard.size());
		String city= client.chooseCity(game.getCurrentPlayerTurn().getPlayerPermitCard().get(index).getCityAllowed());
		if(!runActionRMI.runMainActionBuildEmporiumPermitCard(index, city, game)){
			client.notifyMessage("YOU DON'T HAVE ENOUGH COIN, CHOOSE ANOTHER ACTION");
			client.chooseMainAction();
			return;
		}
		client.notifyMessage("SUCCESS!!!");
		client.notifyUpdate(MESSAGE, game.getCurrentPlayerTurn().getPlayerCoin());
		displayPoliticCard(client);
		displayPermitCard(client);
		this.notifyChange("The player "+ client.getColor()+"have build an emporium", "In the city"+city);
		game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
	}
	
	private void runBuildEmporiumKing(IClientRMI client) throws RemoteException{
		List<String> path=new ArrayList<>();
		this.displayKingBalcon(client);
		int numCity=client.chooseNumOfCities();
		String initialCity=game.getMap().getKing().getCurrentCity().getName();
		for(int i=0;i<numCity;i++){
			client.notifyMessage("choose city "+i+" that you want to pass starting to king city!");
			String string=client.chooseKingPath(game.getMap().getCity(initialCity).getFriends());
			initialCity=string;
			path.add(initialCity);
		}
		String destination=path.get(path.size()-1);
		if(!runActionRMI.runMainActionBuildOnKing(game,path, destination)){
			client.notifyMessage("YOU DON'T HAVE ENOUGH COIN, CHOOSE ANOTHER ACTION");
			client.chooseMainAction();
			return;
		}
		client.notifyMessage("SUCCESS!!!");
		client.notifyUpdate(MESSAGE, game.getCurrentPlayerTurn().getPlayerCoin());
		client.notifyMessage("KING: " + game.getMap().getKing().getCurrentCity().getName());
		this.displayPoliticCard(client);
		this.notifyChange("The player "+ client.getColor()+"have build an emporium with king"
				," THE KING CITY IS NOW: "+ game.getMap().getKing().getCurrentCity().getName());
		game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
		
	}
	/**
	 * run the control action BuyPermitCard
	 * @param client
	 * @throws RemoteException
	 */
	private void runMainBuyPermitCard(IClientRMI client) throws RemoteException{
		Set<String> visibleCard=new HashSet<>();
		String region=client.setRegion(game.getMap().regionListToString());
		visibleCard.addAll(game.getMap().getRegionFromName(region).visibleCardToString());
		int index=client.chooseIndexPermitCard(visibleCard, 2);
		if(!runActionRMI.runMainActionBuyPermitCard(game, region, index)){
			client.notifyMessage("YOU DON'T HAVE ENOUGH COIN, CHOOSE ANOTHER ACTION!");
			client.chooseMainAction();
			return;
		}
		client.notifyMessage("SUCCESS!!!");
		client.notifyUpdate(MESSAGE, game.getCurrentPlayerTurn().getPlayerCoin());
		this.displayPermitCard(client);
		
		game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
	}
	/**
	 * return the player associated to the client
	 * @param client
	 * @return
	 */
	private Player getPlayerFromClient(IClientRMI client){
		User user=clientMap.get(client);
		Player player=null;
		for (Player players: game.getPlayers()){
			if(user.getColor().equals(Constants.convertColorToString(players.getPlayerColor()))){
				player=players;
			}
		}
		return player;
	}
	
	public void displayPermitCard(IClientRMI client) throws RemoteException{
		client.notifyMessage("YOUR PERMIT CARD ARE :"+ this.getPlayerFromClient(client).displayPermitCard());
	}
	
	public void displayPoliticCard(IClientRMI client) throws RemoteException{
		client.notifyMessage("YOUR POLITIC CARD ARE: "+this.getPlayerFromClient(client).displayPoliticCard());
	}
	
	@Override
	public void disconnection() throws RemoteException {
		Iterator<IClientRMI> iter = clientSet.iterator();
		while (iter.hasNext()) {
			IClientRMI client = iter.next();
			try {
			 client.notifyDisconnection();
			} catch (RemoteException e) {
				Logger.getGlobal().log(Level.SEVERE, "error in disconnection", e);
			}
		}
	}
	
	/**
	 * notify the winner of the game to all clients	
	 * @throws RemoteException
	 */
	private void notifyWinner() throws RemoteException{
		Player winner=game.getStateGame().getGameCompleted().getWinner();
		serverStart.removeGame(game);
		Iterator<IClientRMI> iter = clientSet.iterator();
		while (iter.hasNext()) {
			IClientRMI client = iter.next();
			Display.printMessage(game.getGameName()+ " notify that game is finished");
			client.notifyGameEnd(winner.getName()+Constants.convertColorToString(winner.getPlayerColor()));
		}
		disconnection();
	}
	
	
	@Override
	public void showVisiblePermitCardRegion(IClientRMI client) throws RemoteException {
		for(Region region: game.getMap().getRegionList()){
			client.notifyMessage("Region: ["+ region.getName()+"]"+"Visible card: "+ region.visibleCardToString());
		}
	}

	@Override
	public void showCouncilBalcon(IClientRMI client) throws RemoteException {
		for(Region region: game.getMap().getRegionList()){
			client.notifyMessage("Region: ["+ region.getName()+"]"+ region.getCouncilBalcon().toString());
		}
		client.notifyMessage("[King:]"+game.getMap().getKing().getCouncilBalcon().toString());	
	}

	@Override
	public void NoFastAction() throws RemoteException {
		game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
	}
	
	private void notifyChange(String message, String change) throws RemoteException{
		Iterator<IClientRMI> iter = clientSet.iterator();
		while (iter.hasNext()) {
			IClientRMI client = iter.next();
			client.notifyMessage(message);
			client.notifyMessage(change);
		}
	}
	@Override
	public void displayPlayerSituation(IClientRMI client) throws RemoteException {
		client.notifyMessage(getPlayerFromClient(client).toString());
	}

	@Override
	public void displayMap(IClientRMI client) throws RemoteException {
		client.notifyMessage(game.getMap().displayMap());
	}

	@Override
	public void displayConnectionCity(IClientRMI client) throws RemoteException {
		for(City city: game.getMap().getCityList()){
			client.notifyMessage("City: ["+ city.getName()+"]");
			client.notifyMessage("Linked city: ["+ city.getFriends()+"]");
		}
	}
	
	@Override
	public void displayPlayerPoliticCard(IClientRMI client) throws RemoteException{
		Player p= this.getPlayerFromClient(client);
		client.notifyMessage(p.displayPoliticCard());
	}

	@Override
	public boolean isMyTurn(IClientRMI clientRMI) throws RemoteException {
		if(this.takeCurrentClient()==clientRMI){
			return true;
		}
		else return false;
	}

	@Override
	public void passTurnMarket(IClientRMI clientRMI) throws RemoteException {
		game.getStateGame().getCurrentMarket().getSell().getCurrentTurn().removeAction();
		passTurn();
	}

	@Override
	public void displayKingCity(IClientRMI clientRMI) throws RemoteException {
		clientRMI.notifyMessage("king city: "+ game.getMap().getKing().getCurrentCity().getName());
	}
	
	@Override
	public void displayProductOnSale(IClientRMI clientRMI) throws RemoteException {
		clientRMI.notifyMessage(game.productListToString());
		
	}

	@Override
	public void passTurnMarketBuy(IClientRMI clientRMI) throws RemoteException {
		clientRMI.notifyMessage("TURN PASSED");
		game.getStateGame().getCurrentMarket().getBuy().getCurrentTurn().removeAction();
		passTurn();
		
	}
	private void displayKingBalcon(IClientRMI client) throws RemoteException{
		client.notifyMessage("THE KING BALCON: ");
		client.notifyMessage(game.getMap().getKing().getCouncilBalcon().toString());
	}
	@Override
	public int getDimensionSellList(IClientRMI clientRMI) throws RemoteException {
		return game.getProductToSell().size();
	}
	
	@Override
	public void run() {
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry(SERVER_RMI_PORT);
			registry.rebind(NAME_MANAGER, this);	
			this.startGame(game);
		} catch (RemoteException e) {
			Logger.getGlobal().log(Level.SEVERE,"error locate manager", e);
		}	
	}
	
	public void setGame(GameBoard game){
		this.game=game;
	}
}
