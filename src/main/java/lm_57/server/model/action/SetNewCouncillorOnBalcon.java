package lm_57.server.model.action;

import java.rmi.RemoteException;

import lm_57.server.model.Constants;
import lm_57.server.model.Player;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.Councillor;

public class SetNewCouncillorOnBalcon extends AMainAction {
	
	private CouncilBalcon council;
	private Councillor councillor;
	private Player agentPlayer;
	
	public SetNewCouncillorOnBalcon(Player agentPlayer,CouncilBalcon council,Councillor newCouncillor) throws RemoteException{
		this.agentPlayer=agentPlayer;
		this.council=council;
		this.councillor=newCouncillor;
	}
	@Override
	public boolean isCompleted(){
		return addCoinForSetCouncillor() && changeCouncillor();
	}
	
	private boolean addCoinForSetCouncillor(){
		int tmp=this.agentPlayer.getPlayerCoin();
		if((tmp=tmp+4)<=Constants.MAX_COIN)
			this.agentPlayer.addPlayerCoin(4);
		else
			this.agentPlayer.setPlayerCoin(Constants.MAX_COIN);
		return true;
	}
	private boolean changeCouncillor(){
		this.council.setCouncillorOnBalcon(councillor);
		return true;
	}

	
}