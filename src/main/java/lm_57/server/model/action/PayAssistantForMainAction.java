package lm_57.server.model.action;

import lm_57.server.model.Player;

public class PayAssistantForMainAction extends AFastAction{
	
	private Player agentPlayer;
	
	public PayAssistantForMainAction(Player agentPlayer){
		this.agentPlayer=agentPlayer;
	}
	
	public boolean isCompleted(){
		return payAssistant(); 
	}
	
	private boolean payAssistant(){
		
		if(this.agentPlayer.getPlayerAssistant()>=3){
			this.agentPlayer.removePlayerAssistant(3);
			return true;
		}
		else
			return false;
	}
}
