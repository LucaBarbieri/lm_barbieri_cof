package lm_57.server.model.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.Councillor;

public class ElectedCouncilor extends AFastAction{
	
	private final CouncilBalcon council;
	private final Councillor councillor;
	private Player agentPlayer;
	
	public ElectedCouncilor(Player agentPlayer,final CouncilBalcon council,final Councillor councillor) throws RemoteException{
		this.agentPlayer=agentPlayer;
		this.council=council;
		this.councillor=councillor;
	}
	
	public boolean isCompleted(){
		return payAssistant() && electedCouncillor();
	}
	
	private boolean payAssistant(){
		this.agentPlayer.removePlayerAssistant(1);
		return true;
	}
	
	private boolean electedCouncillor(){
		this.council.setCouncillorOnBalcon(councillor);
		return true;
	}
	
	
}
