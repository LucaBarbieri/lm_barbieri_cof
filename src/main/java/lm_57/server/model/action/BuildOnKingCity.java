package lm_57.server.model.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.King;

public class BuildOnKingCity extends AMainAction {
	
	private City city;
	private King king;
	private Player agentPlayer;
	
	public BuildOnKingCity(Player agentPlayer,City city,King king) throws RemoteException{
		this.agentPlayer=agentPlayer;
		this.city=city;
		this.king=king;
	}
	
	public boolean isCompleted() {
		return buildEmporiumOnKingCity();
		
	}
	
	private boolean buildEmporiumOnKingCity(){
		if(checkPlayerPoliticCard()){
			king.moveKing(city);
			this.agentPlayer.buildEmporium(city);
			return true;
		}
		return false;
	}
	
	private boolean checkPlayerPoliticCard(){
		int coinForBalcon=this.king.getCouncilBalcon().coinPoliticCard(this.agentPlayer.getPoliticCard());
		if(this.agentPlayer.getPlayerCoin()>=coinForBalcon){
			this.agentPlayer.removePlayerCoin(coinForBalcon);
			this.agentPlayer.removePlayerPoliticCard(this.king.getCouncilBalcon().returnPoliticCardToRemove());
			return true;
		}
		return false;
	}	
}