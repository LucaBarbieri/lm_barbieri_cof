package lm_57.server.model.action;


import java.util.HashSet;
import java.util.Set;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.utility.PermitCard;

public class BuildEmporiumByPermitCard extends AMainAction{

	private PermitCard permitCard;
	private City city;
	private Set<String> allowedCity=new HashSet<>();
	private Player agentPlayer;
	
	public BuildEmporiumByPermitCard(Player agentPlayer,PermitCard cardPlayer, City city){
		this.agentPlayer=agentPlayer;
		permitCard=cardPlayer;
		this.city=city;
	}
	@Override
	public boolean isCompleted() {
		return buildEmporiumOnPermitCardCity();
	}
	
	private boolean buildEmporiumOnPermitCardCity(){
		allowedCity=permitCard.getCityAllowed();
		if(allowedCity.contains(city.getName())){
			payAssistantForBuildingAnEmporium();
			this.agentPlayer.buildEmporium(city);
			this.agentPlayer.getRelativeBonus(city.getBonus());
			return true;
		}
		else
			return false;
	}
	
	private int payAssistantForBuildingAnEmporium(){
		int coinForNumCity=city.getEmporium().size();
		this.agentPlayer.removePlayerAssistant(coinForNumCity);
		return coinForNumCity;
	}


	
	

}
