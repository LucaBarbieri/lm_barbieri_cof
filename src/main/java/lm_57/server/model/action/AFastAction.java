package lm_57.server.model.action;

public abstract class AFastAction extends Action {
	
	@Override
	public abstract boolean isCompleted();
	
}
