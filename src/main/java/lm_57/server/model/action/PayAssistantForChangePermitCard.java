package lm_57.server.model.action;

import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.map.Region;

public class PayAssistantForChangePermitCard extends AFastAction {
	
	private Region region;
	private Player agentPlayer;
	
	public PayAssistantForChangePermitCard(Player agentPlayer,Region region) throws RemoteException{
		this.agentPlayer=agentPlayer;
		this.region=region;
	}
	
	public boolean isCompleted(){
		return payAssistant() && changePermitCard();
	}
	
	private boolean payAssistant(){
		if(this.agentPlayer.getPlayerAssistant()>=1){
			this.agentPlayer.removePlayerAssistant(1);
			return true;
		}
		else
			return false;
	}
	
	private boolean changePermitCard(){
		region.setVisiblePermitCard(2);
		return true;
	}
	
}
