package lm_57.server.model.action;

import lm_57.server.model.Player;

public class BuyAssistant extends AFastAction {

	private Player agentPlayer;
	
	public BuyAssistant(Player agentPlayer){
		this.agentPlayer=agentPlayer;
	}
	
	public boolean isCompleted(){
		return payForAssistant();
	}
	
	private	boolean payForAssistant(){
		this.agentPlayer.removePlayerCoin(3);
		this.agentPlayer.addPlayerAssistant(1);
		return true;
	}
	
}
