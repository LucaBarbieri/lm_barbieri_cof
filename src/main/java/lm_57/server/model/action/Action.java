package lm_57.server.model.action;


import lm_57.server.model.Player;

public abstract class Action {
	
	protected Player agentPlayer;
	
	public void setAgent(Player player){
		agentPlayer=player;
	}
	
	public Player getAgent(){
		return agentPlayer;
	}
		
	public abstract boolean isCompleted();
	
}
