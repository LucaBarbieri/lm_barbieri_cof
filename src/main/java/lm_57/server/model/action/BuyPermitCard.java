package lm_57.server.model.action;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import lm_57.server.model.Player;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class BuyPermitCard extends AMainAction{
	
	private Region region;
	private int indexPermitCard;
	private List<PermitCard> permitCard;
	private Player agentPlayer;
	
	public BuyPermitCard(Player agentPlayer,Region region,int indexPermitCard) throws RemoteException{	
		this.agentPlayer=agentPlayer;
		this.region=region;
		this.indexPermitCard=indexPermitCard;
		this.permitCard=new ArrayList<>();
	}
	
	public boolean isCompleted() {
		return buyPermitCard();
	}
	
	private boolean checkPlayerPoliticCard(){
		this.agentPlayer.removePlayerCoin(this.region.getCouncilBalcon().coinPoliticCard(this.agentPlayer.getPoliticCard()));
		this.agentPlayer.removePlayerPoliticCard(this.region.getCouncilBalcon().returnPoliticCardToRemove());
		return true;
	}
	
	private boolean buyPermitCard(){
		if(checkPlayerPoliticCard()){
			permitCard.add(this.region.getVisibleCard().get(indexPermitCard));
			this.region.removePermitCard(indexPermitCard);
			this.region.addVisiblePermitCard();
			this.agentPlayer.addPlayerPermitCard(permitCard);
			return true;
		}
		return false;
	}
}
