package lm_57.server.model;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.controller.User;
import lm_57.server.controller.state.IState;
import lm_57.server.controller.state.StateGameContext;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;
import lm_57.server.model.market.ProductToSell;



public class GameBoard extends Observable implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3618981645890451029L;
	private final String name;
	private final Set<Integer> positionBonus=new HashSet<>();
	private int startCoin;
	private int startAssistant;
	private int numMap;
	private int numBonus;
	private StateGameContext state;
	private List<ProductToSell> marketProduct=new ArrayList<>();
	

	private Map map;
	private final Queue<Player> players =new LinkedList <>();


	/**
	 * create the gameBoard with the map 
	 * @param numBonus for each permitCard and city 
	 * @param numMap of the configuration file
	 * 
	 * @throws IOException 
	 * 
	 */
	
	public GameBoard(String name) {
		super();
		this.name=name;
		startAssistant=0;
		startCoin=5;
		state=null;		
	}
	
	public void setNumMap(int numMap){
		this.numMap=numMap;
	}
	public void setNumBonus(int numBonus){
		this.numBonus=numBonus;
	}
	
	public void setMap() throws IOException{
		Config file= new Config(numMap, numBonus);
		file.buildingMap();
		map=file.getMap();
	}
	public Set<Integer> getPositionBonus(){
		return positionBonus;
	}
	
	public void setPositionBonus(Set<Integer> setBonus){
		this.positionBonus.addAll(setBonus);
	}
	/**
	 * set the first turn
	 */
	private void setInitialTurn(){
		state= new StateGameContext(players);
	}
	/**
	 * start the game creating the players and setting the initial turn of the game
	 * @param numPlayer
	 * @param playerName are the user list 
	 * @throws RemoteException
	 */
	public void start() throws RemoteException{
		try{
		this.setMap();
		}
		catch(IOException e){
			Logger.getAnonymousLogger().log(Level.SEVERE,"cannot instanziate map");
		}
		this.setInitialTurn();
	}
	
	/**
	 * add a product to the list during market round
	 */
	public void addProductToSell(ProductToSell object){
		this.marketProduct.add(object);
	}
	public void removeProduct(int index ){
		this.marketProduct.remove(index);
	}
	public void clearProduct(){
		marketProduct.clear();
	}
	
	public List<ProductToSell> getProductToSell(){
		return this.marketProduct;
	}
	
	public void setNextTurn(){
		state.setNextState();
	}
	
	public Player getCurrentPlayerTurn(){
		return state.getCurrentState().getCurrentAgent();
	}
	
	public String getTurnColor(){
		return state.getCurrentState().getColorAgent();
	}
	
	public String getCurrentPlayerToString(){
		return this.getCurrentPlayerTurn().getName();
	}
	
	/**
	 * create the players of the game 
	 * @param numPlayer
	 * @param playerName
	 * @throws RemoteException
	 */
	
	public void addPlayer(int numPlayer,List<User> playerName ) throws RemoteException{
		List<Player> playerGame=new ArrayList<>();
		
		for(int i=0;i<playerName.size();i++){
			playerGame.add(i, new Player(playerName.get(i).getName(),startCoin,startAssistant,Constants.convertStringToColor(playerName.get(i).getColor()), this.positionBonus));
			startCoin++;
			if(startAssistant<=5){
				startAssistant++;
			}
		}
		
		this.players.addAll(playerGame);
	}
	
	public String getGameName(){
		return this.name;
	}
	
	public Map getMap(){
		return map;
	}

	public Queue<Player> getPlayers(){
		return this.players;
	}
	
	/**
	 * displayed the current situation of game, so the state (round, market, or gameCompleted) and the current turn
	 * @return
	 */
	
	public String displayCurrentTurn(){
		return state.toString();
	}
	
	/**
	 * notify change to the observer 
	 */
	public void notifyChange(){
		setChanged();
		notifyObservers(this);
	}
	
	public StateGameContext getStateGame(){
		return state;
	}
	
	public IState getState(){
		return state.getCurrentState();
	}
	
	/**
	 * get the list of product to sell during market to string
	 * @return a string of product by index 
	 */
	public String productListToString(){
		String string=new String();
		string="OBJECT ON SALE : \n";
		for(int i=0; i<this.marketProduct.size();i++){
			string += "\nindex: ["+i+" ]"+"object:" +marketProduct.get(i).getName() 
					+" Quantity: "+marketProduct.get(i).getQuantity() +"\nPrice: "+ marketProduct.get(i).getCoin();
		}
		return string;
	}
	
	public void removePlayer(Player p){
		this.players.remove(p);
	}
	
}
    
  
    
 
    
   