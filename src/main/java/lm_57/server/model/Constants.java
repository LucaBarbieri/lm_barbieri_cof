package lm_57.server.model;


import java.awt.Color;

public final class Constants {

	/**
	* server timer 
	*/
	public static final int TIMEOUT_START_GAME = 20000;
	public static final int MAX_PLAYERS = 10;
	public static final int MIN_PLAYERS = 2;

	
	/**
	* model starting option
	*/
	public static final int NUM_POLITICCARD=5;
	public static final int START_NOBILITY=0;
	public static final int START_VICTORY=0;
	public static final int MAX_COIN=25;
	public static final int NUM_EMPORIUM=10;
	public static final int NUMREGIONPERMITCARDS=20;
	public static final int NUM_CITY_FOR_REGION=5;
	public static final int MAXNUMBONUS = 5;
	public static final int NUMBEROFMAP = 8;
	
	/**
	 * politic card colors
	 */
	public static final Color MULTICOLOR=Color.CYAN;
	private static final Color[] COLORS={Color.BLACK,Color.ORANGE,Color.BLUE,Color.MAGENTA,Color.WHITE,Color.PINK,Color.CYAN};
	
	/**
	 * strings of cities, bonus type, and regions
	 */
	private static final String[] NAMEOFBONUS={"Coin","Victory","Nobility","PoliticCard","Assistant"};
	public static final String[] REGIONS={"Hills","Mountain","Coast"};
	private static final String[] NAMEOFCITIESREGION1={"Arkon","Burgen","Castrum","Dorful","Esti"};
	private static final String[] NAMEOFCITIESREGION2={"Framek","Graden","Hellar","Juvelar","Indur"};
	private static final String[] NAMEOFCITIESREGION3={"Kultos","Naris","Lyram","Osium","Merkatim" };
	
	/**
	 * colors for the player in the game
	*/
	private static final Color[] COLORSUPPORTED={Color.BLACK,Color.ORANGE,Color.BLUE,Color.MAGENTA,Color.WHITE,Color.PINK,Color.CYAN,Color.RED,Color.GREEN,Color.YELLOW,Color.GRAY};
	private static final String[] COLORSUPPORTEDSTRING={"Black","Orange","Blue","Magenta","White","Pink","Cyan","Red","Green","Yellow","Gray"};
	
	/**
	 * Game mode
	 */
	
	public static final String LOCAL="Local";
	public static final String NET="Network";
	
	/**
	 * Net Mode
	 */
	
	public static final String RMI="RMI";
	public static final String SOCKET="Socket";
	
	/**
	* RMI
	*/
	public static final int SERVER_RMI_PORT = 1099;
	public static final String NAME_MANAGER= "MatchManager";
	public static final String GAME_NAME_PREFIX = "CouncilOfFour_Match_";
	public static final String NAME_SERVER= "CouncilOfFour_Server";
	private static int num_Connection=0;
	private static int matchNumber=0;
	private static int numGameboard=0;
	
	private Constants(){
		
	}
	
	/**
	 * generate the name of the game match for the current players 
	 * incrementing the number of match
	 * 
	 * @return 
	 * 			the name to saved in the registry RMI
	 */
	public static synchronized String generateMatchName() {
		matchNumber++;
		return GAME_NAME_PREFIX + matchNumber;
	}
	
	/**
	 * reset the number of connection at the beginning of every match
	 * 
	 */
	
	public static  synchronized void resetConnections(){
		num_Connection=0;
	}
	
	public static int getConnectionNumber(){
		return num_Connection;
	}
	
	/**
	 * add a connection
	 */
	public static void addConnection(){
		num_Connection++;
	}
	
	public static Color[] getColors(){
		return COLORS;
	}
	
	public static String[] getNameOfBonus(){
		return NAMEOFBONUS;
	}
	
	public static String[] getNameRegions(){
		return REGIONS;		
	}
	
	public static String[] getNameOfCitiesRegion1(){
		return NAMEOFCITIESREGION1;
	}
	public static String[] getNameOfCitiesRegion2(){
		return NAMEOFCITIESREGION2;
	}
	public static String[] getNameOfCitiesRegion3(){
		return NAMEOFCITIESREGION3;
		
	}
	public static String[] getColorSupportedString(){
		return COLORSUPPORTEDSTRING;
	}
	public static Color[] getColorSupported(){
		return COLORSUPPORTED;
	}
	
	public static Color convertStringToColor(String color){
		   for(int i=0;i<Constants.getColorSupportedString().length;i++){
			   if(color.equals(getColorSupportedString()[i])){
				   return getColorSupported()[i];
			   }
		   }
		   return null;
	}
	
	public static boolean isInRegion(String nameRegion){
		for(int i=0;i<REGIONS.length;i++){
			if(REGIONS[i].equals(nameRegion)){
				return true;
			}
		}
		return false;
	}
	
	public static String convertColorToString(Color color){
		
		for(int i=0;i<Constants.getColorSupported().length;i++){
			if(color.equals(getColorSupported()[i])){
				return getColorSupportedString()[i];
			}
		}
		return null;
	}
	
	public static int getTimeOut(){
		return TIMEOUT_START_GAME;
	}
	public static int getNumGameBoard(){
				return numGameboard;
			}
			
	public static void addGameBoard(){
			       numGameboard++;
			}
			
}