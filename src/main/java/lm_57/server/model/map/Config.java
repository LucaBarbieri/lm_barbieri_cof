package lm_57.server.model.map;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.model.Constants;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;

public class Config {
    

	private int mapNum;
	private int numBonus;
	private Map map;
	private BufferedReader buff;
	private InputStream in;



	public Config(int mapNum,int numBonus) throws IOException{
		this.mapNum=mapNum;
		this.numBonus=numBonus;
		this.in=getClass().getClassLoader().getResourceAsStream("config.txt");
		this.buff=new BufferedReader(new InputStreamReader(in));
		
}
	public String readConfig(){
		String temp= new String();
		try {
			 temp=this.buff.readLine();
			 } catch (IOException e) {
			
			Logger.getGlobal().log(Level.CONFIG,"Error with file", e);
		}
		return temp;
	} 
	
	public void findMap() throws IOException{
		int mapFlag=0;
		String stringFlag;
	    while(mapFlag!=this.mapNum){
	    	stringFlag=this.readConfig();
				if("*".equals(stringFlag)){
					mapFlag++;
					}
				}
	    
	}
	
	

	
	public City createCity(int region) throws IOException{
		City city;
		String cityFriends;
		Color color;
		String cityName=this.readConfig();
		String colorString=this.readConfig();
		int friendsFlag=Integer.parseInt(this.readConfig());	
		color=Constants.convertStringToColor(colorString);
		city=chooseCity(region, cityName, color);  
	    for(int i=0;i<friendsFlag;i++){
		         
				 cityFriends=this.readConfig();
				 city.addFriends(cityFriends);
}
		return city;
			}
	/**
	 * 
	 * @param city
	 * @param r
	 * @param cityName
	 * @param k
	 * @return 
	 * @throws RemoteException
	 */
			
	public City chooseCity(int r, String cityName,Color color) throws RemoteException{
		if(r==0){
			return new City(cityName,"Hills",color,this.numBonus);
		}
		if(r==1){
			return new City(cityName,"Coast",color,this.numBonus);
		}
		if (r==2){
			return new City(cityName,"Mountains",color,this.numBonus);
		}
		return null;
	}
		
	public void buildingMap() throws IOException{
		int region=0;
		int cityRegion=0;
		City cityFlag;

		if (this.mapNum!=1){
			this.findMap();
		}
		this.map=new Map(this.numBonus);
		while(region<3){
			   while(cityRegion<5){				
                 cityFlag=this.createCity(region);
				this.map.addCity(cityFlag);
				this.map.getRegionList().get(region).addCity(cityFlag);
				
				cityRegion++;	
			}
			region++;
			cityRegion=0;
		}
		
		map.putCityInFriends();
		
		map.setKing(map.getCity("Juvelar"));
	}
	public Map getMap(){
		return this.map;
	}
	
	
	}