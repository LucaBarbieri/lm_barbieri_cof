package lm_57.server.model.map;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.*;

import lm_57.server.model.bonus.Bonus;
import lm_57.server.model.utility.Emporium;


public class City {

	private final String name;
	private final String regionName;
	private final Color color;
	private  final Bonus cityBonus;
	private final List<Emporium> emporium= new ArrayList<>();
	
	
	
	private final List<City> linkedCity=new ArrayList<>();
	private final List<String> cityFriends=new ArrayList<>();
		
	public City(final String name,String regionName, final Color color, int numBonusCity)throws RemoteException{
		this.name=name;
        this.color=color;
		this.cityBonus=new Bonus(numBonusCity);
		this.regionName=regionName;
		
	}
	public void addFriends(String cityFriends){
		this.cityFriends.add(cityFriends);
	}
	public void addLinkedCity(City city){
		this.linkedCity.add(city);
	} 
	
	public List<String> getFriends(){
		return this.cityFriends;
		
	}

		
	public boolean isConnected(City connectTo){
		return linkedCity.contains(connectTo);
	}
	
	public void connectTo(City cityToConnect){
		if(!this.isConnected(cityToConnect)){
			linkedCity.add(cityToConnect);
			cityToConnect.connectTo(this);
		}
	}
	public List<City> getLinkedCity(){
		return linkedCity;
	}
	
	public void addEmporium(Emporium e){
			emporium.add(e);
	}

	
	public List<Emporium> getEmporium(){
		return this.emporium;
	}
	public int emporiumListSize(){
		return emporium.size();
	}
	public Color getColor(){
		return this.color;
	}
	public Bonus getBonus(){
		return cityBonus;
	}
	public String getName(){
		return this.name;
	}


	public String ConnectCitiesToString(){
		String string;
		string="LINKED CITY TO: "+ this.getName();
		for(int i=0;i<linkedCity.size();i++){
			string += " ["+getLinkedCity().get(i).getName()+" ]";
		}
		return string;
	}
	
	public String getRegionName(){
		return this.regionName;
	}
}
	
