package lm_57.server.model.map;

import java.rmi.RemoteException;
import java.util.*;

import lm_57.server.model.Constants;
import lm_57.server.model.utility.Councillor;
import lm_57.server.model.utility.PermitCard;

public class Region{
	
	private final String name;
	
	private  List<City> cities = new ArrayList<>();
	private CouncilBalcon council=new CouncilBalcon();
    private List<PermitCard> deckPermitCard=new ArrayList<>();
    private List<PermitCard> cardVisible=new ArrayList<>();
    private Random randomGenerator=new Random();
   
	
    /**
     * 
     * @param name of the region
     * @param numBonus random number of bonus to put on permit region deck
     * 
     */
	public Region(String name,int numBonus)throws RemoteException{
		this.name=name;
		generatePermitDeck(numBonus);
		setVisiblePermitCard(2);
	}
	
	/**
	 * generate the deck of the region with a random number of bonus on every cards
	 * 
	 * @param numBonus 
	 * @throws RemoteException 
	 */
	
	public void generatePermitDeck(int numBonus) throws RemoteException{
		for(int i=0;i<Constants.NUMREGIONPERMITCARDS;i++){
			deckPermitCard.add(new PermitCard(this.name,numBonus));
		}
	}
	
	/**
	 * 
	 * @param numCards is the number of visible permit card for the region
	 */
	
	public void setVisiblePermitCard(int numCards){      
		int index;
		 if(!this.cardVisible.isEmpty()){
			 deckPermitCard.addAll(cardVisible);
			 cardVisible.clear();
		 	}
          for(int i=0;i<numCards;i++){
            	  index=this.randomGenerator.nextInt(this.deckPermitCard.size());
            	  this.cardVisible.add(deckPermitCard.get(index));
            	  this.deckPermitCard.remove(index);
                 }
	}
	
	public void addVisiblePermitCard(){
		int index=this.randomGenerator.nextInt(this.deckPermitCard.size());
		this.cardVisible.add(deckPermitCard.get(index));
  	  	this.deckPermitCard.remove(index); 
	}
	/**
	 * allow to take a card from the visible permit cards and replace it from the deck
	 * 
	 * @param cardPlayer the card that player wants to take
	 * @return the card chose by the player or null if the card chose by the player is not present
	 * 
	 */
	
	public PermitCard takePermitCard(PermitCard cardPlayer){
		PermitCard card;
		         for(int i=0;i<cardVisible.size();i++){		        	 
		        	 if(cardVisible.get(i).equals(cardPlayer)){
		        		 card=cardVisible.get(i);
		        		 cardVisible.remove(i);
		        		 setVisiblePermitCard(1);
		        		 return card;
		        	 }
		         }
		return null;
	}
	
	public void removePermitCard(int indexPermitCardToRemove){
		cardVisible.remove(indexPermitCardToRemove);
	}
	
	public List<PermitCard> getVisibleCard(){
		return this.cardVisible;
	}
	
	public void addCity(City cityRegion){
		if (!containCity(cityRegion)){
			cities.add(cityRegion);
		}
	}
	
	public boolean containCity(City c){
		if (cities.contains(c)){
			return true;
		}
		return false;
	}

	
	public String getName(){
		return this.name;
	}
	
	
	public List<City> getCities(){
		return cities;
	}

	public CouncilBalcon getCouncilBalcon(){
		
		    return council;
	}
	
	public void setCouncillorOnBalcon(Councillor councillor){
		
		   this.council.setCouncillorOnBalcon(councillor);
	}	
	
	public Set<String> listCitiesToString(){
		Set<String> cityString=new HashSet<>();
		for(int i=0;i<cities.size();i++){
			cityString.add(cities.get(i).getName());
		}
		return cityString;
	}
	
	public Set<String> councilBalconToString(){
		Set<String> balconString=new HashSet<>();
		for(int i=0;i<council.getCouncilBalcon().size();i++){
			balconString.add(council.getCouncilBalcon().get(i).getColorCouncillor().toString());
		}
		return balconString;
	}
	/**
	 * set of visible card on this region
	 * @return a set of string of card
	 */
	public Set<String> visibleCardToString(){
		Set<String> cardString=new HashSet<>();
		for(int i=0;i<2;i++){
			cardString.add(cardVisible.get(i).PermitCardtoString());
		}
		return cardString;
	}
	
	public List<PermitCard> getPermitDeck(){
		return this.deckPermitCard;
	}	
	
	
	@Override
	public String toString() {
		return "Region [name=" + this.name + ", cities=" + listCitiesToString() 
		+ ", balcon=" + councilBalconToString() + ", cardVisible=" +
				visibleCardToString() + "]";
	}
	
}
