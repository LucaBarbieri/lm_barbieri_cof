package lm_57.server.model.map;



import java.rmi.RemoteException;
import java.util.List;

import lm_57.server.model.utility.Councillor;

public class King {
	
	private City currentCity;
	private final CouncilBalcon kingCouncil;	
	
	public King(City city )throws RemoteException{
		this.currentCity=city;
		kingCouncil=new CouncilBalcon();
	}
	
	public void moveKing(City newCity){
		this.currentCity=newCity;
	}
	public CouncilBalcon getCouncilBalcon(){
		return kingCouncil;
	}
	public List<Councillor> getCouncillorOnBalcon(){
		return kingCouncil.getCouncilBalcon();
	}
	public City getCurrentCity(){
		return currentCity;
	}
	
	public void setKingCouncillorOnBalcon(Councillor councillor){
		this.kingCouncil.setCouncillorOnBalcon(councillor);
	}
	
}
