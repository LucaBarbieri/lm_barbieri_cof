package lm_57.server.model.map;
import java.awt.Color;
import java.rmi.RemoteException;
import java.util.*;

import lm_57.server.model.Constants;
import lm_57.server.model.utility.Councillor;
import lm_57.server.model.utility.PoliticCard;

public class CouncilBalcon {

	
	LinkedList<Councillor> balcon=new LinkedList<>();
	private List<PoliticCard> politicCardToRemove=new ArrayList<>();
	
	
	public CouncilBalcon() throws RemoteException{
		for(int i=0;i<4;i++){
			balcon.add(new Councillor());
		}
	}
	
	public void setCouncillorOnBalcon(Councillor council){
		       
		       this.balcon.removeLast();
		       this.balcon.add(0,council);
	 }
	
	
	public Color[] getCouncilBalconColor(){
		Color[] color=new Color[4];
		for(int i=0;i<balcon.size();i++){
			color[i]= balcon.get(i).getColorCouncillor();
		}
		return color;
	}
	public List<Councillor> getCouncilBalcon(){
		return this.balcon;
	}
	
	public int  coinPoliticCard(List<PoliticCard> politicCards){
		
	          int numCoinMulticolor=comparateMulticolorCard(politicCards);
              int numCoinCard=comparateCards(politicCards);
             return numCoinCard+numCoinMulticolor;
	}
	
	public int comparateMulticolorCard(List<PoliticCard> cards){
	
		 int coin=0;
		 for(PoliticCard currentCard:cards){
			 if(currentCard.getPoliticCard().equals(Constants.MULTICOLOR)){
				 coin+=2;
				 this.politicCardToRemove.add(currentCard);
			 }
		 }		
		return coin;
	}
	
	public int comparateCards(List<PoliticCard> card){
		    
		      Councillor councillor;
		      PoliticCard politicCard;
		      int coin=8;
		      for(int i=0;i<card.size();i++){
		      politicCard=card.get(i);
		        for(int j=0;j<balcon.size();j++){
		        	  councillor=balcon.get(j);		        	  
		           	if(councillor.getColorCouncillor().equals(politicCard.getPoliticCard())){
		           		coin-=2;
		           		this.politicCardToRemove.add(politicCard);
		           		
		           		break;
		           	}
		        }
		        
		        }
		      return coin;
		  
              
	}
	
	public List<PoliticCard> returnPoliticCardToRemove(){
		return this.politicCardToRemove;
	}
	
	@Override
	public String toString(){
		String string;
		string="Council Balcon: ";
		for(Councillor councillor: this.balcon){
			string += "[ "+ councillor.toString()+ " ]";
		}
		return string;
	}

}