package lm_57.server.model.map;

import java.io.Serializable;

public class Coin implements Serializable{ 
	
	private int numCoin;
	
    public Coin (int coin){ 
        this.numCoin=coin;
     }
    
    public int getCoin(){
		return this.numCoin;}
	
    public void setCoin(int numCoin){
    	this.numCoin=numCoin;
    }
	
    public void addCoin(int numCoin ){
		this.numCoin+=numCoin; 
	}
    public void removeCoin(int numCoin){
    	this.numCoin-=numCoin;
    }
}
	
	
	