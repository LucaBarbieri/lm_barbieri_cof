package lm_57.server.model.map;

import static lm_57.server.model.Constants.START_NOBILITY;
import java.util.HashSet;

import java.util.Set;

import lm_57.server.model.bonus.Bonus;

public class Nobility {
	private int numNobility;
	private Bonus bonusNobility;
	private HashSet<Integer> positionBonus=new HashSet<>();

	
	public Nobility(Set<Integer> positionBonus){
	this.numNobility=START_NOBILITY;
	this.bonusNobility=new Bonus(2);
	this.positionBonus=(HashSet<Integer>) positionBonus;
   }
	
	public Bonus addNobility(int numNobility){
		this.numNobility+=numNobility;
		 for(Integer currentPosition:this.positionBonus){
			 if(this.numNobility==currentPosition){
				 this.bonusNobility=new Bonus(2);
				 return this.bonusNobility;
			 }
		 }
		 return null;
	}
	
	public int getNobility(){
		 return this.numNobility;
	}
	

}
