package lm_57.server.model.map;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class Map {
	
	private  ArrayList<City> city=new ArrayList<>(); //city list
	
	private  ArrayList<Region> region=new ArrayList<>();//regioni sempre definite e costuite
	private King king;

	
	public Map(int numBonus) throws RemoteException {
		
		Region Hills= new Region ("Hills", numBonus);
		Region Mountain= new Region("Mountain", numBonus);
		Region Coast= new Region ("Coast", numBonus);
		
			
		
		this.region.add(Hills);
		this.region.add(Coast);
		this.region.add(Mountain);
		
		}
	
	
	public void putCityInFriends(){
		for(City temp1: this.city){
			 for(int i=0;i<temp1.getFriends().size();i++){
				 for(City temp2: this.city){
			if(temp1.getFriends().get(i).equals(temp2.getName())){
				 temp1.addLinkedCity(temp2);
				
				 }
			 }
				 }
		}
		
	}
	
	public List<String> getCitiesName(){
		List<String> cityName=new ArrayList<>();
		for(City cities: this.city){
			cityName.add(cities.getName());
		}
		return cityName;
	}
	
	public List<Region> getRegionList(){
		return this.region;
	}
	
	public List<City> getCityList(){
		return this.city;
	}
	
	public City getCity(String name){
		for(City c: city){
			if(c.getName().equals(name)){
				return c;
			}
		}
		return null;
	}
	
	public void addCity(City city){
		this.city.add(city);
	}
	
	
   public void setKing(City city) throws RemoteException{
	   this.king=new King (city);
   }

   /**
    * convert the region name in the object region
    * @param nameRegion
    * @return
    */
	
   public Region getRegionFromName(String nameRegion){
		
		for(Region temp: region){
			if(temp.getName().equals(nameRegion))
					return temp;
				
			}
	   return null;
		}
	

	public King getKing(){
		return king;
	}
	
	public Region getRegionCity(City c){
		for (Region temp:region){
			if(temp.getName().equals(c.getRegionName()))
				return temp;
		}
		return null;
	}
	
	public List<String> regionListToString(){
		List<String> regions=new ArrayList<>();
		for(Region currentRegion: this.region){
			regions.add(currentRegion.getName());
		}
		return regions;
	}
	
	public String displayMap(){
		String string;
		string="Map:";
		for(Region region: this.getRegionList()){
			string +=" [Region: "+region.getName()+ "[Cities: "+region.listCitiesToString()+"]\n";
		}
		return string;
	}
	

	}