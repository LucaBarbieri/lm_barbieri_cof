package lm_57.server.model.map;

import java.io.Serializable;
import static lm_57.server.model.Constants.START_VICTORY;

public class Victory implements Serializable{

	private int numVictory;
	
	public Victory(){
		this.numVictory=START_VICTORY;            
	}
	
	public int getVictory(){
		return numVictory;
	} 
	
	public void setVictory(int numVictory){
		this.numVictory=numVictory;
	}
	
	public void addVictory(int numVictory){
		this.numVictory+=numVictory;
	}                                // Method to set victory points 
}
