package lm_57.server.model.utility;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import lm_57.server.model.Constants;
import lm_57.server.model.bonus.Bonus;

public class PermitCard{

	private HashSet<String> cityAllowConstruction=new HashSet<>();
    private Bonus permitBonus;
    
    
    public PermitCard(String region,int numBonus) throws RemoteException{
    	int j=(int)(1+3*Math.random());
    	int p=(int) (1+numBonus*Math.random());
    	boolean eq=true;
    	permitBonus=new Bonus(p);
         for(int i=0;i<j;i++){
        	 do{
        	 eq=this.cityAllowConstruction.add(setCityAllow(region));
        	 }while(eq==false);
         }
          
    }
                

   public Set<String> getCityAllowed(){
    	return cityAllowConstruction;
    }
 
    public Bonus getPermitBonus(){
    	return permitBonus;
    }
    
    public String setCityAllow(String region){
    	String tmp;
    	int k=(int)(5*Math.random());
    	switch(region){
    	case "Mountain":
    	     tmp=Constants.getNameOfCitiesRegion1()[k];
    	     break;
    	case "Hills":
    	      tmp=Constants.getNameOfCitiesRegion2()[k];
    	      break;
    	case "Coast":
    	      tmp=Constants.getNameOfCitiesRegion3()[k];
    	      break;
    	default:
    		  tmp=null;
    	}
    	return tmp;   
    }



	
	public String PermitCardtoString() {
		return "PermitCard \n"+ this.getCityAllowed() + this.getPermitBonus().toString();
	} 
	
    


}