package lm_57.server.model.utility;

import java.awt.Color;
import java.io.Serializable;

import lm_57.server.model.Constants;

public class PoliticCard implements Serializable{
    
	  /**
	 * 
	 */
	private static final long serialVersionUID = 5920922038230601208L;
	private Color colorPoliticCard;
		
	     public PoliticCard(){
	    	 int randomIndex=(int) (Constants.getColors().length*Math.random());
	    	 this.colorPoliticCard=Constants.getColors()[randomIndex];
	     } 
	     
	     public Color getPoliticCard(){
	    	 return this.colorPoliticCard;
	     }
	 
	     @Override
	     public String toString(){
	    	 return Constants.convertColorToString(this.colorPoliticCard);
	     }
		
}
