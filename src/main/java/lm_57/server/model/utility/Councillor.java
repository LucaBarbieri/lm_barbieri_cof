package lm_57.server.model.utility;
import java.awt.*;

import lm_57.server.model.Constants;
public class Councillor {
    private Color councillorColor;
	
	public Councillor(){
		    int i=(int) (Constants.getColors().length*Math.random()-1);
            this.councillorColor=Constants.getColors()[i];		
	}	
	public Color getColorCouncillor(){
		return this.councillorColor;
	}
	public Councillor getCouncillor(){
		return this;
	}

	public void setCouncillorColor(Color color){
		this.councillorColor=color;
	}
	
	@Override
	public String toString(){
		return Constants.convertColorToString(this.councillorColor);
	}
}
