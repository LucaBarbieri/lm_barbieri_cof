package lm_57.server.model.utility;

import java.io.Serializable;

public class Assistant implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7511085200880034046L;
	private int numAssistant;
	
	public Assistant(int numAssistant){
		this.numAssistant=numAssistant;
	}
	
	public void setAssistant(int number){
		this.numAssistant=number;
	}
	
	public void addAssistant(int numAssistant){
		this.numAssistant+=numAssistant;
	}
	
	public void removeAssistant(int numAssistant){
		if(this.numAssistant>=numAssistant){
		this.numAssistant-=numAssistant;
		}
	}
	
	public int getAssistant(){
		return this.numAssistant;
	}

}
