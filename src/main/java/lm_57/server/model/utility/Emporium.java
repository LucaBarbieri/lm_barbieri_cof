package lm_57.server.model.utility;

import java.awt.Color;
import java.rmi.RemoteException;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;


public class Emporium {
	
	
	private final Color colorOwner;
	private final Player owner;
	
	private City position;
	
	public Emporium(Player owner)throws RemoteException{
		this.colorOwner=owner.getPlayerColor();
		this.owner=owner;
		this.position=null;
	}
	
	/*
	 * 
	 */
		public void setPosition (final City city){
				for(int i=0;i<city.getEmporium().size(); i++){
	       			if(city.getEmporium().get(i).getColor().equals(this.getColor())){
						return;
		                 }
		               }

				if(city.getEmporium().isEmpty()){                                        
		 			city.addEmporium(this);
		 			this.position=city;
				}
				else{
					owner.removePlayerAssistant(city.emporiumListSize());
					this.position=city;
					city.addEmporium(this);
				}
		 	}

	public Color getColor(){
		return this.colorOwner;
	}
	
	public City getPosition(){
		return this.position;
	}
}
