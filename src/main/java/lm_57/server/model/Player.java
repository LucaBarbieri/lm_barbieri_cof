package lm_57.server.model;


import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lm_57.server.model.bonus.Bonus;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Coin;
import lm_57.server.model.map.Nobility;
import lm_57.server.model.map.Victory;
import lm_57.server.model.utility.Assistant;
import lm_57.server.model.utility.Emporium;
import lm_57.server.model.utility.PermitCard;
import lm_57.server.model.utility.PoliticCard;
import static lm_57.server.model.Constants.NUM_EMPORIUM;
import static lm_57.server.model.Constants.NUM_POLITICCARD;

public class Player {

	
	private final String name;
	private final Color color;
	
	
    private Coin coinNumber;
    private Nobility nobilityNumber;
    private Victory victoryNumber;
    private Assistant assistantNumber;
	
	private List<PermitCard> ownedPermitCard;
	private final List<Emporium> ownedEmporium=new ArrayList<>();
	private List<PoliticCard> ownedPoliticCard;
	
	/**
	 * 
	 * @param name player name
	 * @param startCoin the initial value of player coin
	 * @param startAssistant the initial number of player assistant
	 * @param color of player
	 * @param positionBonus the positions of bonus in the nobility track for the match
	 */
	
	public Player(final String name, int startCoin, int startAssistant, Color color,Set<Integer> positionBonus) throws RemoteException{
		
		this.name=name;
		this.color=color;
		ownedPermitCard=new ArrayList<>();
		ownedPoliticCard=new ArrayList<>();
		this.victoryNumber=new Victory();
		this.nobilityNumber=new Nobility(positionBonus);
		this.coinNumber=new Coin(startCoin);
		this.assistantNumber=new Assistant(startAssistant);
		
		for(int i=0; i<NUM_POLITICCARD;i++){
			this.ownedPoliticCard.add(new PoliticCard());
		}
	
		for (int i=0; i<NUM_EMPORIUM; i++){
			this.ownedEmporium.add(new Emporium(this));
		}
		
	}
	
	public String getName(){
		return name;
	}
	
	public List<PoliticCard> getPoliticCard(){
		return ownedPoliticCard;
	}
	
	public List<String> getPoliticCardToString(){
		List<String> playerPoliticCard=new ArrayList<>();
		
		for(PoliticCard p:this.getPoliticCard()){
			playerPoliticCard.add(p.getPoliticCard().toString());
		}
		return playerPoliticCard;
	}
	
	
	public void addPoliticCard(int numOfCard){
		for(int i=0; i<numOfCard; i++){
			ownedPoliticCard.add(new PoliticCard());
		}
	}
	
	public int getPlayerAssistant(){
		return this.assistantNumber.getAssistant();
	}
	
	public void addPlayerAssistant(int numAssistant){
		this.assistantNumber.addAssistant(numAssistant);
	}
	
	public void removePlayerAssistant(int numAssistant){
		if(this.assistantNumber.getAssistant()>=numAssistant){
			this.assistantNumber.removeAssistant(numAssistant);
			return;
		}
	}
	
	public void removePlayerCoin(int numCoin){
		if(this.getPlayerCoin()>=numCoin){
			this.coinNumber.removeCoin(numCoin);   
		}
	}
	
	public void removeEmporium(){
		ownedEmporium.clear();
	}
	
	public void addPlayerCoin(int numCoin){
		this.coinNumber.addCoin(numCoin);
	}
	public void setPlayerCoin(int numCoin){
		this.coinNumber.setCoin(numCoin);
	}
	
	public int getPlayerCoin(){
		 return this.coinNumber.getCoin();
	}
	public int getPlayerNobility(){
		return this.nobilityNumber.getNobility();
	}
	public int getPlayerVictory(){
		return this.victoryNumber.getVictory();
	}
	
	public void addPlayerVictory(int victory){
		this.victoryNumber.addVictory(victory);
	}
	public void addPlayerNobility(int nobility){
		this.nobilityNumber.addNobility(nobility);
	}
	
	/**
	 * build an emporium in a city and remove the emporium from the list
	 * @param city where the player wants to build
	 */
	
	public void buildEmporium(City city){
		ownedEmporium.get(0).setPosition(city);
		ownedEmporium.remove(0);
	}
	
	public List<Emporium> getPlayerEmporium(){
		return ownedEmporium;
	}
	
	public List<PermitCard> getPlayerPermitCard(){
		return ownedPermitCard;
	}
	public Color getPlayerColor(){
		return this.color;
	}
	
	/**
	 * remove a list of politic cards of the player 
	 * 
	 * @param politicCard list of player politic Card to remove
	 * 
	 */
	public void removePlayerPoliticCard(List<PoliticCard> politicCard){
		   for(PoliticCard tmp:politicCard){
			   this.ownedPoliticCard.remove(tmp);
		   }		
	}
	
	/**
	 * add a list of politic card when a player buy it during the market phase
	 * 
	 * @param politicCard list of politic cards that wants to buy
	 * 
	 */
	
	public void addPlayerPoliticCard(List<PoliticCard> politicCard){
		     this.ownedPoliticCard.addAll(politicCard);
	}
	
	/**
	 *  
	 * compare the bonus string b to all possible bonus and return to player the right one.
	 * 
	 * @param b bonus 
	 */
	
	public void getRelativeBonus(Bonus b){
		Set<String> stringBonus=new HashSet<>();
		Bonus bonusNobility=null;
		int[] numBonus=b.getBonusQuantity();
		int i=0;
		stringBonus.addAll(b.getBonusString());
		
		for(String bonus:stringBonus){
			if("Assistant".equals(bonus)){
				this.assistantNumber.addAssistant(numBonus[i]);
			}
			else if("Coin".equals(bonus)){
				this.coinNumber.addCoin(numBonus[i]);
			}
			else if ("Nobility".equals(bonus)){
				 bonusNobility=this.nobilityNumber.addNobility(numBonus[i]);
			}
			else if("Victory".equals(bonus)){
			      this.victoryNumber.addVictory(numBonus[i]);
			
			}
			else if("PoliticCard".equals(bonus)){
				 for(int k=0;k<numBonus[i];k++){
					 this.ownedPoliticCard.add(new PoliticCard());
				 }
			}
			i++;			
		}
		getBonusNobility(bonusNobility);
	}
	
	 public void getBonusNobility(Bonus bonus){
	    	
	    	if(bonus!=null){
	    		getRelativeBonus(bonus);
	    	}
	    }
	

	public void removePlayerPermitCard(List<PermitCard> permitCard) {
			   this.ownedPermitCard.removeAll(permitCard);
	}
	
	public void addPlayerPermitCard(List<PermitCard> permitCard){
		this.ownedPermitCard.addAll(permitCard);
	}
	
	private List<String> displayPlayerEmporium(){
		
		List<String> playerEmporiums=new ArrayList<>();
		
		for(int i=0; i<this.getPlayerEmporium().size();i++){
			if(this.getPlayerEmporium().get(i).getPosition() !=null){
			playerEmporiums.add( this.getPlayerEmporium().get(i).getPosition().getName());
			}
		}
			return playerEmporiums;
	}
		
		@Override 
		public String toString(){
			return " Player [name: "+ this.getName() +"]"+ " with [color: " + Constants.convertColorToString(this.color) + "]"
					+ "\n[coin: " + this.getPlayerCoin() +"]" + "\n[assistant: "+this.getPlayerAssistant()+ "]"+
					"\n[nobility: " + this.getPlayerNobility() +"]"	+ "\n[victory: " + this.getPlayerVictory() +"]"
					+ "\n[emporium on cities: " + this.displayPlayerEmporium()+"]\n";  
		}

		public String displayPermitCard(){
			String string =new String();
			string= "Owned Permit card: ";
			for(PermitCard card: this.ownedPermitCard){
				string +="["+ card.toString()+ "]";
			}
			return string;
		}
		
		public Set<String> getStringPermitCard(){
			Set<String> permit=new HashSet<>();
			for(PermitCard card: this.ownedPermitCard){
				permit.add(card.PermitCardtoString());
			}
			return permit;
		}
		public String displayPoliticCard(){
			String string =new String();
			string= "Politic Card: ";
			for(PoliticCard card: this.ownedPoliticCard){
				string +="["+ card.toString()+ "]";
			}
			return string;
		}
	}
	

	

	
	
	
	

