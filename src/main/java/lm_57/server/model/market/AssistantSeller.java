package lm_57.server.model.market;


import java.rmi.RemoteException;

import lm_57.server.model.Player;

public class AssistantSeller extends ProductToSell{
	

	int numAssistant;
	int numCoin;
	
	public AssistantSeller(Player player,int numAssistant,int numCoin) throws RemoteException{
		super(player);
		this.numAssistant=numAssistant;
		this.numCoin=numCoin;
	}
  	
	@Override
	public void sellingProduct(Player player){
		 player.removePlayerCoin(numCoin);
		 super.getPlayer().removePlayerAssistant(numAssistant);
		 player.addPlayerAssistant(numAssistant);
		 super.getPlayer().addPlayerCoin(numCoin);		 
	}
	
	@Override
	public String getName() {
		return "ASSISTANT";
	}


	@Override
	public int getCoin() {
		return this.numCoin;
	}


	@Override
	public int getQuantity() {
		return this.numAssistant;
	}

}
