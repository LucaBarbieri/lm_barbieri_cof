package lm_57.server.model.market;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import lm_57.server.model.Player;
import lm_57.server.model.utility.PoliticCard;

public class PoliticSeller extends ProductToSell{
	
	
	List<PoliticCard> politicCard=new ArrayList<>();
	int numCoin;
	
	
	public PoliticSeller(Player player,List<PoliticCard> politicCard,int numCoin) throws RemoteException{
		super(player);
		this.politicCard=politicCard;
		this.numCoin=numCoin;
	}
	
	
	@Override
	public void sellingProduct(Player player) {
		  player.removePlayerCoin(numCoin); 
		  super.getPlayer().removePlayerPoliticCard(politicCard);
		  player.addPlayerPoliticCard(politicCard);
		  super.getPlayer().addPlayerCoin(numCoin);		
	}


	@Override
	public String getName() {
		return "POLITIC CARD";
	}


	@Override
	public int getCoin() {
		return this.numCoin;
	}


	@Override
	public int getQuantity() {
		return politicCard.size();
	}
	
	

}
