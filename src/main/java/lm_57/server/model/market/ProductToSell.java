package lm_57.server.model.market;


import lm_57.server.model.Player;

public abstract class ProductToSell {
	
	private Player player;
    
    public ProductToSell(Player player){
    	this.player=player;
    }
    
	
    public abstract void sellingProduct(Player player);
    
    public Player getPlayer(){
    	return this.player;
    }
    
    public abstract String getName();
    
    public abstract int getCoin();
    
    public abstract int getQuantity();
    
	

}
