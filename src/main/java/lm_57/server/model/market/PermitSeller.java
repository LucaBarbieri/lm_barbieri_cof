package lm_57.server.model.market;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import lm_57.server.model.Player;
import lm_57.server.model.utility.PermitCard;

public class PermitSeller extends ProductToSell{
	
	private List<PermitCard> permitCard=new ArrayList<>();
	private int numCoin;
	
	public PermitSeller(Player player,List<PermitCard> permitCard,int numCoin) throws RemoteException{
		super(player);
		this.permitCard=permitCard;
		this.numCoin=numCoin;
	}

	@Override
	public void sellingProduct(Player player) {
		 player.removePlayerCoin(numCoin);
		 super.getPlayer().removePlayerPermitCard(permitCard);
		 player.addPlayerPermitCard(permitCard);
		 super.getPlayer().addPlayerCoin(numCoin);		
	}
	
	@Override
	public String getName() {
		return "PERMIT CARD";
	}


	@Override
	public int getCoin() {
		return this.numCoin;
	}


	@Override
	public int getQuantity() {
		return this.permitCard.size();
	}
}
