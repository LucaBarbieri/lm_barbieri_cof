package lm_57.server.model.bonus;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import lm_57.server.model.Constants;

public class Bonus {

	private HashSet<String> namesBonus=new HashSet<>();
	private int[] quantityOfBonus;
	
	public Bonus(int numBonus){
		boolean eq=true;
            for(int i=0;i<numBonus;i++){
            	do{
            	eq=this.namesBonus.add(generateStringBonus());
            	}while(eq==false);
            } 
            this.quantityOfBonus=generateQuantityBonus(numBonus);
	}
    
    public String generateStringBonus(){
    	
          int i=(int) (Constants.getNameOfBonus().length*Math.random());
          return Constants.getNameOfBonus()[i];
    	
    }
    public int[] generateQuantityBonus(int numBonus){
    	int[] k=new int[numBonus];
    	int i;
    	    for(int l=0;l<k.length;l++){
    	      i=(int) (1+4*Math.random());
    	      k[l]=i;
    	    }
    	 return k;
    }
    
    public Set<String> getBonusString(){
    	return  this.namesBonus;
    }
    public int[] getBonusQuantity(){
    	return this.quantityOfBonus;
    }
    
    public String toStringNameBonus(){
    	String tmp=" ";
    	   for(String bonus:this.namesBonus){
    		   tmp=tmp+bonus+" ";
    	   }
    	   return tmp;
    }
    
    @Override
	public String toString() {
		return "Bonus [" + toStringNameBonus() + "]\n" +"[" +Arrays.toString(quantityOfBonus) + "\n";
	}
    
    
    

}
