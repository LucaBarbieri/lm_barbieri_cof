package lm_57.server.model.bonus;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import lm_57.server.model.Player;

public class BonusColor {
	
	private HashMap<Color,Integer> valuesOfColor=new HashMap<>();
	
	public BonusColor(Map<Color,Integer> values){
		valuesOfColor.putAll(values);
	}
	
	public boolean bonusIsEmpty(){
		if(valuesOfColor.isEmpty()){
			return true;
		}
		return false;
	}
	
	public void getBonus(Player player,Color color){
		if(!bonusIsEmpty()){
		player.addPlayerVictory(valuesOfColor.get(color)); 
		valuesOfColor.remove(color);
		}
	}
	
	
}
