package lm_57.server.model.bonus;
import java.util.HashMap;
import java.util.Map;

import lm_57.server.model.Player;
import lm_57.server.model.map.Region;



public class BonusRegion {

	private HashMap <Region, Integer> bonusOfRegion=new HashMap<>();
	
	public BonusRegion(Map<Region,Integer> bonusOfRegion){
		this.bonusOfRegion.putAll(bonusOfRegion);
		
	}
	
	public boolean bonusIsEmpty(){
		if(bonusOfRegion.isEmpty()){
			return true;
		}
		return false;
	}
	
	public void getBonus(Player player, Region regionBonus){
		if(!bonusIsEmpty()){
			player.addPlayerVictory(bonusOfRegion.get(regionBonus));
			bonusOfRegion.remove(regionBonus);
		}
	}
	
	
}
