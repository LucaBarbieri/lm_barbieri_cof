package lm_57.server.model.bonus;


import java.util.LinkedList;
import java.util.Queue;

import lm_57.server.model.Player;



public class BonusKing {
	
	private Queue<Integer> bonus=new LinkedList<>();
	
	public BonusKing(Queue<Integer> bonus){
		this.bonus.addAll(bonus);
	}
	
	public Queue<Integer> getListBonusKing(){
		return bonus;
	}
	
	public boolean bonusIsEmpty(){
		if(bonus.isEmpty()){
			return true;
		}
		return false;
	}
	

	public void getBonusKing(Player player){
		if(!bonusIsEmpty()){
			player.addPlayerVictory(bonus.poll()); 
		}
	}
	
	
}
