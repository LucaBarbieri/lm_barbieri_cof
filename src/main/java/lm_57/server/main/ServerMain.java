package lm_57.server.main;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.server.controller.RMI.ServerRMI;
import lm_57.server.controller.connection.ServerStartGame;
import lm_57.server.controller.socket.SocketServer;

public final class ServerMain{
	 
	private ServerMain(){
		
	}
	
	public static void main(String [] args){
		ServerStartGame commonServer=new ServerStartGame();
		
		ServerRMI rmi;
		rmi = new ServerRMI(commonServer);
		rmi.startRMI();
		
		SocketServer socket=new SocketServer(commonServer);
		
	}
}
