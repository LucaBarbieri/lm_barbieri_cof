package lm_57.client.controller.RMI;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;


import lm_57.client.view.CLI.Display;
import lm_57.server.controller.RMI.IServerRMIcontroller;

public class ClientRMI  extends UnicastRemoteObject implements IClientRMI,Serializable{
	
	private static final long serialVersionUID = -5392959427741851394L;
	public static final int MANAGER_RMI_PORT = 4663;
	private static final String NAME_MANAGER= "MatchManager";
	private static final String HOST="127.0.0.1";
	
	private int numPlayer;
	private int numBonus;
	private int numMap;
	private String color;
	private String username;
	private Set<Integer> positionBonus=new HashSet<>();

	private IServerRMIcontroller controller;
	private IGameController gameManager;
	
	public ClientRMI(GameController manager) throws RemoteException{
		gameManager=manager;

	}
	
	/**
	 * add the server manager of the game
	 */
	
	@Override
	public void notifyGameStart(String gameName, String color, int numPlayer,String players) throws RemoteException {
		
		Display.printMessage("The game "+gameName+" is started!");
		Display.printMessage("YOUR COLOR IS: "+ color);
		Display.printMessage("THE PLAYER ARE  "+numPlayer+" AND THEIR COLORS: ["+players+"]");
		Registry reg= LocateRegistry.getRegistry(HOST);
		try {
			controller=(IServerRMIcontroller) reg.lookup(NAME_MANAGER);
	
		} catch (NotBoundException e) {
			Logger.getGlobal().log(Level.SEVERE, "error registry", e);
		}		
	}
	
	/**
	 * the first player must to choose the num of bonus for each city and permit card, the position of bonus in nobility track,
	 * the number of players in the game and the number of map in the configuration file
	 * @throws IOException 
	 * 
	 */
	
	@Override
	public void initializeGame() throws IOException{
		
		numPlayer=gameManager.chooseNumPlayer();
		numBonus=gameManager.chooseNumBonus();
		numMap=gameManager.chooseNumMap();
		positionBonus.addAll(gameManager.choosePositionBonus());
		Display.printMessage("wait other players!");
	}
	
	@Override
	public Set<Integer> getPositionBonus(){
		return positionBonus;
	}
	
	@Override
	public int getNumMap(){
		return numMap;
	}
	
	@Override
	public int getNumBonus(){
		return numBonus;
	}

	@Override
	public void notifyTurn(String name, String color) throws RemoteException {
		Display.printMessage("Current turn: "+ "["+ color+"]");
		Display.printMessage("Current turn: "+ "["+ name+"]");
	}

	@Override
	public void notifyUpdate(String message, int num) throws RemoteException {
		Display.printMessage(message +" ["+num+"]");
	}
	
	@Override
	public void notifyDisconnection() throws RemoteException {
		JOptionPane.showMessageDialog(null, "disconnetted");
		
	}
	/**
	 * used to set the username of the client
	 * @param name
	 */
	public void setName(String name){
		username=name;
	}
	
	/**
	 * used to set the color of the client
	 * @param color
	 */
	public void setColor(String color){
		this.color=color;
	}

	@Override
	public String getColor() throws RemoteException {
		return color;
	}
	@Override
	public String getName() throws RemoteException {
		return username;
	}


	@Override
	public void notifyMessage(String message) throws RemoteException {
		Display.printMessage(message);
		
	}

	@Override
	public int getNumPlayer() throws RemoteException {
		return numPlayer;
	}

	@Override
	public void notifyGameEnd(String winner) throws RemoteException {
		JOptionPane.showMessageDialog(null, "THE WINNER IS: "+ winner);
	}
	
	@Override
	public void playTurnRound() throws RemoteException {
		controller.displayPlayerSituation(this);
		controller.displayPlayerPoliticCard(this);
		
		chooseFastAction();
		chooseMainAction();
		passTurn();
	}
	
	private void passTurn() throws RemoteException {
		controller.passTurn();
		
	}

	@Override 
	public void chooseMainAction() throws RemoteException{
		controller.startTimer(this);
		String command = gameManager.chooseMainAction();
		controller.runMainAction(command);
	}
	
	@Override 
	public void chooseFastAction() throws RemoteException{
		controller.startTimer(this);
		String command = gameManager.chooseFastAction();
		if(command.equals("NoAction")){
			controller.NoFastAction();
		}
		controller.runFastAction(command);
	}
	
	@Override
	public String chooseCouncilBalcon() throws RemoteException{
		controller.showCouncilBalcon(this);
		controller.displayPlayerPoliticCard(this);
		controller.displayPlayerPoliticCard(this);
		String council=gameManager.chooseCouncil();
		return council;
	}
	
	@Override
	public String chooseColorCouncillor() throws RemoteException{
		controller.displayPlayerPoliticCard(this);
		return gameManager.chooseCouncillorColor();
	}

	
	@Override
	public String setRegion(List<String> regions) throws RemoteException {
		controller.showVisiblePermitCardRegion(this);
		controller.displayPlayerPoliticCard(this);
		controller.showCouncilBalcon(this);
		String region=gameManager.chooseRegion(regions);
		return region;
	}


	@Override
	public void playTurnBuy() throws RemoteException {
		controller.displayProductOnSale(this);
		String cmd=gameManager.chooseBuy();
		
		switch(cmd){
		case "NO":
			controller.passTurnMarketBuy(this);
			break;
		case "YES":
			runBuy();
			break;
		}
	}

	private void runBuy() throws RemoteException{
		controller.displayProductOnSale(this);
		int index=gameManager.chooseIndex(controller.getDimensionSellList(this));
		controller.runBuy(index);
	}
	@Override
	public void playTurnSell() throws RemoteException {
		controller.displayPlayerSituation(this);
		controller.displayPlayerPoliticCard(this);
		String object =gameManager.chooseWhatSell();
		if(object.equals("NoAction")){
			Display.printMessage("PASS TURN");
			controller.passTurnMarket(this);
			return;
		}
		controller.runSell(object);
	}

	@Override
	public int chooseIndexPermitCard(Set<String> cardList, int dimension) {
		Display.printMessage("List: "+ cardList);
		
		int index= gameManager.chooseIndexPermitCard(dimension);
		return index;
	}

	@Override
	public String chooseKingPath(List<String> linkedCity ) throws RemoteException {
		controller.displayMap(this);
		Display.printMessage("City linked: ["+ linkedCity +"]");
		
		return gameManager.chooseCity(linkedCity);
	}
	
	@Override
	public int chooseNumOfCities() throws RemoteException{
		controller.displayKingCity(this);
		controller.displayMap(this);
		return gameManager.chooseNumCity();
	}
	@Override
	public String chooseCity(Set<String> cities) throws RemoteException {
		Display.printMessage("The list of city avaible are: " + cities);
		String city=gameManager.chooseCity(cities);
		return city;
	}

	@Override
	public int chooseCoin() throws RemoteException {
		int coin= gameManager.chooseCoin();
		return coin;
	}

	@Override
	public int chooseQuantity(int max) throws RemoteException {
		int value=gameManager.chooseQuantity(max);
		return value;
	}
	
	@Override
	public List<Integer> chooseIndex(int max) throws RemoteException {
		List<Integer> index=new ArrayList<>();
		int numCard=chooseQuantity(max);
		for(int i=0;i<numCard; i++){
			int num=gameManager.chooseIndex(max);
			index.add(num);
		}
		return index;
	}


	

}
