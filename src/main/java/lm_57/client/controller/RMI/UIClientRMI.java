package lm_57.client.controller.RMI;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

import lm_57.client.view.CLI.Display;
import lm_57.client.view.CLI.ReadInput;
import lm_57.server.controller.RMI.IServerRMI;

public class UIClientRMI {
	
	private static final String NAME_SERVER= "CouncilOfFour_Server";
	public static final int MANAGER_RMI_PORT = 4663;
	private static final String HOST="127.0.0.1";
	ClientRMI clientStub;
	IServerRMI serverStub;
	String username;
	String color;
	List<String> colors=new ArrayList<>();
	
	public UIClientRMI(GameController manager) throws RemoteException{
		clientStub=new ClientRMI(manager);
		colors.add("Black");
		colors.add("Orange");
		colors.add("Blue");
		colors.add("Magenta");
		colors.add("White");
		colors.add("Pink");
		colors.add("Cyan");
		colors.add("Red");
		colors.add("Green");
		colors.add("Yellow");
		colors.add("Gray");
	}
	
	/**
	 * try to connect to the server and register to it
	 * 
	 * @throws NotBoundException
	 * @throws IOException 
	 */
	public void connect() throws NotBoundException, IOException{
		
		login();
		
		Display.printMessage("connection......");
		
		Display.printMessage("look up registry......");
		Registry reg= LocateRegistry.getRegistry(HOST);
		serverStub= (IServerRMI) reg.lookup(NAME_SERVER);
		
		serverStub.register(clientStub);		
		
	}
	
	private void login() throws IOException{
		Display.printMessage("please enter your username: \n");
		username=ReadInput.readLine();
		clientStub.setName(username);
		Display.printMessage("please enter your color: \n");
			color=ReadInput.readLine();
		while(!colors.contains(color)){
			Display.printMessage("Your choice is not right please insert again!");
			Display.printMessage("the color avaible are: ["+colors.toString()+"]");
			color=ReadInput.readLine();
		}
	clientStub.setColor(color);
	}
	
	public ClientRMI getClient(){
		return clientStub;
	}
}
