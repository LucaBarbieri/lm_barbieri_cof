package lm_57.client.controller.RMI;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.client.view.CLI.Display;
import lm_57.client.view.CLI.ReadInput;
import lm_57.server.model.GameBoard;

public class GameController implements IGameController{
	
	public boolean endGame;
	protected GameBoard game; 
	private List<String> fastAction= new ArrayList<>();
	private List<String> mainAction=new ArrayList<>();
	private List<String> colorCouncillor=new ArrayList<>();
	private List<String> council=new ArrayList<>();
	private List<String> sell=new ArrayList<>();
	/**
	 * create the list of main and fast action avaible, and color avaible for the choice of councillor 
	 * 
	 */
	public GameController(){
		fastAction.add("BuyAssistant");
		fastAction.add("PayAssistantForChangePermitCard");
		fastAction.add("ElectedCouncilor");
		fastAction.add("PayAssistantForMainAction");
		fastAction.add("NoAction");
		mainAction.add("SetNewCouncillorOnBalconRegion");
		mainAction.add("BuildEmporiumByPermitCard");
		mainAction.add("BuyPermitCard");
		mainAction.add("BuildOnKingCity");
		colorCouncillor.add("Black");
		colorCouncillor.add("Orange");
		colorCouncillor.add("Pink");
		colorCouncillor.add("Blue");
		colorCouncillor.add("Magenta");
		colorCouncillor.add("White");
		council.add("King");
		council.add("Mountain");
		council.add("Hills");
		council.add("Coast");
		sell.add("Assistant");
		sell.add("PoliticCard");
		sell.add("PermitCard");
		sell.add("NoAction");
	}
	
	@Override 
	public String chooseRegion(List<String> regionList){
		Display.printMessage("Choose the Region where you want to change visible permit card");
		String region=ReadInput.readLine();
		while(!regionList.contains(region)){
			Display.printMessage("Error in immision of the name, please choose : [Mountain] or [Hills] or [Coast]");
			region=ReadInput.readLine();
		}
		return region;
	}
	
	@Override
	public String chooseCouncil(){
		Display.printMessage("Choose the name of the place of council (region or King");
		String council=ReadInput.readLine();
		while (!this.council.contains(council)){
			Display.printMessage("error in the input please insert the right name: [King],[Coast],[Mountain] or"
					+ "[Hills]");
			council=ReadInput.readLine();
		}
		return council;
	}
	
	@Override
	public String chooseCity(Set<String> cityAllowed){
		Display.printMessage("select the city: ");
		String city=ReadInput.readLine();
		while(!cityAllowed.contains(city)){
			Display.printMessage("Choice not right, please enter again the cities allowed are: ["+cityAllowed+" ]");
			city=ReadInput.readLine();
		}
		return city;
	}
	@Override
	public int chooseNumCity(){
		Display.printMessage("How much city do you want to pass: ");
		int num=ReadInput.readInt();
		while((num<0) && (num>15)){
			Display.printMessage("CHOICE INCORRECT THE MAX IS 14");
			num=ReadInput.readInt();
		}
		return num;
	}
	
	
	@Override
	public int chooseIndexPermitCard(int size){
		Display.printMessage("select the index starting to 0 of the permitCard do you want to use");
		int num=ReadInput.readInt();
		while(num<0 || num>=size){
			Display.printMessage("the index must be between 0 and "+ (size-1));
			num=ReadInput.readInt();
		}
		return num;
	}
	
	@Override
	public String chooseCity(List<String> cities){
		Display.printMessage("enter the city: ");
		String city=ReadInput.readLine();
		while(!cities.contains(city)){
			Display.printMessage("CHOICE NOT RIGHT!The allowed cities are: [ "+ cities+ " ]");
			city=ReadInput.readLine();
		}
		return city;
	}
	
	@Override
	public String chooseCouncillorColor(){
		Display.printMessage("The avaible councillor color are: ");
		for(String color: colorCouncillor){
			Display.printMessage(color);
		}
		Display.printMessage("Choose a color please");
		String myColor=ReadInput.readLine();
		while(!colorCouncillor.contains(myColor)){
			Display.printMessage("error in writing the color please repeat");
			myColor=ReadInput.readLine();
		}
		return myColor;
	}

	/**
	 * not used by this class, it is implemented by the subclass RMITextController
	 */
	@Override
	public void startController() {
		
	}

	@Override
	public String chooseFastAction() {
		Display.printMessage("The avaible fast action are: ");
		Display.printMessage(fastAction.toString());
		Display.printMessage("Choose the fastAction like it was printed");
		String cmdfastAction=ReadInput.readLine();
		while(!fastAction.contains(cmdfastAction)){
			Display.printMessage("the choice was not right please insert the command again");
			cmdfastAction=ReadInput.readLine();
		}
		return cmdfastAction;
	}
	
	@Override
	public String chooseMainAction() {
		Display.printMessage("The avaible main action are: ");
		Display.printMessage(mainAction.toString());
		Display.printMessage("Choose the mainAction like it was printed");
		String cmdmainAction=ReadInput.readLine();
		while(!mainAction.contains(cmdmainAction)){
			Display.printMessage("the choice was not right please insert the command again");
			cmdmainAction=ReadInput.readLine();
		}
		return cmdmainAction;
	}
	
	
	
	/**
	 * this part is about the initialization of the first player
	 */
	@Override
	public int chooseNumPlayer() {
		Display.printMessage("enter number of player(the maximum is 10): \n");
		int numPlayer=ReadInput.readInt();
		
		while(!checkNumPlayer(numPlayer)){
			numPlayer=ReadInput.readInt();
		}
		return numPlayer;
	}
	
	/**
	 * control if the input is correct
	 * @param numMap
	 * @return
	 */
	private boolean checkNumPlayer(int number){
		if(number>=2 && number<=10){
			return true;
		}else{
			Logger.getGlobal().log(Level.WARNING, "number of player must be between 2 and 10!");
			return false;
		}
	}
	@Override
	public int chooseNumBonus() {
		Display.printMessage("enter number of bonus that you want in the game(the maximum is 3): \n");
		int numBonus=ReadInput.readInt();
		while(!checkNumBonus(numBonus)){
			numBonus=ReadInput.readInt();
		}
		return numBonus;
	}
	
	/**
	 * control if the input is correct
	 * @param numMap
	 * @return
	 */
	private boolean checkNumBonus(int number){
		if(number<=3){
			return true;
		}
		else{
			Logger.getGlobal().log(Level.WARNING, "number of bonus must be less then 3!");
			return false;
		}
	}
	@Override
	public int chooseNumMap() {
		Display.printMessage("enter number of map(between 1 and 8): \n");
		int numMap=ReadInput.readInt();
		while(!checkNumMap(numMap)){
			numMap=ReadInput.readInt();
		}
		return numMap;
	}
	
	/**
	 * control if the input is correct
	 * @param numMap
	 * @return
	 */
	private boolean checkNumMap(int numMap){
		if(numMap>=1 && numMap<=8){
			return true;
		}else{
			Logger.getGlobal().log(Level.WARNING, "number of map from file must be between 1 and 8!");
			return false;
		}
	}
	@Override
	public Set<Integer> choosePositionBonus() {

		Display.printMessage("choose how much bonus do you want ,from 0 to 20, for the nobility track: \n");
		int numPosition;
		Set<Integer> set=new HashSet<>();
		int numNobility= ReadInput.readInt();
		while(!checkPositionBonus(numNobility)){
			numNobility= ReadInput.readInt();
		}
		
		Display.printMessage("choose now the position of nobility track bonus between 0 and 20: \n");
		
		for(int i=0;i<numNobility;i++){
			Display.printMessage("bonus: "+ i +"\n");
			numPosition=ReadInput.readInt();
			while(!checkPositionBonus(numPosition)){
				numPosition=ReadInput.readInt();
			}
			set.add(numPosition);
		}
		return set;
	}
	/**
	 * control if the input is correct
	 * @param quantity the number of bonus
	 * @return
	 */
	
	private boolean checkPositionBonus(int quantity){
		if(quantity>=0 && quantity<=20){
			return true;
		}
		else{
			Logger.getGlobal().log(Level.WARNING, "positions of bonus must be a number between 0 and 20!");
			return false;
		}
	}

	@Override
	public String chooseWhatSell() {
		Display.printMessage("What Do you want to sell? ");
		Display.printMessage(sell.toString());
		
		String sell=ReadInput.readLine();
		while(!this.sell.contains(sell)){
			Display.printMessage("choice not right, insert again ");
			sell=ReadInput.readLine();
		}
		return sell;
	}

	@Override
	public int chooseQuantity(int avaible){
		Display.printMessage("what is the quantity that you want to sell? ");
		Display.printMessage("the max is: "+ avaible);
		int num=ReadInput.readInt();
		if(num<1 || num>avaible){
			Display.printMessage("choice not right, insert again ");
			num=ReadInput.readInt();
		}
		return num;
	}
	
	@Override 
	public int chooseIndex(int maxDimension){
		Display.printMessage("what is the index of card? start from 0 to "+(maxDimension-1));
		int num=ReadInput.readInt();
		if(num<0 || num>(maxDimension-1)){
			Display.printMessage("choice not right, insert again ");
			num=ReadInput.readInt();
		}
		return num;
	}

	@Override
	public int chooseCoin() {
		Display.printMessage("what is the price?" );
		int coin=ReadInput.readInt();
		if(coin<1 || coin >15){
			Display.printMessage("choice not right, the max is 15 ");
			coin=ReadInput.readInt();
		}
		return coin;
	}
	
	@Override
	public String chooseBuy(){
		List<String> list=new ArrayList<>();
		list.add("YES");
		list.add("NO");
		Display.printMessage("DO YOU WANT TO BUY SOMETHING? YES/NO");
		String cmd=ReadInput.readLine();
		while(!list.contains(cmd)){
			Display.printMessage("insert YES or NO");
			cmd=ReadInput.readLine();
		}
		return cmd;
	}
}
