package lm_57.client.controller.RMI;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;


public interface IClientRMI extends Remote {
	
	/**
	 * called by the server the number of time that decided the client to insert the path
	 * @param linked city 
	 * @return the linked city where the client want to move
	 * @throws RemoteException
	 */
	public String chooseKingPath(List<String> cities)throws RemoteException;

	/**
	 * called by the server to make the client choose a region 
	 * @param regions
	 * @return
	 * @throws RemoteException
	 */
	public String setRegion(List<String> regions) throws RemoteException;
	
	public int getNumPlayer() throws RemoteException;
	
	public String getColor() throws RemoteException;
	
	public int getNumBonus()	throws RemoteException;
	
	public Set<Integer> getPositionBonus() throws RemoteException;
	
	public int getNumMap() throws RemoteException;
	
	public String getName() throws RemoteException;
	/**
	 * called by the server to make the client initialize game
	 * 
	 * @throws RemoteException
	 */
	
	public void initializeGame() throws RemoteException, IOException;
	/**
	 * used by server to notify to the client that game started
	 * 
	 * @param gameName is the name of the match in the registry to look up
	 * @param color
	 * @param numPlayer
	 */
	
	public void notifyGameStart(String gameName,String color, int numPlayer,String players) throws RemoteException;
	
	/**
	 * the server notify that state of turns changed to all clients in the match
	 *
	 */
	
	public void notifyTurn(String name, String color) throws RemoteException;
	
	/**
	 * used by the server to notify the client that his situation is changed
	 * it is used when coin,victory or nobility changed
	 * 
	 * @throws RemoteException
	 */
	public void notifyUpdate(String message, int num)throws RemoteException;
	
	/**
	 * used by the server to notify the client that the game is finished
	 * 
	 * @throws RemoteException
	 */
	public void notifyGameEnd(String winner) throws RemoteException;
	
	/**
	 * used by the server to notify the client the disconnection
	 * 
	 * @throws RemoteException
	 */
	public void notifyDisconnection() throws RemoteException;
	
	/**
	 * used by the server to write some message to client
	 */
	
	public void notifyMessage(String message) throws RemoteException;
	
	/**
	 * this is called by the server to make client play the turn 
	 * of the round state
	 */
	
	public void playTurnRound()throws RemoteException;
	
	/**
	 * called by the server to make the client choose the place of a council
	 * @return the name of the council
	 * @throws RemoteException
	 */
	String chooseCouncilBalcon()throws RemoteException;

	/**
	 * called by the server to make the client choose the color of a councillor between the avaible colors
	 * @return the color
	 * @throws RemoteException
	 */
	String chooseColorCouncillor()throws RemoteException;
	
	/**
	 * make the client choose a fast action 
	 * @throws RemoteException
	 */
	void chooseFastAction() throws RemoteException;
	
	/**
	 * make the client choose a fast action 
	 * @throws RemoteException
	 */
	void chooseMainAction() throws RemoteException;
	
	/**
	 * called by the server to make the client play the turn of buy market
	 * @throws RemoteException
	 */
	public void playTurnBuy()throws RemoteException;
	
	/**
	 * called by the server to make the client play the turn of sell market
	 * @throws RemoteException
	 */
	public void playTurnSell()throws RemoteException;
	
	/**
	 * called by the server to make the client choose a permit card by index between the avaible ones
	 * @param set avaible
	 * @return index of card
	 * @throws RemoteException
	 */
	public int chooseIndexPermitCard(Set<String> set, int dimension)throws RemoteException;
	
	/**
	 * called by the server to make the client choose the city between the avaible ones 
	 * @param cityAllowed
	 * @return the name of the city
	 * @throws RemoteException
	 */
	public String chooseCity(Set<String> cityAllowed)throws RemoteException;
	
	/**
	 * choose the price of an object that want to sell
	 * @return 
	 * @throws RemoteException
	 */
	public int chooseCoin() throws RemoteException;
	
	/**
	 * choose the quantity of objects that want to sell
	 * @return
	 * @throws RemoteException
	 */
	public int chooseQuantity(int max) throws RemoteException;
	
	/**
	 * makes the player choose how much card want to sell and the index of the card 
	 * @param max
	 * @return
	 * @throws RemoteException
	 */
	public List<Integer> chooseIndex(int max) throws RemoteException;
	
	/**
	 * called by the server to make the client choose the num of cities in the Build With help of King
	 * @return
	 * @throws RemoteException
	 */
	int chooseNumOfCities()throws RemoteException;
	

}
