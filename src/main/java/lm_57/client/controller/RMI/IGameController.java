package lm_57.client.controller.RMI;

import java.util.List;
import java.util.Set;

public interface IGameController {

	/**
	 * this control the choice of a fast action by the client 
	 * and return the correct command string 
	 * @return fast action
	 */
	public String chooseFastAction();
	
	/**
	 * this control the choice of a main action by the client 
	 * and return the correct command string 
	 * @return
	 */
	public String chooseMainAction();
	
	/**
	 * this method is used by the control RMI view to connect the client to the server and set the 
	 * controller of the game for the client side
	 */
	public void startController();

	/**
	 * this is used by the client to choose the council of a region or the king
	 * @return the choice
	 */
	public String chooseCouncil();
	
	/**
	 * this is used by the client to choose the region for the actions
	 * @return the region string
	 */
	public String chooseRegion(List<String> regions);
	
	/**
	 * this is used by the client to choose the color of the councillor 
	 * @return the color string
	 */
	public String chooseCouncillorColor();

	
	/**
	 * used by the client to choose the initial number of player
	 * @return the choice
	 */
	public int chooseNumPlayer();
	
	/**
	 * used by the client to choose the initial number of bonus for each city or permit card
	 * @return the choice
	 */
	public int chooseNumBonus();
	
	/**
	 * used by the client to choose the initial position of bonus 
	 * @return the choice
	 */
	public int chooseNumMap();
	
	/**
	 * used by the client to choose the initial position of bonus 
	 * @return
	 */
	public Set<Integer> choosePositionBonus();
	
	
	/**
	 * the client choose the index of permit card
	 * that want to use or to take
	 * @return
	 */
	public int chooseIndexPermitCard(int size);
	
	/**
	 * used by the client to choose the city where he want to build an emporium
	 * @param cities list of avaible city
	 * @return the city 
	 */
	public String chooseCity(List<String> cities);
	
	/**
	 * the client choose the path of cities that want to pass for building with the hel of the king
	 * @return
	 */
	public int chooseNumCity();
	
	/**
	 * choose a city from a list
	 * @param cityAllowed
	 * @return
	 */
	public String chooseCity(Set<String> cityAllowed);
	
	/**
	 * the player choose what sell 
	 * @return
	 */
	public String chooseWhatSell();
	/**
	 * the player choice how much want to sell of the object for the MARKET
	 * @param avaible
	 * @return
	 */
	public int chooseQuantity(int avaible);
	
	/**
	 * if the player want to sell some cards, he choose the index of the owned card
	 * @param maxDimension
	 * @return
	 */
	int chooseIndex(int maxDimension);
	/**
	 * the player choose the price of an object
	 * @return
	 */
	public int chooseCoin();

	/**
	 * the client choose YES or NO to buy somthing
	 * @return
	 */
	String chooseBuy();

}
