package lm_57.client.controller.CLI;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.client.controller.RMI.UIClientRMI;
import lm_57.client.controller.RMI.GameController;

public class RMItextController extends GameController{
	
	private UIClientRMI client;
	
	public RMItextController() throws RemoteException{
		client=new UIClientRMI(this);
		game=null;
		
	}
	
	@Override
	public void startController(){
		try {
			client.connect();
		} catch (NotBoundException e) {
			Logger.getGlobal().log(Level.SEVERE, "error in connection", e);
		} catch(IOException e){
			Logger.getGlobal().log(Level.SEVERE, "error in the IO", e);
		}
	}
	
}
