package lm_57.client.controller.GUI.listener;

import java.awt.event.ActionEvent;
import static lm_57.server.model.Constants.RMI;
import static lm_57.server.model.Constants.SOCKET;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lm_57.client.controller.CLI.RMItextController;
import lm_57.client.controller.RMI.GameController;
import lm_57.client.view.CLI.ViewCLI;
import lm_57.client.view.startGameGUI.frame.ChooseConnection;
import lm_57.server.controller.socket.ClientSocket;

public class NetListener implements ActionListener {
	
	private String netMode;
	
	public NetListener(String netMode){
		this.netMode=netMode;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		ChooseConnection.getManager().disposeNetFrame();
		
		if(netMode.equals(RMI)){
			GameController controller;
			try {
				controller = new RMItextController();
				ViewCLI.getViewManager().setGameController(controller);
				ViewCLI.getViewManager().startView();
			} catch (RemoteException e) {
				Logger.getGlobal().log(Level.SEVERE, "error", e);
			}
			
		}
		if(netMode.equals(SOCKET)){
			   ClientSocket client=new ClientSocket();
			   client.start();			
		}
	}
	
	

}
