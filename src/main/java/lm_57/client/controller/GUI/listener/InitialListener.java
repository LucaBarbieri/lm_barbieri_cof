package lm_57.client.controller.GUI.listener;

import static lm_57.server.model.Constants.NET;
import static lm_57.server.model.Constants.LOCAL;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import lm_57.client.view.startGameGUI.frame.ChooseConnection;


public class InitialListener implements ActionListener {
	private String mode;
	
	public InitialListener(String mode){
		this.mode=mode;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		ChooseConnection.getManager().disposeInitialFrame();
		
		if(mode.equals(LOCAL)){
			
		}
		if(mode.equals(NET)){
			   ChooseConnection.getManager().netSelection();		
		}
	}

}
