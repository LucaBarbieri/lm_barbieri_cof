package lm_57.client.view.GUI;



import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class SettingsMenu {

	private JFrame frame=new JFrame("Settings Menu");
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public SettingsMenu() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 1211, 716);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1195, 677);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon("C:/Users/User/Desktop/settings_logo.jpg"));
		lblNewLabel.setBounds(0, 0, 1195, 677);
		panel.add(lblNewLabel);
		frame.setContentPane(contentPane);
		
	}
	
	public void setVisible(boolean b){
		
		frame.setVisible(b);
	}
}
