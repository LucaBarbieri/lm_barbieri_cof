package lm_57.client.view.GUI;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CouncilOfFourGUI  {

	JFrame frame=new JFrame("Council Of Four");
	Logger logger=Logger.getAnonymousLogger();

	public CouncilOfFourGUI() {
		
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 731, 780);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 715, 741);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon("C:/Users/User/Desktop/pic2648492_lg.jpg"));
		lblNewLabel.setBounds(0, 27, 715, 741);
		panel.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Play");
		btnNewButton.setBounds(261, 300, 173, 42);
		panel.add(btnNewButton);
		
		frame.getContentPane().add(panel);
		setVisible(true);
		
		btnNewButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent ae) {
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							frame.setVisible(false);
							SettingsMenu frame = new SettingsMenu();
							frame.setVisible(true);
						} catch (Exception e) {
                              logger.log(Level.SEVERE,"exception", e);
						}
					}
				});
				
			}
	
		});
		

		

		


	}

	public void setVisible(boolean b) {
		   frame.setVisible(b);
	}
}
