package lm_57.client.view.CLI;

import java.util.Observable;
import java.util.Observer;

import lm_57.client.controller.RMI.GameController;
import lm_57.server.model.GameBoard;

public class ViewCLI implements Observer{
	
	private static ViewCLI view;
	private GameController controller;
	
	public ViewCLI(){
		controller=null;
	}
	
	public static ViewCLI getViewManager(){
		if(view==null){
			view=new ViewCLI();
		}
		return view;
	}
	
	public GameController getController(){
		return controller;
	}
	
	public void setGameController(GameController gameController){
		this.controller=gameController;
	}
	
	public void startView(){
		Display.printMessage("COUNCIL OF FOUR");
		controller.startController();
	}
	
	@Override
	public void update(Observable observe, Object obj) {
		if(obj.getClass().equals(GameBoard.class)){
			Display.printState((GameBoard)obj);
		}
	}
	
	
}
