package lm_57.client.view.CLI;

import java.util.Scanner;

public class ReadInput {
	
	private static Scanner input=new Scanner(System.in);
	
	private ReadInput(){
		
	}
	
	public static int readInt(){
		return input.nextInt();
	}
	
	public static String readLine(){
		return input.nextLine();
	}
}
