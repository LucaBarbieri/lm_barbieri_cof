package lm_57.client.view.CLI;

import java.util.List;

import lm_57.server.model.Constants;
import lm_57.server.model.GameBoard;
import lm_57.server.model.Player;


public final class Display {

		private Display() {
	
		}

		/**
		 * Prints to the console the message passed
		 * 
		 * @param message
		 *            to display
		 */
		public static void printMessage(String message) {
			System.out.println(message);
		}

		/**
		 * Prints to the console the warning passed
		 * 
		 * @param message
		 *            to display
		 */
		public static void printWarning(String message) {
			System.out.println("WARNING: " + message);
		}

		/**
		 * Display all the players and their characteristics
		 * 
		 * @param players
		 *            the list of players to display
		 */
		public static void displayPlayers(List<Player> players) {
			printBlankLine();
			printMessage("Players: ");
			for (Player player : players) {
				printMessage(player.toString());
			}
		}		
			
		/**
		 * Visualize the match state
		 * 
		 * @param match
		 *            of which to display the state
		 */
		public static void printState(GameBoard game) {
			printMessage("The current state is: "+ game.displayCurrentTurn());
			changeTurn(game.getCurrentPlayerTurn());
		}

		/**
		 * shows the winner
		 * 
		 * @param matchWinners
		 *            the winner(s) to display
		 */
		public static void showWinner(Player winner) {
			printBlankLine();
			System.out.println(winner.getName() + " "+ Constants.convertColorToString(winner.getPlayerColor()));
		}
		
		/**
		 * prints when the turn is changed
		 * @param player
		 */
		public static void changeTurn(Player player) {
			printMessage("<----------  Turn of:  [" + player.toString()+"---------->");
		}

		public static void printBlankLine() {
			System.out.println("\n");
		}
		
		

}

