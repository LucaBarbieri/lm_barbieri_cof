package lm_57.client.view.startGameGUI.frame;

import java.awt.BorderLayout;

import lm_57.client.controller.RMI.ClientRMI;
import lm_57.client.view.startGameGUI.component.InitialPanel;
import lm_57.client.view.startGameGUI.component.NetPanel;




public class ChooseConnection {
	
	private static final int NET_HEIGHT =400;
	private static final int NET_WIDTH = 400;
	
	private static ChooseConnection view;
	private NetFrame netFrame;
	private InitialFrame initialFrame;


	public ChooseConnection(){
		initialFrame=new InitialFrame("choose mode: ", NET_HEIGHT,NET_WIDTH);
		netFrame=new NetFrame("choose connection: ", NET_HEIGHT,NET_WIDTH);
		
	}
	
	public static synchronized ChooseConnection getManager(){
		if(view==null){
			view=new ChooseConnection();
		}
		return view;
	}

	public void initialSelection(){
		initialFrame.setLayout(new BorderLayout());
		InitialPanel panel= new InitialPanel();
		initialFrame.add(panel,BorderLayout.CENTER);
		initialFrame.pack();
		initialFrame.setLocationRelativeTo(null);
		initialFrame.setVisible(true);
	}
	public void netSelection(){
		netFrame.setLayout(new BorderLayout());
		NetPanel netPanel= new NetPanel();
		netFrame.add(netPanel,BorderLayout.CENTER);
		netFrame.pack();
		netFrame.setLocationRelativeTo(null);
		netFrame.setVisible(true);
		
	}

	public void disposeNetFrame(){
		netFrame.dispose();
	}

	public void disposeInitialFrame(){
		initialFrame.dispose();
	}

	
}
