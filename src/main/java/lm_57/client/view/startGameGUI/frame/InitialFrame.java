package lm_57.client.view.startGameGUI.frame;

import java.awt.Dimension;

import javax.swing.JFrame;

public class InitialFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6824003696852479282L;

	public InitialFrame(String title, int height, int width){
		super(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(width,height));
		this.setResizable(false);
	}
}
