package lm_57.client.view.startGameGUI.component;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lm_57.client.controller.GUI.listener.NetListener;

import static lm_57.server.model.Constants.RMI;
import static lm_57.server.model.Constants.SOCKET;

import java.awt.FlowLayout;
import java.awt.GridLayout;

public class NetPanel extends JPanel{
	
	private static final long serialVersionUID = 8687888741373574899L;
	private JButton rmi=new JButton(RMI);
	private JButton socket=new JButton(SOCKET);
	
	public NetPanel(){
		super();
		setLayout(new GridLayout(2,1));
		JLabel label=new JLabel("what connection do you want to use? ");
		label.setVerticalAlignment(JLabel.CENTER);
		label.setHorizontalAlignment(JLabel.CENTER);
		add(label);
		JPanel buttonPanel=new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		rmi.addActionListener(new NetListener(RMI));
		buttonPanel.add(rmi);
		socket.addActionListener(new NetListener(SOCKET));
		buttonPanel.add(socket);
		add(buttonPanel);
	}
}
