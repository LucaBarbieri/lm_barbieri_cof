package lm_57.client.view.startGameGUI.component;

import static lm_57.server.model.Constants.NET;
import static lm_57.server.model.Constants.LOCAL;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import lm_57.client.controller.GUI.listener.InitialListener;


public class InitialPanel extends JPanel {

	private static final long serialVersionUID = 8687888741373574899L;
	private JButton net=new JButton(NET);
	private JButton local=new JButton(LOCAL);
	
	public InitialPanel(){
		super();
		setLayout(new GridLayout(2,1));
		JLabel label=new JLabel("HOW DO YOU WANT TO PLAY? ");
		label.setVerticalAlignment(JLabel.CENTER);
		label.setHorizontalAlignment(JLabel.CENTER);
		add(label);
		JPanel buttonPanel=new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		net.addActionListener(new InitialListener(NET));
		buttonPanel.add(net);
		local.addActionListener(new InitialListener(LOCAL));
		buttonPanel.add(local);
		add(buttonPanel);
	}
}
