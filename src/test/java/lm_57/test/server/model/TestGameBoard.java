package lm_57.test.server.model;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.*;

import lm_57.server.controller.User;
import lm_57.server.controller.state.StateGameContext;
import lm_57.server.model.GameBoard;
import lm_57.server.model.Player;


public class TestGameBoard {
	
	List<User> users=new ArrayList<>();
	Set <Integer> positionBonus=new HashSet<>();
	List<Player> players=new ArrayList<>();
	int numMap;
	int numBonus;
	int numPlayer;
	GameBoard game;
	
	@Before
	public void setUp(){
		numMap=1;
		numBonus=1;
		numPlayer=1;
		users.add(new User("player1", "Yellow"));
		users.add(new User ("player2", "Black"));
		positionBonus.add(1);
		positionBonus.add(2);
	}
	
	@Test
	public void gameTest() throws IOException{
		//create the game and set the initial values
		
		game=new GameBoard("Council of Four_ MATCH_1");
		game.setNumMap(numMap);
		game.setPositionBonus(positionBonus);
		game.setNumBonus(numBonus);
	
		
		assertEquals(game.getPositionBonus(),positionBonus);
		assertEquals(game.getGameName(),"Council of Four_ MATCH_1");
		
		//add the players of the game
		game.addPlayer(numPlayer, users);
		
		assertEquals(game.getPlayers().peek().getName(), "player1");
		assertEquals(game.getPlayers().peek().getPlayerColor(), Color.YELLOW);
		
		players.addAll(game.getPlayers());
		
		//set the initial turn and start the game
		game.start();
		
		assertEquals(game.getCurrentPlayerTurn(), game.getPlayers().peek());
		assertEquals(game.getTurnColor(), "Yellow");
		assertEquals(game.getCurrentPlayerToString(),"player1");
		
		//to string turn
		assertEquals(game.displayCurrentTurn(),"[ state: Round ]" + "[current Turn: "+ "player1"+"]");
		
		System.out.println(game.displayCurrentTurn()+"\n"+game.getTurnColor());
		
		//set the next turn 
		
		game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
		game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
		game.setNextTurn();
		
		assertNotEquals(game.getCurrentPlayerTurn(), game.getPlayers().peek());
		
		
	}
}
