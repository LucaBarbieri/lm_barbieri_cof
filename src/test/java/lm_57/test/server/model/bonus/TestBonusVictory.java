package lm_57.test.server.model.bonus;

import lm_57.server.model.Player;
import lm_57.server.model.bonus.BonusColor;
import lm_57.server.model.bonus.BonusKing;
import lm_57.server.model.bonus.BonusRegion;
import lm_57.server.model.map.Region;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.junit.*;


public class TestBonusVictory {

	BonusKing  king;
	BonusRegion region;
	BonusColor color;
	Map<Color,Integer> victoryColor=new HashMap<>();
	Map<Region,Integer> victoryRegion=new HashMap<>();
	Queue<Integer> victoryKing=new LinkedList<>();
	Region hill;
	
	Player player1;
	
	@Before
	public void createBonus() throws RemoteException{
		 hill=new Region("Hills",2);
		player1=new Player("1",15,2,Color.BLACK ,new HashSet<Integer>());
		victoryColor.put(Color.blue, 1);
		victoryColor.put(Color.white,3);
		victoryColor.put(Color.gray, 5);
		victoryRegion.put(hill,7);
		victoryKing.add(5);
		victoryKing.add(3);
		victoryKing.add(1);
		color=new BonusColor(victoryColor);
		king=new BonusKing(victoryKing);
		region=new BonusRegion(victoryRegion);
	}
	
	@Test
	public void testBonus(){
		
	
	color.getBonus(player1, Color.blue);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(), 1+5);
	
	region.getBonus(player1, hill);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(), 6+7+3);
	
	color.getBonus(player1, Color.white);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(),16+3+1);
	
	color.getBonus(player1, Color.gray);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(),20+5);
	
	color.getBonus(player1, Color.magenta);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(),25);
	
	color.getBonus(player1, Color.green);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(),25);
	
	region.getBonus(player1, hill);
	king.getBonusKing(player1);
	assertEquals(player1.getPlayerVictory(), 25);
	
	
		
	}	
}
