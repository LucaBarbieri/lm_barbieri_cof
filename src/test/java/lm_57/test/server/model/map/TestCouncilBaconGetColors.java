package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.Test;

import lm_57.server.model.map.CouncilBalcon;



public class TestCouncilBaconGetColors {

	@Test
	public void test() throws RemoteException {
		CouncilBalcon balcon=new CouncilBalcon();
		ArrayList<Color> colors=new ArrayList<>();
		Color[] tmp=new Color[4];
		for(int l=0;l<4;l++){
		 colors.add(balcon.getCouncilBalconColor()[l]);
		}
          for(int i=0;i<4;i++){
        	  tmp[i]=balcon.getCouncilBalconColor()[i];
          }

          for(int k=0;k<tmp.length;k++){
        	  assertSame(tmp[k],colors.get(k));
          }
	}

}
