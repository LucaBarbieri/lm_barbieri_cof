package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Test;

import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.Councillor;

public class TestElectionCouncillorBalcon {

	@Test
	public void test() throws RemoteException {
		CouncilBalcon balcon=new CouncilBalcon();
		CouncilBalcon tmp=balcon;
		Councillor councillor=new Councillor();
		
		tmp.getCouncilBalcon().remove(3);
		tmp.getCouncilBalcon().add(0,councillor);
		
		balcon.setCouncillorOnBalcon(councillor);
		 
		assertEquals(tmp,balcon);
	}

}
