package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.Test;

import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.PoliticCard;



public class TestCostCardMulticolor {

	@Test
	public void test() throws RemoteException {
	   CouncilBalcon balcon=new CouncilBalcon();
	   ArrayList<PoliticCard> politicCard=new ArrayList<>();
	   PoliticCard polCard;
	   Color tmp;
	    for(int i=0;i<2;i++){
	    	do{
	    		polCard=new PoliticCard();
	    		tmp=polCard.getPoliticCard();
	    	}while(tmp!=Color.CYAN);
	         politicCard.add(polCard);
	    }
	    int cost=balcon.comparateMulticolorCard(politicCard);
	    System.out.println(cost);
	    assertEquals(4,cost);
	}
	

}
