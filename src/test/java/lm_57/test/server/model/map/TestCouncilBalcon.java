package lm_57.test.server.model.map;

import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.map.King;
import lm_57.server.model.utility.Councillor;
import lm_57.server.model.utility.PoliticCard;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class TestCouncilBalcon {

	CouncilBalcon balcon;
	King king;
	ArrayList<Color> colors=new ArrayList<>();
	ArrayList<PoliticCard> cards=new ArrayList<>();
	
	@Before
	public void setUp() throws RemoteException{
		balcon=new CouncilBalcon();
		king=new King(null);
		colors.add(Color.BLACK);
		colors.add(Color.ORANGE);
		colors.add(Color.BLUE);
		colors.add(Color.MAGENTA);
		colors.add(Color.WHITE);
		colors.add(Color.PINK);
		for (int i=0;i<6;i++){
			cards.add(new PoliticCard());
		}
	}
	
	@Test
	public void testBalcon(){
		
		for(int i=0;i< king.getCouncillorOnBalcon().size();i++){
			assertTrue(colors.contains(king.getCouncillorOnBalcon().get(i).getColorCouncillor()));
		}
		
		List<Councillor> currentBalcon= new ArrayList<>();
		currentBalcon.addAll(king.getCouncillorOnBalcon());
		
		int coin=balcon.coinPoliticCard(cards);
		
		assertNotNull(coin);
		Councillor councillor=new Councillor();
		king.setKingCouncillorOnBalcon(councillor);
		if(!councillor.equals(currentBalcon.get(0))){
			assertNotSame(currentBalcon,king.getCouncilBalcon() );
		}
	}
}
