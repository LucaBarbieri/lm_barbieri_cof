package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Constants;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;

public class TestConfigCounst {
Config config;
Map map;
City city;
Color c;
String temp;

@Before
public void setUp() throws IOException{
	config=new Config(1,0);
	
	temp="Blue";
	c= Constants.convertStringToColor(temp);
	map = new Map(0);
	city=new City("Arkon","Coast",c,0);
	config.buildingMap();
	
	
	
}
	@Test
	public void testConfig() throws IOException {
		
	
		assertTrue(city.getName().equals(config.getMap().getCity("Arkon").getName()));
}
	

	@Test
	public void testGetMap() {
		
for(int i=0;i< map.getRegionList().size();i++){
			
			assertEquals( map.getRegionList().get(i).getName() ,config.getMap().getRegionList().get(i).getName());
				
	}

}
	@Test
	public void testPutCityInFriends() throws IOException{
	
		assertEquals("Graden",config.getMap().getCity("Framek").getLinkedCity().get(0).getName());
	}
}
