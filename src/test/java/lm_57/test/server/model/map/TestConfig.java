package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Constants;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;

public class TestConfig {
	Config config;
	Map map;
	Color c;
	String temp;
	int region=0;
	String cityName="Framek";
	City city;
	
	@Before
	public void setUp() throws IOException{
		config=new Config(4,0);
		temp="Yellow";
		c= Constants.convertStringToColor(temp);
		config.chooseCity(region, cityName, c);
		city=new City(cityName,"Hills",c,0);
		
	}
		@Test
	public void testCreateCity() throws IOException {
		assertEquals(city.getName(), config.createCity(region).getName());
		
	}

	@Test
	public void testChooseCity() throws RemoteException {
		region=5;
		assertEquals(null,config.chooseCity(region, cityName, c));
	}

	

}
