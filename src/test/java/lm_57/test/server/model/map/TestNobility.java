package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.*;

import lm_57.server.model.map.Nobility;


public class TestNobility {
	
	Nobility nobilityPath;
	Set<Integer> bonusPosition=new HashSet<>();
	
	@Before
	public void setPath(){
		bonusPosition.add(2);
		bonusPosition.add(8);
		nobilityPath=new Nobility(bonusPosition);
	}
	
	@Test
	public void TestNobilityPath(){
		
		assertEquals("initial position not null",nobilityPath.getNobility(),0);
		
		assertNull(nobilityPath.addNobility(3));
		assertEquals(nobilityPath.getNobility(),3);
		
		assertNotNull(nobilityPath.addNobility(5));
		assertEquals(nobilityPath.getNobility(),8);
		
	}
	
	
}
