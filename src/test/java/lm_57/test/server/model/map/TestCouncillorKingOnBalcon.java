package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;

import org.junit.Test;

import lm_57.server.model.map.City;
import lm_57.server.model.map.King;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.Councillor;



public class TestCouncillorKingOnBalcon {

	@Test
	public void test() throws RemoteException {
		Region region=new lm_57.server.model.map.Region("Hills",3);
		City city=new City("Juvelar","Hills",Color.BLACK,3);
		King king=new King(city);
		Councillor councillor=new Councillor();
		King tmp=king;
		
		king.setKingCouncillorOnBalcon(councillor);
		tmp.getCouncillorOnBalcon().add(0,councillor);
		tmp.getCouncillorOnBalcon().remove(3);
		
		assertSame(king.getCouncilBalcon(),tmp.getCouncilBalcon());
		
	}

}
