package lm_57.test.server.model.map;


import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.map.City;
import lm_57.server.model.map.King;
import lm_57.server.model.map.Map;

public class TestMapKing {
	Map map;
	King kingFloat;
	City city;
	Color yellow;
	@Before
	public void setUp() throws RemoteException {
		map=new Map(3);
		
		city=new City("Arkon","Hills",yellow, 0);
		kingFloat=new King(city);
		
	
}


	@Test
	public void testGetKing() {
		assertNull(map.getKing());
	}
	
	@Test
	public void testSetKing() throws RemoteException {
		map.setKing(city);
		assertFalse(map.getKing().equals(null));
	}

}