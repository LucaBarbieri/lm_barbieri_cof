package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.map.Config;
import lm_57.server.model.map.Region;

public class TestMapRegionLisToString {
	Config confi;
	Region Hills;
    Region Mountain;
	Region Coast;
	int numBonus=1;
	ArrayList<Region> regionx=new ArrayList<>();
	
	
	@Before
	public void setUp() throws IOException{
		confi=new Config(1,numBonus);
		confi.buildingMap();
		
		Region Hills= new Region ("Hills", numBonus);
		Region Mountain= new Region("Mountain", numBonus);
		Region Coast= new Region ("Coast", numBonus);
		regionx.add(Hills);
		regionx.add(Coast);
		regionx.add(Mountain);
		
		
	}

	@Test
	public void testRegionListToString() {
		for(int i=0;i<regionx.size();i++){
		assertEquals(regionx.get(i).getName(),confi.getMap().regionListToString().get(i));
		}
	
	}

}//ok

