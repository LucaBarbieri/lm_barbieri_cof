package lm_57.test.server.model.map;


import static org.junit.Assert.*;
import java.awt.Color;
import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;

public class TestMapGetCityAddCity {
	Map map;
	Color yellow;
	City city;

	@Before
	public void setUp() throws RemoteException {
		map=new Map(0);
		city=new City("Arkon","Hills",yellow, 0);
		map.addCity(city);
	}

	@Test
	public void testGetCity() throws RemoteException {
		assertEquals(city.getName(),map.getCity("Arkon").getName());
		assertNull(map.getCity("Arko"));
		
		 
		
	}


	@Test
	public void testAddCity() {
		map.addCity(city);
	}

}