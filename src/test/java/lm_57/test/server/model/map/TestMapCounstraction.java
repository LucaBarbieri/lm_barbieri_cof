package lm_57.test.server.model.map;


import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Test;

import lm_57.server.model.map.Map;

public class TestMapCounstraction {
	Map map;

	@Test
	public void testMap() throws RemoteException {
		map=new Map(0);
		assertEquals("Coast",map.getRegionFromName("Coast").getName());
		assertNull(map.getKing());
		assertNull(map.getCity("Juvelar"));
	}

}//ok
