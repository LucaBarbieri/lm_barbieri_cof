package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import lm_57.server.model.map.City;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class TestDeckRegion {

	
	List<PermitCard> permitDeck=new ArrayList<>();
	
	@Test
	public void testRegionDeck() throws RemoteException{
		Region coast=new Region("Coast", 3);
		Region hill=new Region("Hills", 2);
		for(int i=0; i<2;i++){
			assertFalse(coast.getVisibleCard().get(i).getCityAllowed().isEmpty());
			
		}
		
		permitDeck.add(0,hill.takePermitCard(hill.getVisibleCard().get(1)));
		assertNotNull(permitDeck.get(0));
		
		//test region
		City arkam=new City("arkam",coast.getName(),Color.black,1);
		coast.addCity(arkam);
		assertTrue(coast.containCity(arkam));
		assertFalse(hill.containCity(arkam));
		
		coast.addCity(arkam);
		assertTrue(coast.getCities().size()==1);
	}
}
