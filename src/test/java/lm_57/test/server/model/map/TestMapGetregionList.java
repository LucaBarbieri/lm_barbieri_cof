package lm_57.test.server.model.map;


import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.map.City;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;

public class TestMapGetregionList {
	Map map;
	City city;
	private final ArrayList<Region> regionexp=new ArrayList<>();
	int numBonus=0;
	
	@Before
	public void setUp() throws RemoteException {
		map=new Map(0);
		Region Hills= new Region ("Hills", numBonus);
		Region Mountains= new Region("Mountain", numBonus);
		Region Coast= new Region ("Coast", numBonus);
		
		regionexp.add(Hills);
		regionexp.add(Coast);
		regionexp.add(Mountains);
}

	@Test
	public void testGetRegionList() throws RemoteException {
for(int i=0;i< map.getRegionList().size();i++){
			
			assertEquals( regionexp.get(i).getName() ,map.getRegionList().get(i).getName());
				}

}
}