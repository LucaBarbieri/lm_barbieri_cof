package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.PoliticCard;



public class TestCostOfPermitCard {

	@Test
	public void test() throws RemoteException {
		CouncilBalcon balcon=new CouncilBalcon();
		List<PoliticCard> politicCard=new ArrayList<>();
		PoliticCard polCard;
		Color temporaney;
		Color[] tmp=balcon.getCouncilBalconColor();
		  for(int i=0;i<2;i++){
			  do{
				  polCard=new PoliticCard();
				  temporaney=polCard.getPoliticCard();				  
			  }while(temporaney!=tmp[i]);
			  politicCard.add(polCard);
		  }
		  do{
		     polCard=new PoliticCard();
		  }while(polCard.getPoliticCard()!=Color.CYAN);
		    politicCard.add(polCard);
		  
		double cost=balcon.comparateCards(politicCard);
		double delta=0.1;

		assertEquals(4.0,cost,delta);
		
	}



}
