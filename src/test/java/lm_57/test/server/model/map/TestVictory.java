package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import org.junit.*;

import lm_57.server.model.map.Victory;

public class TestVictory {
	Victory victoryPath;
	
	@Before
	public void setUp(){
		victoryPath=new Victory();
	}
	
	@Test
	public void testPathVictory(){
		assertEquals("the initial position is not null", 0, victoryPath.getVictory());
		
		victoryPath.addVictory(10);
		assertEquals(10, victoryPath.getVictory());
		
		victoryPath.setVictory(2);
		assertEquals(2, victoryPath.getVictory());
		
	}
}
