package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Constants;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;

public class TestMapGetArrayList {
	Config config;
	Map map;
	Color c;
	String temp;
	City city;
	 @Before
	public void setUp() throws IOException{
		config=new Config(1,0);
		config.buildingMap();
		map = new Map(0);
		temp="Blue";
		c= Constants.convertStringToColor(temp);
		city=new City("Arkon","Coast",c,0);
	}


	@Test
	public void testGetCitiesName() {
		assertEquals("Graden",config.getMap().getCitiesName().get(1));
		
}
	


}//ok
