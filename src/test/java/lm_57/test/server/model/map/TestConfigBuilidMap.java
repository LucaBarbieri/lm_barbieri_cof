package lm_57.test.server.model.map;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Constants;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;

public class TestConfigBuilidMap {
	
	
		Config config;
		Map map;
		Color c;
		String temp;
		City city;
		ArrayList<Region> regionexp=new ArrayList<>();
		
	@Before
	public void setUp() throws IOException{
		config=new Config(4,0);
		map = new Map(0);
		temp="Blue";
		c= Constants.convertStringToColor(temp);
		city=new City("Arkon","Coast",c,0);
	}

	@Test
	public void testFindMap() throws IOException {
		config.findMap();
		
	}

	@Test
	public void testBuildingMap() throws IOException {
		config.buildingMap();
		assertTrue(city.getName().equals(config.getMap().getCity("Arkon").getName()));
		
		Region Hills= new Region ("Hills", 0);
		Region Mountains= new Region("Mountain", 0);
		Region Coast= new Region ("Coast", 0);
		
		regionexp.add(Hills);
		regionexp.add(Coast);
		regionexp.add(Mountains);
 for(int i=0;i< map.getRegionList().size();i++){
			
			assertEquals( regionexp.get(i).getName() ,config.getMap().getRegionList().get(i).getName());
	
	}
 
	
	

}
}
