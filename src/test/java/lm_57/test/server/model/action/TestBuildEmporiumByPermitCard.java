package lm_57.test.server.model.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuildEmporiumByPermitCard;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class TestBuildEmporiumByPermitCard {
	
	Region region;
	Set<Integer> positionBonus=new HashSet<>();
	PermitCard permitCard1;
	PermitCard permitCard2;
	Player player1;
	Player player2;
	City city;
	BuildEmporiumByPermitCard buildEmporiumByPermitCard;
	BuildEmporiumByPermitCard buildEmporiumByPermitCard2;
	
	
	@Before
	public void setPermitCard() throws RemoteException{
		region=new Region("Hills",2);
		player1=new Player("anacleto",10,3,Color.BLUE,positionBonus);
		player2=new Player("anacleto2",10,3,Color.GRAY,positionBonus);
		
		
		do{
			city=new City("Hellar","Coast",Color.BLACK,2);
		}while(city.getBonus().getBonusString().contains("Assistant"));
		
		do{
			permitCard1=new PermitCard("Hills",2);
		}while(!(permitCard1.getCityAllowed().contains(city.getName()) && !permitCard1.getPermitBonus().toStringNameBonus().contains("Assistant")));
		
		do{
			permitCard2=new PermitCard("Hills",2);
		}while(!(permitCard2.getCityAllowed().contains(city.getName()) && !permitCard2.getPermitBonus().toStringNameBonus().contains("Assistant")));
		buildEmporiumByPermitCard=new BuildEmporiumByPermitCard(player1, permitCard1,city);
		buildEmporiumByPermitCard.setAgent(player1);
		buildEmporiumByPermitCard2=new BuildEmporiumByPermitCard(player2, permitCard2,city);
		buildEmporiumByPermitCard2.setAgent(player2);
	}
	
	@Test
	public void test() {
		assertTrue(buildEmporiumByPermitCard.isCompleted());
		assertEquals(1,city.getEmporium().size());
		assertTrue(buildEmporiumByPermitCard2.isCompleted());
		assertEquals(2,city.getEmporium().size());
		assertEquals(3,player1.getPlayerAssistant());
		assertEquals(2,player2.getPlayerAssistant(),2);	
	}
}
