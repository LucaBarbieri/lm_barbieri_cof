package lm_57.test.server.model.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.SetNewCouncillorOnBalcon;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.Councillor;

public class TestSetNewCouncillorOnBalcon {
	
	Region region;
	CouncilBalcon balcon;
	HashSet<Integer> positionBonus=new HashSet<>();
	Player player1;
	Player player2;
	Councillor councillor=new Councillor();
	ArrayList<Color> colors=new ArrayList<>();
	Color[] councillorColors=new Color[4];
	
	@Before
	public void setUp() throws RemoteException{
		region=new Region("Hills",2);
		balcon=region.getCouncilBalcon();
		player1=new Player("giulio",10,3,Color.BLACK,positionBonus);
		player2=new Player("aldo",25,3,Color.YELLOW,positionBonus);
	}
	@Test
	public void test() throws RemoteException {
		
		SetNewCouncillorOnBalcon setCouncillor=new SetNewCouncillorOnBalcon(player1, balcon,councillor);
		setCouncillor.setAgent(player1);
		SetNewCouncillorOnBalcon setCouncillor1=new SetNewCouncillorOnBalcon(player2, balcon,councillor);
		setCouncillor1.setAgent(player2);
		setCouncillor.isCompleted();
		setCouncillor1.isCompleted();
		for(int l=0;l<4;l++){
			 colors.add(balcon.getCouncilBalconColor()[l]);
			}
			 for(int i=0;i<4;i++){
	       	  councillorColors[i]=balcon.getCouncilBalconColor()[i];
			 }
			 
	          for(int k=0;k<councillorColors.length;k++){
	        	  assertSame(councillorColors[k],colors.get(k));
	          }
		
	          assertEquals(14,player1.getPlayerCoin());
	          assertEquals(25,player2.getPlayerCoin());
	}

}
