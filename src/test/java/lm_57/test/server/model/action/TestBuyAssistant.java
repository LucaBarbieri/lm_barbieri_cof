package lm_57.test.server.model.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuyAssistant;


public class TestBuyAssistant {

	Player player1;
	Set<Integer> positionBonus=new HashSet<>();
	BuyAssistant action;
	
	@Before
	public void setPlayer() throws RemoteException{
		player1=new Player("Anacleto",10,3,Color.BLACK,positionBonus);
		action=new BuyAssistant(player1);
	}
	@Test
	public void test() {
		assertTrue(action.isCompleted());
		assertEquals(player1.getPlayerCoin(),7);
		assertEquals(player1.getPlayerAssistant(),4);
	}
}
