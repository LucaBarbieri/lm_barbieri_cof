package lm_57.test.server.model.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.PayAssistantForChangePermitCard;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class TestPayAssistantChangePermitCard {
	
	Region region;
	HashSet<Integer> positionBonus=new HashSet<>();
	Player player;
	PayAssistantForChangePermitCard permit;
	ArrayList<PermitCard> permitCard=new ArrayList<>();
	ArrayList<PermitCard> permitRegion=new ArrayList<>();		
	
	@Before
	public void setUp() throws RemoteException{
		player=new Player("giacomo",10,3,Color.BLACK,positionBonus);
		region=new Region("Hills",2);
		permit=new PayAssistantForChangePermitCard(player, region);
		permit.setAgent(player);
		permitCard.addAll(region.getVisibleCard());
	}
	@Test
	public void test() {
		permit.isCompleted();
		permitRegion.addAll(region.getVisibleCard());
		assertEquals(2,player.getPlayerAssistant());		
	}

}
