package lm_57.test.server.model.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuildOnKingCity;
import lm_57.server.model.map.City;
import lm_57.server.model.map.King;
import lm_57.server.model.map.Region;

public class TestBuildOnKingCity {
	
	 Set<Integer> positionBonus=new HashSet<>();
	 Player player1;
	 Player player2;
	 Region region;
	 City graden;
	 City juvelar;
	 City framek;
	 King king;
	
	 @Before
	 public void setUp() throws RemoteException{
		 
	 player1=new Player("ugo",10,3,Color.BLACK,positionBonus);
	 player2=new Player("uga",10,3,Color.BLUE,positionBonus);
	 region=new Region("Coast",2);
	 graden=new City("Graden","Coast",Color.BLACK,2);
	 juvelar=new City("Juvelar","Hills",Color.CYAN,2);
	 framek=new City("Framek","Coast",Color.GREEN,2);
	 king=new King(juvelar);
	 }
	 
	@Test
	public void test() throws RemoteException {
	 
	 BuildOnKingCity build1=new BuildOnKingCity(player1, graden,king);
	 build1.setAgent(player1);
	 build1.isCompleted();
	 assertEquals(graden,king.getCurrentCity());
	 
	 
	 BuildOnKingCity build2=new BuildOnKingCity(player1, graden,king);
	 
	 build1.setAgent(player2);
	build2.isCompleted();
	 assertEquals(graden.getName(),king.getCurrentCity().getName());
	}

}
