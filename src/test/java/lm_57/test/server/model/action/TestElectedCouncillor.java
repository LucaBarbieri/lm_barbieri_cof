package lm_57.test.server.model.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.ElectedCouncilor;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.utility.Councillor;

public class TestElectedCouncillor {
	
	ElectedCouncilor electedCouncilor;
	CouncilBalcon balcon;
	Councillor concillor;
	Set<Integer> positionBonus=new HashSet<>();
	Player player1;
	
	@Before
	public void setElectedCouncilor() throws RemoteException{
		balcon=new CouncilBalcon();
		concillor=new Councillor();
		player1=new Player("Anacleto",10,3,Color.BLUE,positionBonus);
		electedCouncilor=new ElectedCouncilor(player1, balcon,concillor.getCouncillor());
		electedCouncilor.setAgent(player1);
	}
	@Test
	public void test() {
		//System.out.println(balcon.getCouncilBalcon().toString());
		assertTrue(electedCouncilor.isCompleted());
		assertEquals(2,player1.getPlayerAssistant());
		//System.out.println(balcon.getCouncilBalcon().toString());
	}

}
