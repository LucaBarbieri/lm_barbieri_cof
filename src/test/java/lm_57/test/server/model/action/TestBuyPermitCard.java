package lm_57.test.server.model.action;


import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.action.BuyPermitCard;
import lm_57.server.model.map.CouncilBalcon;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PoliticCard;

public class TestBuyPermitCard {
	Region region;
	HashSet<Integer> positionBonus=new HashSet<>();
	Player player;
	CouncilBalcon balcon;		
	List<PoliticCard> politicCard=new ArrayList<>();
	PoliticCard polCard;
	Color currentColor;
	List<PoliticCard> polCards=new ArrayList<>();
	
	@Before
	public void setUp() throws RemoteException{
		region=new Region("Hills",2);
		player=new Player("giacomo",10,3,Color.BLACK,positionBonus);
		 balcon=region.getCouncilBalcon();	
	}
	@Test
	public void test() throws RemoteException {
		polCards.addAll(player.getPoliticCard());
		player.removePlayerPoliticCard(polCards);
		Color[] colors=balcon.getCouncilBalconColor();
		  for(int i=0;i<2;i++){
			  do{
				  polCard=new PoliticCard();
				  currentColor=polCard.getPoliticCard();				  
			  }while(currentColor!=colors[i]);
			  politicCard.add(polCard);
		  }
		  player.addPlayerPoliticCard(politicCard);
		  politicCard.removeAll(politicCard);
		  BuyPermitCard permit=new BuyPermitCard(player, region,1);
		  permit.setAgent(player);
		  assertTrue(permit.isCompleted());
		  assertEquals(politicCard,player.getPoliticCard());
		  assertEquals(6,player.getPlayerCoin());
		  
	}

}
