package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Region;

public class TestSameEmporiumInTheSameCity {

	/*
	 * 
	 */
	@Test
	public void test() throws RemoteException{
		Set<Integer> nobility=new HashSet<>();
		Region coast=new Region("Coast",2);
		City A=new City("A","Coast",Color.blue,1);
		coast.addCity(A);
		
		nobility.add(3);
		nobility.add(8);
		Player player1=new Player("1",10,3,Color.black, nobility);
		
		player1.buildEmporium(A);
		player1.buildEmporium(A);
		player1.buildEmporium(A);
		
		assertTrue(A.getEmporium().size()==1);
		
	}

	
}
