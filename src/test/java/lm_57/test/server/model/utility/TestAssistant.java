package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.utility.Assistant;

public class TestAssistant {
	
	Assistant assistant;
	
	@Before
	public void setUp(){
		assistant=new Assistant(0);
	}
	
	@Test
	public void testAssistantNumber(){
		
		assistant.setAssistant(100);
		assertEquals(100,assistant.getAssistant() );
		
		assistant.addAssistant(200);
		assertEquals(300, assistant.getAssistant());
		
		assistant.removeAssistant(400);
		assertEquals(300, assistant.getAssistant());
	}
}
