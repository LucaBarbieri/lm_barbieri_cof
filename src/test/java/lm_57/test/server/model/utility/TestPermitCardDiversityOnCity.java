package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Test;

import lm_57.server.model.utility.PermitCard;

public class TestPermitCardDiversityOnCity {

	@Test
	public void test() throws RemoteException {
		
		
		PermitCard card=new PermitCard("Mountain",3);
		PermitCard card1=new PermitCard("Hills",3);
	
		assertNotSame(card1.getCityAllowed(),card.getCityAllowed());
	
	}

}
