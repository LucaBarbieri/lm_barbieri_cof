package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.rmi.RemoteException;

import org.junit.Test;

import lm_57.server.model.utility.PermitCard;

public class TestPermitCardDiversityOnBonus {

	@Test
	public void test() throws RemoteException {
		
		PermitCard card=new PermitCard("Coast",3);
		PermitCard card1=new PermitCard("Hills",3); 
		  
		assertNotSame(card.getPermitBonus(),card1.getPermitBonus());
	}

}
