package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Test;

import lm_57.server.model.utility.Councillor;

public class TestCouncillorNotMulticolor {

	@Test
	public void test() {
		Councillor test=new Councillor();
		Color color=Color.CYAN ;  
		
		assertNotSame(test.getColorCouncillor(),color);
		
		test.setCouncillorColor(Color.BLACK);
		assertEquals(test.getColorCouncillor(), Color.BLACK);
	}

}
