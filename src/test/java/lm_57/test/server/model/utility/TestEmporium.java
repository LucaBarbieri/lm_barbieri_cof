package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;


public class TestEmporium {
	
	private Player player1;
	private Set<Integer> set=new HashSet<>();
	
	
	@Before
	public void setUp() throws Exception{
		player1= new Player("1",10,2,Color.black, set);
	}
	
	@Test
	public void nullIntialPosition(){
		for(int i=0; i <player1.getPlayerEmporium().size();i++){
			assertEquals("null initial position",null,
					player1.getPlayerEmporium().get(i).getPosition());
		}
	}
}
