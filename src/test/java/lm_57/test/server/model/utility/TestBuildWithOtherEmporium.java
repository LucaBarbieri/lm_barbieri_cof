package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;


import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Region;


public class TestBuildWithOtherEmporium {
	
	Player player1;
	Player player2;
	Set<Integer> set=new HashSet<>();
	
	Region coast;
	City arkam;
	
	@Before
	public void createObject() throws RemoteException{
		player1=new Player("1",10,1,Color.black,set);
		player2=new Player("2", 10,1, Color.blue,set);
		coast=new Region("Coast",2);
		arkam=new City("A","Coast",Color.blue,1);
	}
	
	@Test
	public void testBothPlayerBuild(){
		player2.buildEmporium(arkam);
		player1.buildEmporium(arkam);
		assertEquals(2,arkam.getEmporium().size());
		assertEquals(0, player1.getPlayerAssistant());
		assertEquals(1, player2.getPlayerAssistant());
		
	}
}
