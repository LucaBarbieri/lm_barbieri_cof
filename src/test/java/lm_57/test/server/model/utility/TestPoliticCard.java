package lm_57.test.server.model.utility;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.*;

import lm_57.server.model.utility.PoliticCard;

public class TestPoliticCard {

	PoliticCard politicCard;
	ArrayList< PoliticCard> cards=new ArrayList<>();
	ArrayList<Color> colors=new ArrayList<>();
	
	@Before
	public void setPoliticCard(){
		for(int i=0;i<100;i++){
			politicCard= new PoliticCard();
			cards.add(politicCard);
		}
		colors.add(Color.BLACK);
		colors.add(Color.ORANGE);
		colors.add(Color.BLUE);
		colors.add(Color.MAGENTA);
		colors.add(Color.WHITE);
		colors.add(Color.PINK);
		colors.add(Color.CYAN);
	}
	
	@Test
	public void testCard(){
		
		assertEquals(100, cards.size());
		for(int i=0;i<cards.size();i++){
			assertTrue(colors.contains(cards.get(i).getPoliticCard()) );
		}
	}
}
