package lm_57.test.server.model.market;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.market.AssistantSeller;

public class TestAssistantSeller {
	Player player1;
	Player player2;
	HashSet<Integer> position=new HashSet<>();
	
	
	@Before 
	public void setUp() throws RemoteException{
		 player1=new Player("Luca",4,6,Color.BLACK,position);
		 player2=new Player("Ahmed",7,8,Color.WHITE,position);
	}
	
	@Test
	public void test() throws RemoteException {
			
		AssistantSeller seller=new AssistantSeller(player2,3,3);
		seller.sellingProduct(player1);
		
		assertEquals(1,player1.getPlayerCoin());
		assertEquals(10,player2.getPlayerCoin());
		assertEquals(9,player1.getPlayerAssistant());
		assertEquals(5,player2.getPlayerAssistant());
		
		
	}

}
