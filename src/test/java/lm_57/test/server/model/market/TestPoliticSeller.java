package lm_57.test.server.model.market;


import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.market.PoliticSeller;
import lm_57.server.model.utility.PoliticCard;

public class TestPoliticSeller {
	HashSet<Integer> position=new HashSet<>();
	Player player1;
	Player player2;
	ArrayList<PoliticCard> politicCard=new ArrayList<>();
	ArrayList<PoliticCard> tmp=new ArrayList<>();
	
	@Before 
	public void setUp() throws RemoteException{
		 player1=new Player("Luca",4,6,Color.BLACK,position);
		 player2=new Player("Ahmed",7,8,Color.WHITE,position);
	}
	@Test
	public void test() throws RemoteException {

			player1.addPoliticCard(2);
			tmp.addAll(player2.getPoliticCard());
			politicCard.addAll(player1.getPoliticCard());
		    tmp.addAll(politicCard);
		PoliticSeller seller=new PoliticSeller(player1,politicCard,3);
		seller.sellingProduct(player2);
		politicCard.removeAll(politicCard);
		assertEquals(7,player1.getPlayerCoin());
		assertEquals(4,player2.getPlayerCoin());
		assertEquals(politicCard,player1.getPoliticCard());
		assertEquals(tmp,player2.getPoliticCard());
	}

}
