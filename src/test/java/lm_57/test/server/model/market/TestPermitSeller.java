package lm_57.test.server.model.market;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.model.Player;
import lm_57.server.model.market.PermitSeller;
import lm_57.server.model.utility.PermitCard;

public class TestPermitSeller {
	
	HashSet<Integer> position=new HashSet<>();
	Player player1;
	Player player2;
	ArrayList<PermitCard> tmp=new ArrayList<>();
	ArrayList<PermitCard> permitCard=new ArrayList<>();
	
	@Before 
	public void setUp() throws RemoteException{
		 player1=new Player("Luca",6,6,Color.BLACK,position);
		 player2=new Player("Ahmed",7,8,Color.WHITE,position);
	}
	@Test
	public void test() throws RemoteException {
		
		   for(int i=0;i<2;i++){
			   permitCard.add(new PermitCard("Mountain",3));
		   }
		   
		   tmp.addAll(permitCard);
		   player2.addPlayerPermitCard(permitCard);
		PermitSeller seller=new PermitSeller(player2,permitCard,6);
		     seller.sellingProduct(player1);

			permitCard.removeAll(permitCard);

		assertEquals(permitCard,player2.getPlayerPermitCard());
		assertEquals(tmp,player1.getPlayerPermitCard());
		assertEquals(0,player1.getPlayerCoin());
		assertEquals(13,player2.getPlayerCoin());
	}

}

