package lm_57.test.server.controller.action;
import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.controller.action.ControlBuildEmporiumByPermitCard;
import lm_57.server.controller.action.ControlBuyAssistant;
import lm_57.server.model.Player;
import lm_57.server.model.map.City;

public class TestControllBuyAssistent {
	 
	
	ControlBuyAssistant flag;
    Player player;
    City city;
	HashSet<Integer> positionBonus=new HashSet<>();
	
	@Before
	public void setUp () throws RemoteException{
		
		for(int i=0;i<3;i++){
			positionBonus.add(i);
		}
	player=new Player("Ahm",1,1,Color.BLACK,positionBonus);
	city= new City("Kultos","Coast",Color.BLUE,0);
	flag=new ControlBuyAssistant(player);
	}
	
	@Test
	public void testControlBuyAssistant() {
		
		assertEquals(player.getName(),flag.getPlayer().getName());
	}

	@Test
	public void testIsCompletedTrue() throws RemoteException {
		player.addPlayerCoin(10);
		assertTrue(flag.isCompleted());
		
	
	}
	@Test
	public void testIsCompletedFalse() throws RemoteException {
		player.addPlayerCoin(0);
		assertFalse(flag.isCompleted());
		
	
	}

}
