package lm_57.test.server.controller.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.controller.action.ControlBuildEmporiumByPermitCard;
import lm_57.server.model.Player;
import lm_57.server.model.action.BuildEmporiumByPermitCard;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;
import lm_57.server.model.map.Region;
import lm_57.server.model.utility.PermitCard;

public class TestContollerBuildByPermitCard {
	
	ControlBuildEmporiumByPermitCard flag;
    Player player;
    City city;
    Map map;
	HashSet<Integer> positionBonus=new HashSet<>();
	PermitCard permitCard;
	List<PermitCard> permitCardList=new ArrayList<>();
	
	
	
	
	@Before
	public void setUp () throws RemoteException{
		
		for(int i=0;i<3;i++){
			positionBonus.add(i);
		}
	player=new Player("Ahm",1,1,Color.BLACK,positionBonus);
	city= new City("Kultos","Coast",Color.BLUE,0);
	map=new Map(1);
	flag= new ControlBuildEmporiumByPermitCard(player,map,permitCard,city);
	
	}

	
	@Test
	public void testControlBuildEmporiumByPermitCard() {
		String temp= player.getName();
		
		assertEquals(temp,flag.getPlayer().getName());
		assertEquals("Kultos",flag.getCity().getName());
	
	}
	
	@Test
	public void testIsCompletedFalse() throws RemoteException {
		assertFalse(flag.isCompleted());
	}
	 

	@Test
   public void testIsCompletedTrue() throws IOException {
		
		City city=new City("Naris","Coast",Color.BLUE,0);
		
		PermitCard permitCard1 = new PermitCard("Coast",1);
	    permitCard1.setCityAllow("Coast");
		
		while(!(permitCard1.getCityAllowed().contains(city.getName()))){
			System.out.println(city.getName());
			permitCard1=new PermitCard("Coast",1);
			permitCard1.setCityAllow("Coast");
			permitCardList.add(permitCard1);
			player.addPlayerPermitCard(permitCardList);
			assertTrue(permitCardList.contains(permitCard1));
			assertTrue(player.getPlayerPermitCard().contains(permitCard1));
			System.out.println(permitCard1.PermitCardtoString());
		}
		assertTrue(permitCard1.getCityAllowed().contains(city.getName()));
		System.out.println(permitCard1.PermitCardtoString());

	   
		

		
	}
}