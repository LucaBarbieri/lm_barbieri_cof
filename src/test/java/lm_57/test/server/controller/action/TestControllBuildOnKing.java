package lm_57.test.server.controller.action;

import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.controller.action.ControlBuildEmporiumByPermitCard;
import lm_57.server.controller.action.ControlBuildOnKingCity;
import lm_57.server.model.Player;
import lm_57.server.model.map.City;
import lm_57.server.model.map.Config;
import lm_57.server.model.map.Map;
import lm_57.server.model.utility.PermitCard;

public class TestControllBuildOnKing {
    Player player;
    City city;
	HashSet<Integer> positionBonus=new HashSet<>();
	PermitCard permitCard;
	ControlBuildOnKingCity flag;
	Config config;
	List<String> cityPath=new ArrayList<>();;
	
	@Before
	public void setUp () throws IOException{
		
		for(int i=0;i<3;i++){
			positionBonus.add(i);
		}
	player=new Player("Ahm",1,1,Color.BLACK,positionBonus);
	city= new City("Arkon","Hills",Color.BLUE,0);
	config=new Config(1,3);
	config.buildingMap();
	
}

	@Test
	public void testControlBuildOnKingCity() {
		
		flag=new ControlBuildOnKingCity(player,city,config.getMap(),cityPath);
		assertEquals("Ahm",flag.getPlayer().getName());
		assertEquals("Arkon",flag.getCity().getName());
		assertTrue(flag.getCityPath().isEmpty());
	
	}

	@Test
	public void testIsCompleted() throws RemoteException {
		flag=new ControlBuildOnKingCity(player,city,config.getMap(),cityPath);
		assertTrue(flag.isCompleted());
		cityPath=null;
		ControlBuildOnKingCity flag2=new ControlBuildOnKingCity(player,city,config.getMap(),cityPath);
		assertFalse(flag2.isCompleted());

		
	}

}
