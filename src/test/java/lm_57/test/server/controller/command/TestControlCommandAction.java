package lm_57.test.server.controller.command;


import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Test;

import lm_57.server.controller.socket.ControlCommandAction;
import lm_57.server.controller.socket.ControlCommandMarket;
import lm_57.server.model.Player;
import lm_57.server.model.map.Config;

public class TestControlCommandAction {

	ControlCommandAction controlAction;
	Config file;
	HashSet<Integer> positionBonus=new HashSet<>();
	Player player1;
	Queue<Player> players=new LinkedBlockingQueue<>();
	String command1="1-Arkon_1-";
	String command2="2-Hills_1-";
	String command3="3-Arkon Burgen-";
	String command4="4-King_Black";
	String command4a="4-Hills_Black";
	String command5="5-   -";
	String command6="6-Hills-";
	String command7="7-King_Pink";
	String command7a="7-Coast_Pink";
	String command8="8-  -";
	
	@Test
	public void test() throws IOException,RemoteException{
		file=new Config(1,0);
		file.buildingMap();
		for(int i=0;i<3;i++){
			positionBonus.add(i);
		}
		player1=new Player("luca",1,1,Color.BLACK,positionBonus);
		players.add(player1);
		controlAction=new ControlCommandAction(file.getMap(),players);
		
		assertEquals(false,controlAction.controlPrototypeCommand(command1,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command2,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command3,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command4,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command4a,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command5,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command6,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command7,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command7a,player1.getName()));
		assertEquals(true,controlAction.controlPrototypeCommand(command8,player1.getName()));
		
		assertEquals(player1,controlAction.getCurrentPlayer());
		assertNotEquals(null,controlAction.getCity());
		assertNotEquals(null,controlAction.getCityPath());
		assertNotEquals(Color.DARK_GRAY,controlAction.getColorChose());
		assertEquals(null,controlAction.getPermitCard());
		assertNotEquals(null,controlAction.getIndexRegion());
		assertNotEquals(null,controlAction.getKing());
		
		
		
	}
           
	
}
