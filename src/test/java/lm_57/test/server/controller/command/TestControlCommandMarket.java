package lm_57.test.server.controller.command;


import static org.junit.Assert.*;

import java.awt.Color;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Test;

import lm_57.server.controller.socket.ControlCommandAction;
import lm_57.server.controller.socket.ControlCommandMarket;
import lm_57.server.model.Player;
import lm_57.server.model.map.Config;
import lm_57.server.model.utility.PermitCard;
import lm_57.server.model.utility.PoliticCard;

public class TestControlCommandMarket {

	ControlCommandMarket controlMarket;
	Config file;
	HashSet<Integer> positionBonus=new HashSet<>();
	Player player1;
	Queue<Player> players=new LinkedBlockingQueue<>();
	List<PoliticCard> politicCards=new ArrayList<>();
	List<PermitCard> permitCards=new ArrayList<>();
	String command1="1-1_1-";
	String command2="2-1_2-";
	String command3="3-1_2-";
	
	@Test
	public void test() throws IOException,RemoteException{
		file=new Config(1,0);
		file.buildingMap();
		for(int i=0;i<3;i++){
			positionBonus.add(i);
		}
		player1=new Player("luca",1,1,Color.BLACK,positionBonus);
		players.add(player1);
		controlMarket=new ControlCommandMarket(players);
		assertEquals(true,controlMarket.controlPrototypeCommand(command1,player1.getName()));
		assertEquals(false,controlMarket.controlPrototypeCommand(command2,player1.getName()));
		assertEquals(true,controlMarket.controlPrototypeCommand(command3,player1.getName()));
		
		politicCards.add(new PoliticCard());
		politicCards.add(new PoliticCard());
		player1.addPlayerPoliticCard(politicCards);
		
		permitCards.add(new PermitCard("Hills",2));
		permitCards.add(new PermitCard("Coast",2));
		player1.addPlayerPermitCard(permitCards);

		assertEquals(true,controlMarket.controlPrototypeCommand(command3,player1.getName()));
		assertEquals(true,controlMarket.controlPrototypeCommand(command2,player1.getName()));
	}

}
