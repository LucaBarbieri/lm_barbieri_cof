package lm_57.test.server.controller.RMI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Test;

import lm_57.server.controller.User;
import lm_57.server.controller.RMI.runActionRMI;
import lm_57.server.model.GameBoard;
import lm_57.server.model.utility.PermitCard;

public class TestRunSell {
	 
	@Test
	public void Test() throws IOException{
		
		Set<Integer> position=new HashSet<>();
		List<User> user=new ArrayList<>();
		user.add(new User("Player1", "Black"));
		user.add(new User("Player2", "White"));
		position.add(1);
		String color="Blue";
		GameBoard game=new GameBoard("COUNCIL OF FOUR");
		game.setNumMap(1);
		game.setNumBonus(1);
		game.setPositionBonus(position);
		game.addPlayer(2, user);
		game.setMap();
		game.start();
		
		game.getCurrentPlayerTurn().addPlayerAssistant(4);
		assertTrue(runActionRMI.runSellAssistant(game,2, 1));
		
		List<PermitCard> list=new ArrayList<>();
		list.add(new PermitCard("Hills",2));
		game.getCurrentPlayerTurn().addPlayerPermitCard(list);
		List<Integer> index=new ArrayList<>();
		index.add(0);
		
		assertTrue(runActionRMI.runSellPermit(game, 3, index));
		assertTrue(runActionRMI.runSellPolitic(game, 4, index));
		
		assertTrue(game.getProductToSell().size()==3);
	}
}
