package lm_57.test.server.controller.RMI;

import static org.junit.Assert.*;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import lm_57.server.controller.User;
import lm_57.server.controller.RMI.runActionRMI;
import lm_57.server.model.GameBoard;

public class TestRunKingEmporium {

	@Test
	public void TestRunAction() throws IOException, RemoteException{
		
		Set<Integer> position=new HashSet<>();
		List<User> user=new ArrayList<>();
		user.add(new User("Player1", "Black"));
		user.add(new User("Player2", "White"));
		position.add(1);
		GameBoard game=new GameBoard("COUNCIL OF FOUR");
		game.setNumMap(1);
		game.setNumBonus(1);
		game.setPositionBonus(position);
		game.addPlayer(2, user);
		game.setMap();
		game.start();
		
		assertTrue(game.getMap().getKing().getCurrentCity().getName().equals("Juvelar"));
		List<String> list=new ArrayList<>();
		
		game.getCurrentPlayerTurn().addPlayerCoin(20);
		assertTrue(runActionRMI.runMainActionBuildOnKing(game, list, "Juvelar"));
		
		list.add("Merkatim");
		assertTrue(runActionRMI.runMainActionBuildOnKing(game, list, "Merkatim"));
		assertEquals(game.getMap().getKing().getCurrentCity().getName(),"Merkatim");
		list.clear();
		list.add("Arkam");
		assertFalse(runActionRMI.runMainActionBuildOnKing(game, list, "Arkam"));
		assertFalse(runActionRMI.runMainActionBuildOnKing(game, null, null));
	
	}
}
