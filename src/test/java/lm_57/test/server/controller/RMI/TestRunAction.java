package lm_57.test.server.controller.RMI;
import static org.junit.Assert.*;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import lm_57.server.controller.User;
import lm_57.server.controller.RMI.runActionRMI;
import lm_57.server.model.GameBoard;
import lm_57.server.model.utility.PermitCard;



public class TestRunAction {

	@Test
	public void TestRunAction() throws IOException, RemoteException{
		
		Set<Integer> position=new HashSet<>();
		List<User> user=new ArrayList<>();
		user.add(new User("Player1", "Black"));
		user.add(new User("Player2", "White"));
		position.add(1);
		String color="Blue";
		GameBoard game=new GameBoard("COUNCIL OF FOUR");
		game.setNumMap(1);
		game.setNumBonus(1);
		game.setPositionBonus(position);
		game.addPlayer(2, user);
		game.setMap();
		game.start();
		
		assertFalse(runActionRMI.runFastActionElection(game, "Hills", color));
		game.getStateGame().getCurrentRound().getCurrentTurn().removeFastAction();
		game.getStateGame().getCurrentRound().getCurrentTurn().removeMainAction();
		game.setNextTurn();
		assertTrue(runActionRMI.runFastActionElection(game, "Hills", color));
		game.getCurrentPlayerTurn().addPlayerAssistant(1);
		assertTrue(runActionRMI.runFastActionChangeVisibleCard(game, "Hills"));
		assertFalse(runActionRMI.runFastActionChangeVisibleCard(game,"Hills"));
		
		assertTrue(game.getCurrentPlayerTurn().getPlayerCoin()>=3);
		assertFalse(runActionRMI.runPayForMainAction(game.getCurrentPlayerTurn()));
		game.getCurrentPlayerTurn().addPlayerAssistant(3);
		assertTrue(runActionRMI.runPayForMainAction(game.getCurrentPlayerTurn()));
		assertTrue(runActionRMI.runFastActionBuyAssistant(game.getCurrentPlayerTurn()));
		game.getCurrentPlayerTurn().removePlayerCoin(3);
		assertFalse(runActionRMI.runFastActionBuyAssistant(game.getCurrentPlayerTurn()));
		
		game.getCurrentPlayerTurn().addPlayerCoin(20);
		List<PermitCard> list=new ArrayList<>();
		list.add(new PermitCard("Hills",2));
		game.getCurrentPlayerTurn().addPlayerPermitCard(list);
		List<String> cities=new ArrayList<>();
		cities.addAll(list.get(0).getCityAllowed());
		assertTrue(runActionRMI.runMainActionElectedCouncillor(game,"Hills","Magenta"));
		
	}
}
