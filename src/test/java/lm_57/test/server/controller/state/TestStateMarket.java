package lm_57.test.server.controller.state;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.controller.state.StateMarket;
import lm_57.server.model.Player;

public class TestStateMarket {
	Player player1;
	Player player2;
	Set<Integer> bonus=new HashSet<>();
	Queue<Player> playerGame=new LinkedBlockingQueue<>();
	StateMarket market;
	
	@Before
	public void setGame() throws RemoteException{
		
		player1= new Player ("player1",10,10,Color.BLACK,bonus );
		player2=new Player("player2", 10,10,Color.BLUE,bonus);
		playerGame.add(player1);
		playerGame.add(player2);
		market=new StateMarket(playerGame);
	}
	
	@Test
	public void TestGame(){
	
		assertFalse(market.getSell().gameIsFinished());
		market.getSell().getCurrentTurn().removeAction();
		assertNotEquals(market.getCurrentState(),market.getSell().getCurrentState());
		assertEquals(market.getCurrentAgent(), market.getSell().getCurrentAgent());
		assertEquals(market.getSell().getCurrentAgent(), player1);
		assertEquals(market.getSell().getColorAgent(),"Black");
		assertEquals(market.getColorAgent(),"Black");
		assertEquals(market.toString(), "[ state: Market sell] "
		+ "[ current turn: "+ market.getSell().getCurrentTurn().getCurrentAgent().getName()+"]");
		market.setNextState();
		market.getSell().getCurrentTurn().removeAction();
		market.setNextState();
		assertFalse(market.getBuy().gameIsFinished());
		assertNotEquals(market.getCurrentState(),market.getBuy().getCurrentState());
		assertEquals(market.getCurrentAgent(), market.getBuy().getCurrentAgent());
		market.setNextState();
		market.getBuy().getCurrentTurn().removeAction();
		market.setNextState();
		market.getBuy().getCurrentTurn().removeAction();
		assertFalse(market.gameIsFinished());
		assertEquals(market.toString(),"[ state: Market buy] "+ "[ current turn: "
		+ market.getBuy().getCurrentTurn().getCurrentAgent().getName()+"]");
		market.setNextState();
		assertEquals(market.toString(), "The state of market is finished");
	}
}
