package lm_57.test.server.controller.state;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Before;
import org.junit.Test;

import lm_57.server.controller.state.StateGameContext;
import lm_57.server.model.Player;

public class TestGameState {
	
	Player player1;
	Player player2;
	Set<Integer> bonus;
	Queue<Player> playerGame;
	StateGameContext controller;
	
	@Before
	public void setGame() throws RemoteException{
		playerGame=new LinkedBlockingQueue<>();
		bonus=new HashSet<>();
		player1= new Player ("player1",10,10,Color.black,bonus );
		player2=new Player("player2", 10,10,Color.blue,bonus);
		playerGame.add(player1);
		playerGame.add(player2);
		controller=new StateGameContext(playerGame);
	}
	
	@Test
	public void TestGame(){
		
		//round
		assertEquals(controller.getCurrentState(),controller.getCurrentRound());
		
		//turn player1
		assertEquals(controller.getCurrentRound().getCurrentTurn().getCurrentAgent(),player1 );
		assertEquals(controller.getCurrentState().getCurrentAgent(),player1);
		
		//player1 end the turn
		controller.getCurrentRound().getCurrentTurn().removeFastAction();
		controller.getCurrentRound().getCurrentTurn().removeMainAction();
		assertTrue(controller.getCurrentRound().getCurrentTurn().isCompleted());
		
		assertFalse(controller.getCurrentRound().isCompleted());
		
		//turn player2
		controller.getCurrentRound().setNextTurn();
		assertEquals(controller.getCurrentRound().getCurrentTurn().getCurrentAgent(),player2);
		assertEquals(controller.getCurrentState().getCurrentAgent(), player2);
		
		//player2 end the turn
		controller.getCurrentRound().getCurrentTurn().removeFastAction();
		controller.getCurrentRound().getCurrentTurn().removeMainAction();
		
		assertTrue(controller.getCurrentState().isCompleted());
		
		//round end, new market state
		
		controller.setNextState();
		assertEquals(controller.getCurrentState(), controller.getCurrentMarket());
		assertFalse(controller.getCurrentMarket().isCompleted());
		
		//state market: sell
		
		assertEquals(controller.getCurrentMarket().getMarketState(), controller.getCurrentMarket().getSell());
		
		//turn market sell:player1
		
		assertEquals(controller.getCurrentMarket().getSell().getCurrentTurn().getCurrentAgent(),player1);
		assertEquals(controller.getCurrentState().getCurrentAgent(), player1);
		
		//end sell market
		
		controller.getCurrentMarket().getSell().getCurrentTurn().removeAction();
		assertTrue(controller.getCurrentMarket().getSell().getCurrentTurn().isCompleted());
		
		controller.getCurrentMarket().getSell().setNextTurn();
		assertFalse(controller.getCurrentMarket().isCompleted());
		assertFalse(controller.getCurrentMarket().getSell().getCurrentTurn().isCompleted());
		
		controller.getCurrentMarket().getSell().getCurrentTurn().removeAction();
		assertTrue(controller.getCurrentMarket().getSell().isCompleted());
	
		
		//state market: buy
		
		controller.getCurrentMarket().setNextState();
		assertEquals(controller.getCurrentMarket().getMarketState(), controller.getCurrentMarket().getBuy());
		
		//end market buy

		controller.getCurrentMarket().getBuy().getCurrentTurn().removeAction();
		controller.getCurrentMarket().getBuy().setNextTurn();
		controller.getCurrentMarket().getBuy().getCurrentTurn().removeAction();
		assertTrue(controller.getCurrentMarket().getBuy().isCompleted());
		
		//end market
		controller.getCurrentMarket().setNextState();
		assertNull(controller.getCurrentMarket().getMarketState());
		
		assertTrue(controller.getCurrentMarket().isCompleted());
		
		//new round
		
		controller.setNextState();
		assertEquals(controller.getCurrentState(),controller.getCurrentRound());
	
		
	}
}
