package lm_57.test.server.controller.state;

import static org.junit.Assert.*;

import java.awt.Color;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.*;

import lm_57.server.controller.state.StateGameContext;
import lm_57.server.model.Player;

public class TestStateGameCompleted{
	
	Set<Integer> bonus;
	StateGameContext controller;
	Queue<Player> players;
	
	Player player1;
	Player player2;
	
	@Before
	public void setUp() throws RemoteException{
		 bonus =new HashSet<>();
		players=new LinkedBlockingQueue<>();
		player1=new Player("1",10,3,Color.BLUE,bonus);
		player2=new Player("2",10,3,Color.GRAY,bonus);
		players.add(player1);
		players.add(player2);
		controller=new StateGameContext (players);
		player1.addPlayerVictory(50);
		player2.addPlayerVictory(20);
		player1.addPlayerNobility(10);
		player2.addPlayerNobility(12);
	}
	
	@Test
	public void testGame(){
		
		assertTrue(controller.getCurrentRound().getCurrentTurn().getCurrentAgent().equals(player1));
		player1.removeEmporium();
		
		//end round
		controller.getCurrentRound().getCurrentTurn().removeFastAction();
		controller.getCurrentRound().getCurrentTurn().removeMainAction();
		
		assertTrue(controller.getCurrentRound().getCurrentTurn().isCompleted());
		
		
		assertTrue(controller.getCurrentRound().gameIsFinished());
		
		controller.getCurrentRound().setNextTurn();
		
		
		controller.getCurrentRound().getCurrentTurn().removeFastAction();
		controller.getCurrentRound().getCurrentTurn().removeMainAction();
		
		//end game
		assertTrue(controller.getCurrentRound().isCompleted());
		
		controller.setNextState();
		
		assertEquals(controller.getCurrentState(),controller.getGameCompleted());
		
		//winner
		
		assertEquals(controller.getGameCompleted().getWinner(),player1);
		
		assertEquals(player1.getPlayerVictory(),55);
		assertEquals(player2.getPlayerVictory(),25);
		
		assertTrue(controller.getGameCompleted().isCompleted());
		
		controller.setNextState();
		assertEquals(controller.getGameCompleted(),controller.getCurrentState());
		
		
	}
	
	
}
