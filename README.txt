1. Run in the package lm_57.server.main the class ServerMain to start the server
2. Run in the package lm_57.client.main the class CouncilOfFour
3. You now have a window with the choice of the mode, you can choose the Local game or the Network one( with Socket or RMI)
4. In the Network mode you can choose Socket or RMI, unfotunately they don't work together.

note:

LOCAL:This part are the same of the socket.
      It is incomplete.

SOCKET:In the Socket part the first client connected chose the game initializations(number of bonus, number of map, number of max players for the current game)
       For chose the action we implemented some commands:
       Prototype: NumAction-Options-
       Also for the market stage were defined some commands.
       The prototype commands for the market stage and for playing actions are described later in this file.
       The actions are identified by a number.
       The main actions go from 1 to 4, the remaining ones are fast actions.
       Every single action has a different option based on what they need.
       Every option is described below.
       Same goes for market actions.
       The socket part is not fully completed because the market buy stage don't start.
      
      
RMI: In the RMI mode we implemented the timer for both connection and action of 20sec, at the beginning the user have to login
	and then initialize the game(only for the first player). All the rules of game are implemented here but we have 
	problems to implements multiple cuncurrent matches.


functionality of the project:

-Regole complete + CLI
-Socket + CLI
-RMI + CLI


Commands Action Socket:1)BuildEmporiumByPermitCard-City_IndexPlayerPermitCard-
				       2)BuyPermitCard-Region_IndexRegionPermitCard-
				       3)BuildWithKing-ListCities-
				       4)SetNewCouncillorOnBalcon-King or Region_Color-
				       5)BuyAssistant-   -
				       6)PayAssistantForCHangePermitCard-Region-
				       7)ElectedCouncillor-King or Region_Color-
				       8)PayAssistantForNewMainAction-    -
	
	
				       
Commands Market Socket:1)Assistant-NumAssistant_CoinToBuy-
				       2)PoliticCard-NumPoliticCard_CoinToBuy-
				       3)PermitCard-NumPermitCard_CoinToBuy-
				       
				       